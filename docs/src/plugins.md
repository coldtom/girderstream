# Plugins

There are two distinct types of plugins in girderstream. They are source plugins
and build plugins. Both plugins are run inside a sandbox the difference is that
source plugins are responsible for fetching and so have access to the network
but build plugins do not.

## Source plugin overview

Source plugins are responsible for transforming there configuration into source
code. A given set of configuration should result in the same sources code no
mater how or when they are run. If they can not retrieve the source code then
they should fail.

If they fetch something like a tar from a web archive they should have the info
for retrieving the exact source, but they should also include some kind of check
ie a sha they can check the extracted sources match.

## Build plugin overview

Build plugins should have a set of instructions so that for a given set of sources
the same binary or other outputs are always the same.

This can be quite tricky as compilers like to put things like time/date stamps
into there outputs. But there are many projects that aim to make reproducible
builds that we can leverage.

## Caching

A key aim of girderstream is to leverage cached builds to allow members of a team
to reuse what other members or there CI has already build. This can only work if
girderstream can isolate the host from the build and to recognize how changes
to the inputs affect the outputs.

But this will only work if the plugins deterministically us the inputs they are
given from girderstream.

## Source Plugin Inputs

The source plugin configuration in the project.conf define:
 * The plugin's name, use to identify it in elements.
 * There dependencies, ie the binary file they use, them selves and tools
 * A set of commands to run that includes how to conclude the yaml input

Girderstream then runs the commands from the plugins configuration in the sandbox
and then takes everything from the `installation directory` to from the output
of the source.

### Track

Girderstream source plug-ins can be configured so you can use them to update
the version of the source a element points to.

Plugins can only do this when Girderstream is invoked with the track verb. When
instructed to track, a source plugin can use the network to identify any new versions
of the source code they point to.

This is a little analogues to `git pull`. In the git case, when the user runs the `pull`
command of git. git goes and gets the latest version of the current branch and checks-out
that new code. In Girdersteam the idea is that whe invoked with `track` the a git source
plugin could update the `sha` of that element to point to the most up-to-date `sha` for the
currently tracked branch.

Exactly what the plugin does is up to the plugin, Girderstream just invokes the sandbox
with the same layout of the plugin and its dependencies as it it was about to do a fetch
but adds the --track option. Then instead of expecting a fetched set of sources it expects
a file containing yaml for new values to be updated. Then Girderstream takes the new values
and changes the original input `.grd` file and updates the given values.

## Build Plugin Inputs

The build plugin configuration in the project.conf define:
* The plugin's name, use to identify it in elements.
* There dependencies, ie the binary file its self and the build tools and sdk.
* A set of command to run in the sandbox.

For a given element using a build plugin Girderstream will compose a chroot
including the elements that the plugin specifies, the outputs of the elements
sources, and any build dependencies specified by the element. And a file
containing all of the plugin variables both from teh element and the project. 

Girderstream then runs the commands from the plugins configuration in the sandbox
and then takes everything from the `installation directory` to from the output
of the element.
