# Elements

The elements contain:
  * The kind of element (Must be one of the build plugins from project.conf)
  * The name of what the element `Provides`
  * The project context they should inherit
  * The elements plugin variables
  * The elements environment variables
  * The elements sources
  * The elements dependencies

## Example elements

Here is a example element

``` yaml
kind: autotools

provides: nettle

depends:
- dependency: tool_chain
  scope: Build
- dependency: tool_chain_cross
  scope: Build
- dependency: tool_chain_cross_libs
  scope: Build
- dependency: gmp
  scope: BuildAndRun

sources:
 - kind: git_source
   url: https://git.lysator.liu.se/nettle/nettle.git
   sha: ea4ea5e6904124b297ade0d1b182c5e530909765

context: build_target

environment:
  join_strategy: Join
  variables:
    PKG_CONFIG_PATH: /usr/lib/%{target-triplet}/pkgconfig/
    PKG_CONFIG_LIBDIR: /usr/lib/%{target-triplet}/pkgconfig/

plugin_variables:
  join_strategy: Join
  variables:
    prefix: /usr
    conf-args:  --build %{host-triplet} --host %{extra_host} --prefix "%{prefix}" --libdir "%{libdir}/%{target-triplet}" --includedir "%{includedir}/%{target-triplet}" --bindir "%{bindir}/%{target-triplet}" --enable-mini-gmp --disable-documentation
    make: make -j10

    configP_install-commands: |
      rm -rf "%{install-root}/usr/share"
```

## Sections

### Kind

The type of build plugin to use when building this element. This is defined in
the projects `project.conf` or from a `project.conf` in a junction if configured
appropriately.

### Provides

The name of the element other elements use to depend on this element, more than
one `element`/`.grd file` can provide the same thing but the project.conf file
most make it explicit which one the project uses for a given configuration.

### Project context

The project specifies a set of contexts, the elements and then specify which
context to use. These contexts provide a number of plugin and environment variable
but can also set them on a project configuration basis.

### Plugin variables

The plugin variables are a map of `key` and `value` pairs that are past to the plugin.

The variables from the element are combined with the plugin variables and the
variables in the plugin context. And all passed to the plugin via yaml. The plugin
its self does the substitutions eg substituting the %{} patten

Some girderstream plugins use the `%{variable}` patten so that bst plugins can
be re-used.

#### Plugin Configuration

Girderstream does not have the plugin configuration section like Buildstream does.

To use Buildstream plugins in Girderstream plugin variables can be used. This is
done by matching plugin vaiable name to pattens. Girderstream its self does not
do this but some plugins do.

For instance the `config_` prefix is used to over ride a config `config_install-commands`

The `configA_` prefix is used to add onto the end of default configuration.

The `configP_` prefix is used to prepend onto the start of default configuration.

These are key value pairs just like all other plugin variables, un like bst
were plugin configuration is done with lists.

### The elements environment variables

These set the values of the environment variables in the sandbox, environment vars
are passed into the sandbox as is, eg before expanding `%{variable}` but the plugin
its self will update the environment variables as it does with plugin variables
and so expansion can be used like `%{variable}`. But only for the build and not the
plugin but only if using a suitable plugin.

### The elements sources

The sources bring in the source code of the software to be built. The will be
loaded into the `/source/` directory inside the build plugins sandbox

``` yaml
 - kind: git_source
   url: https://git.lysator.liu.se/nettle/nettle.git
   sha: ea4ea5e6904124b297ade0d1b182c5e530909765
```

The source must have a `kind` and this is used to find the source plugin as defined
in the `project.conf`. The other key value pairs are passed to this source plugin
and used to fetch the sources.

### The elements dependencies

The dependencies are a list of other elements needed to build and or run the plugin.

A dependency in a deployment element that needs the v4l-utils element may have

``` yaml
- dependency: v4l-utils
  scope: Build
  location: /targetfs/
  stage: Run
```

The `dependency` key is mandatory and has the name of the element required

The `scope` key sets when the dependency is required it defaults to BuildAndRun
and can be `Build`, `Run` or `BuildAndRun`.

The `location` key sets where in the sandbox this element and its staged dependencies
should be placed in the sandbox defaulting to `/`. Note this is a valid option
if the `scope` is `Run`.

The `stage` key sets which dependencies will be staged into the sandbox when
the element is used to build. This is invalid if the `scope` is `run`. Valid options
are `Run` `Build`, `BuildAndRun` and `None`. This defaults to `None` for
all scopes types.

If a dependency has all its optional keys as defaults then you can just specify
it as the name. You can combine the dependencies list to have both just the name
and detailed dependencies.

``` yaml
depends:
- sdk
- dependency: tool_chain
  scope: Build
- dependency: v4l-utils
  scope: Build
  location: /targetfs/
  stage: Run
```

To add the `sdk`, `tool_chain` and `v4l-utils` elements.
