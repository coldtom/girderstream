# Girderstream Project Intro

Girderstream projects build software from sources. All of the input files are in
yaml and must uniquely define there sources so that the project can be repeatable.

The project is made up of at least:
  * Project configuration file (project.conf)
  * Element files describing each component (element.grd)

It may also contain:
  * A file to define remote caches (.remotes)

Girderstream uses the cas part of REAPI (Remote Execution API) to store its boot
straps and the contents of cached built elements. And the remote asset part of
REAPI to store identifiers of cached builds.

This section of the book starts with using the girderstream software tool:

- [Install](./install.md)
- [Running](./running.md)

Then covers the various files that make up a girderstream project:

- [The project](./project.md)
- [Elements](./elements.md)
- [Plugins](./plugins.md)

Concluding with covering the various files and concepts that make up a girderstream project:

- [Bootstrapping](./bootstrap.md)
- [Requires Provides Patten](./provides_requires.md)
- [Junctions](./junctions.md)
