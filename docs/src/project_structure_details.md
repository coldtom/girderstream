# Buildstream structure and goals and their friction points for many projects

This document is under construction

GirderStream is currently in the design/early implementation phase.  But can
be used to build fairly complex projects.

## Intro

Despite years of working with integration tools for work I still do not use them routinely
for my personal projects. I was thinking that I really should make more effort
to use them. Because they have a lot of advantages but so far I have not found
any that have sufficiently low costs to make there use worth while, in
my opinion. And the following lays out my thinking as to why I have not used
them and what is stopping me using them.

Firstly I really like buildstream and dislike but begrudgingly respect yocto. And
having recently used yocto for a rather cool project.
I have come to re enforce some of my views of the two. Namely that in many ways
buildstream is even more superior to yocto than I thought, And in some areas that
they both are not as good as they could be. But fundamentally there are several
use cases (that I and others have) that build stream in my opinion is unsuitable for.
And that despite my many issues with yocto I must admit that it is the tool that is most
suitable for these jobs by a long way.

## An aside on `Host isolation`

A side note at this point is to say that the one thing that I am most unimpressed
with both of them is there host isolation, I have had many issues with Yocto.
But buildstreams host isolation of its plugins can still course issues.

While bst has a good host isolation for build steps it uses host tools for sources
and its plugins are effectively "host tools". While all projects must be boot strapped
this does not necessitate that all source plugins need to be bootstrap-able.

## Element re-use

This is something that I talked to many maintainers and users of Buildstream about
over the years in a number of contexts. I have often talked about it in the context
of embedded development but I think the problem is repeated in many places.

Here I have tried to explain the clearest examples of this issue that will effect the most
users of any integration tool. But the same problem is repeated elsewhere. Eg The
Gnome-build-meta and fd-sdk issues with element re-use are well documented. [4]

### Numpy and Machine learning use case

Numpy is a python library that allows apps to have a simple python api but have the
mathematical operation be executed by specialist linear algebra library. Many
machine learning libraries use Numpy to provide interfaces that are easy to use
but with the powerful efficient implementation.

Numpy can be compiled ageist several different linger algebra libraries.

Numpy can be compiled with no LINPACK lib or OpenBLAS or MKL it also has
fortran tests that you may wish to build.

It can be build with a number of c compiles.

The standard acceleration libs are:

*    MKL
*    BLIS
*    OpenBLAS
*    ATLAS
*    BLAS (NetLIB)

But others libs which support the BLAS/LAPACK API's could be used

The numpy lib tries to auto detect the libs so the only change to the element
should be to change the dependency.

Different people will want to use different acceleration libs depending
on the scaling properties of there problem, reuse with parts of the project,
licensing, or other preferences.

In order for a project like libreML to support all the different use cases they
could have 5 different versions of numpy.bst. There are 10 elements which
depend on numpy in libreML at the current time.

This can get even worse things like pytouch and tensorflow which need to be
compiled for the specific cuda lib available on the target platform.

Cuda has a host of licensing requirements that can complicate matters further.

This means that any element that depends purely on the api provided by the apis
provided by numpy and pytourch would need many definitions changing only by
dependencies. It has been suggested by the authors of buildstream [1] that the
best way to support this case is for each project to fork or re-implement the
element

### OEM use case

This case that is repeated across a number of industries that I am aware of.
Is that of OEM's, original equipments manufacture, building products with
embedded computers where the computer is not meant to be part of the "idea" of
the product but the software that run on it is needed for the "idea" of the
product.

The OEM will buy components parts from supplies, known as "tier 1"s as
they are one step removed from the final product. The tier one will sell/supply
small computer(s) and some support equipment, and provide some drivers and
kernel(s) needed to run the OEMs applications on the hardware.

It should be noted that there may be 1-100+ separate computers in a single
product, and they could come from a range of tier 1's.

Typically the tier 1's supply a set of kernels and drivers for every separate
computer. And the OEM has per kernel middleware. But the OEM will try to have
the various different middleware expose the same app facing API. The OEM has
many kernels across the range of products, some times many per
product so many middlewares must be maintained but all apps see a
consistent api from the middleware.

The OEM has many apps that all build vs the same middleware api and so only
need a single recipe/element for each app that can be used in many builds.
but the other requirements of the system mean that the same recipe needs to be
build many times as each system may have subtly different ABI or libc's etc
so the element/recipe needs to be build for each case.

Some times the cases are simple enough that the same "app" can be built on top
of many different kernels and libc's and the kernel and libc are stable enough
that they provide this "middleware" in and of them selves and sometimes the
OEM has decades of IP invested in the middleware, but most in my experience
are somewhere between the two. But for cost reasons they strive to keep the
apps as consistent as possible, often the same build instructions work across
different architectures of CPU.

The need to build each identical recipe for each case lends its self to yocto
where, in yocto each app can have a single recipe, but in, buildstream every app
needs a new version for every build.

## Sharing Elements/Recipeise vs sharing caches/artifacts

To over simplify my understanding of the two projects:

  * Yocto allows for sharing recipes as easily as possible
    to the point that is its cached builds can not be
    reliably shared even withing the same project

  * Builstream explicitly [1] dis courages sharing elements
    but is very good at sharing artifacts within a project
    and if you happen to have exactly the same ABIs as the
    project then you may be able to use the artifact but you
    will have

One thing that is a huge advantage of bst over yocto is its
declarative yaml and effective cache keys. The ability to share
a cache between devs, testers and ci saves huge amounts of time
effort and compute.

But the current implementation and philosophy of [1] means that the
elements them selves can not be shared between project. Even closely
coupled projects like FD-SDK and Gnome-build-meta can not do this
in a way that the maintainers recommend [2] Indeed you still can not
override just the dependencies so ether elaborate element structures
must be used or some element duplication is still required.

After years of work developing and using buildstream and other similar
tools. I think that if we kept the strictly declarative yaml but
had a more relaxed dependency structure, with some mechanism
that used declarative yaml to declare `provide` and `require` mechanisms
we could have the best of both while limiting the difficulties of both.
I am not so naive as to not realize that this will make buildstream
more difficult but by using declarative yaml we can avoid many
of the pitfalls of yocto. And by having the strict cache keys we can
have projects share artifacts amongst them selves and probably even
with others in a lot of cases.

## General notes

There are some cases where I think bst works really well. It is clearly
excelling for the flatpak use case. And those with large numbers of
of servers with consistent hardware, even with heterogeneous software
across them can have good luck with bst.

But even in HPC environments with 1000's of very similar Xeons I have seen
scientific workloads that have software optimised to slightly different
Xeons compiled to take advantage of different instructions and deployed
to specific machines. Which mimics the above ML use case.

It has been suggested to me by Bst maintainers on a number of occasions that the
 way to
achieve a similar result is by having projects declare a plugin for
every element and then the project can have its own implementation of
each element and then every project that consumes the project create its
own elements implementing that plugin. I have many issues with this
not least the amount of time and effort and duplication.

I have no desire to personally try to maintain the number of elements and plugins
that this strategy would necessitate in my personal projects. And I don't imagine
clients with these sorts of use cases wanting to maintain such a large
matrix of elements and plugins that they would need.

## Final thoughts

And so I see a opportunity for a tool with declarative yaml AND graph
flexibility. And I would be interested if others also see this gap between
bst and yocto? And if others think that it would be valuable to address
this niche? It would seem the most sensible way to achieve this would be to work with
the bst community to add this functionality to buildstream.

## References

[1] While artifacts and plugins are things to be reused and shared among
projects, elements themselves are not, they are a project's mechanism
of declaring an 'Element' (instance of a plugin). 
https://mail.gnome.org/archives/buildstream-list/2020-May/msg00016.html

Apache thread discussing over-riding elements through a junction.
Tristan and Jurg both accept element over-rides but both say that
you should avoid it.
[2]  https://lists.apache.org/thread/x4kl5yy9m1jlhv3vs93dozjb09nn8bk7
[3]  https://lists.apache.org/thread/qdh9vmhstnjxogrcbl560pbng84v912s

Modifying/excluding elements from junctions
[4] https://mail.gnome.org/archives/buildstream-list/2020-May/msg00013.html

