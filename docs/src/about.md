# What is Girderstream

Girdersteam is a `Meta Build Tool` or an `Integration tool`. This means
it manages the building of software.

Girderstream is typically uses by people who ether want to build one piece
of software in a very controlled environment. Or who want to build a number
of pieces of software in combination in a manageable way.

Typically this may be for a `production` environment where people need to
incrementally improve the software but will take costly measures to avoid
introducing any inadvertent changes. Such requirements frequently arise in
regulated industries or highly available systems where down time or bugs are
very costly.

## How is it implemented

Girderstream is a rust cli program that takes a collection of input files, or
a `project`. And uses those inputs to generate build instructions to a REAPI
cas and worker to build some software.

Currently you need the girderstream binary, buildbox-casd and buildbox-worker to
build something with girderstream.

When you run `girderstream build` in a project girderstream will run until all the
elements have been built. Currently this would happen in your local buildbox-worker
but in the future options for it to use a REAPI server will be added. If the
elements have previously been built and are in cache then the build will stop
without building.

## What is a girderstream `project`

A girderstream project must have a `project.conf` file. The project.conf file is in
yaml and tells girderstream how to find all the other files in the project.
All the steps of the build take place in a sandbox so we start with an import
into the sandbox to "bootstrap" the project. This could just be a import of the
standard development environment of a project and must contain everything needed
to fetch and build everything else (sometimes indirectly, hence bootstrap).

Each unit of work in girderstream is a `.grd` file and each .grd file can depend
on other `.grd` files. When a .grd file changes it will invalidate the cached
build of other elements that depend on it but elements not effected will not remain
cached.

Girderstream takes data in structured decorative input files, namely `.grd` and
`project.conf`. Currently that structure is yaml but as girderstream uses serde
for its de-serialization there is no reason why we could not support other similar
formats that also implement the serde traits. However yaml is a good language for
this task as it is declarative, reasonably easy for humans to edit and is fairly clear.

All work needed to build the software within a element will be built inside the
sandbox and the relevant sources will be fetched from within the sandbox but as
a separate preliminary steps. The sandbox has access to the internet when fetching
but not when building.
