# Bootstrap

Bootstrapping is a important part of allowing integration tools to produce reproducible results independent of the
operating system that is running the integration tool.

Most integration tools import a bootstrap or base tool chain to start the build process with.

This doc shows some basics of how todo this in girderstream and then talks a little about the more general problems of bootstrapping
in general and how girderstream allows you to create sensible projects taking these factors into consideration.

## Girderstream bootstrap basics

Girderstream projects typically use the built in `bootstrap_import` build plugin and the
built in `cas` source element to create very simple import elements to import both
the, bootstrap or base, tool chain and the, build and source, plugins.

Once a project has bootstrap element(s) the project can use them as or to build
plugins to be used to fetch all the other sources and build them.

This ensures that the operation of the plugins is repeatable.

Girderstreams boot strap elements can look like the following:

``` yaml
kind: bootstrap_import

sources:
- kind: cas
  digest: 6d6dabc8d9d6fbcd1df3ce5e5aca67c627a0e9d5250930ea04e4ac557941b4fc/532
```

## Bootstrap considerations

Boot strapping a integration project typically involves going from a set of build elements that describe how to build
software to having a basic set of actual pieces of software that are consistent no matter where you run your integration
tool and that then let the rest of you build instructions to be build consistently from this base layer of software.

This section talks about software development kits, SDK, these are typically a set of different compilers and libraries
that are used to build software. SDKs are a for runner to integration tools, they package to gether a group of compilers
and library's and allow you to refer to a exact set of versions for each of there components by a single identifier, typically
the version of the sdk and they then provide a easy way to install all these different components in one go.

A integration tools go further than sdks, they allow you to use one tool to build or acquire your sdk, the sources
of the pieces of software you want to build from your sdk and then build these pieces in the right order to ensure the libs
you need are build before the binaries etc and then also assemble these varies build software components into packages.
By packages i mean anything from a fragment of a system like a deb or a hole operating system image.

The base layer of software that your bootstrap gets you too may well be called your sdk. Or you may say that there is a
core part of your sdk from which you then build the rest of your sdk. 

All of the terms like bootstrap, sdk, package, are highly over loaded, meaning they have different meanings
and expectations around them depending on which community you are talking to.

## Bootstrapping a girderstream project

When setting up a girderstream project you can bootstrap your project your self. You can find some base binaries to build
your sdk on or you can import a full sdk your self.

You could also reuse a existing projects bootstrap. In the buildstream community many project build upon the freedesktop-sdk
project using it as there bootstrap, but not all, and in yocto community the poky project is often used as the base sdk.

At the time of writing girderstream does not have a equivalent to the freedesktop-sdk project but it does have a import
project of the free desktop-sdk, this can be used as a bootstrap of girderstream projects but debian chroots have also
been successfully used also.

A difference between buildstream and girderstream is that every girderstream project needs a little boostrap to be used
to run its plugins so that is plugins are consistent but that build stream does not have these because the equivalent
functionality comes from the host.

The small plugin bootstrap element looks very similar to any other bootstrap element and can come from the sdk that you are
using so it is generally a small detail. Due to girderstreams compatibility you typically end up with far fewer elements
in a girderstream project that uses a junction to a sdk than a buildstream project as you can reuse packaging elements
that can not be reused in buildstream.
