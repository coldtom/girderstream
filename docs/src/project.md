# Project

The project configuration file can contain:
  * Build Plugin definitions
  * Source Plugin definitions
  * Build contexts
  * Project configuration options
  * Project name
  * The folder containing all the element definitions
  * Rules for resolving which elements provide what from the given project
    configuration options.

A nominal example is given below:

``` yaml
project_name: ws-sdk

element-path: elements

configuration:
  bsp:
      default: pi
      options: [ name: pi, name: x86, name: aarch64 ]

contexts:
- name: build_target
  plugin_variables:
    install-root: /output_dir/
    extra_arch_busy:
      - value: ARCH=arm64
        when: bsp=pi
      - value: ARCH=arm64
        when: bsp=aarch64
      - value: ARCH=x86
        when: bsp=x86

  environment_variables:
    PATH:
      - value: /plugins:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/var/lib/snapd/snap/bin:/usr/lib/sdk/toolchain-aarch64/bin
        when: bsp=pi
      - value: /plugins:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/var/lib/snapd/snap/bin:/usr/lib/sdk/toolchain-aarch64/bin
        when: bsp=aarch64
      - value: /plugins:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/var/lib/snapd/snap/bin
        when: bsp=x86

provides_rules:
- provision: kernel
  provider: kernel_pi.grd
  when:
    bsp: pi
- provision: kernel
  provider: kernel_x86.grd
  when:
    bsp: x86
- provision: kernel
  provider: kernel_aarch64.grd
  when:
    bsp: aarch64

source_plugins:
- kind: git_source
  cli: |
    echo "nameserver 8.8.8.8" > "/etc/resolv.conf"
    mkdir /output_dir/; plugin_git %{plugin_vars}; rm -rf /output_dir/.git;
    ls -la /; ls -la /output_dir;
  depends:
  - tool_chain
  - plugin_git

build_plugins:
- kind: autotools
  depends:
  - tool_chain
  - build_plugin_bst
  cli: |
    mkdir -p /sources/build
    mkdir -p /output_dir/
    cd /sources/
    girderstream_build_plugin_bst -v /plugin_vars.yaml -p /autotools.yaml
    chmod u+x configured.sh
    ./configured.sh
- kind: bootstrap_manual_min
  depends:
  - build_plugin_bst

```

## Detailed descriptions

### Project name

The `Project_name` key is used to identify the project.

### Elements path

The `elements_path` optionally spesifies the location of the folder which
contains the elements, eg the `.grd` files. The default is `elements`

### Description

The `description` gives some information about the project and is used
to describe the project.

### Source Plugins

The source plugins are use to fetch elements source code. They are described
in the `project.conf` but they are combined with the details from element
who's source they are fetching, before doing the fetch. The properties specified
here are generic to all element.

In the project.conf the `kind`, `dependencies` and optionally the `cli` are given
for each source plugin.

The `kind` is used to specify this plugin in the element. The `kind` specified
here must match the `kind` in the elements sources list.

The `dependencies` set the elements used to fetch the source. Note that
when a element specifies this plugin in its source then the `dependencies`
of this plugin are added to that element. This might mean that elements used
to build the git element might need to use a bootstrap version of git.

### Build Plugins

The build plugins are use to build the elements source code. They are described
in the `project.conf` but they are combined with the details from element
who they are building, before doing the build. The properties specified
here are generic to all element.

In the project.conf the `kind`, `dependencies` and optionally the `cli` are given
for each build plugin.

The `kind` is used to specify this plugin in the element. The `kind` specified
here must match the `kind` in the elements kind.

The `dependencies` set the elements used to fetch the source. Note that
when a element specifies this plugin in its source then the `dependencies`
of this plugin are added to that element. This might mean that elements used
to build the autotools element might need to use a bootstrap version of autotools.

### Provides map

Girderstream allows more than one `element.grd` file to provide the same
piece of software, the project then pics which to use based on the options and the
`provides_rules`. The project must be explicit about which to use, if you have two
provides for the same element then girderstream will fail to load the project.

The `provides_rules` block is used to determine which element to use to provide
a particular peice of software.

For example we might want to use a differnt kernel for our platform based on the
bsp option. eg:

``` yaml
provides_rules:
- provision: kernel
  provider: kernel_pi.grd
  when:
    bsp: pi
- provision: kernel
  provider: kernel_x86.grd
  when:
    bsp: x86
- provision: kernel
  provider: kernel_aarch64.grd
  when:
    bsp: aarch64
```

Or we might want to have different OpenBLAS implementations to build Numpy on top
of. Numpy can be build on top of about 5 different OpenBLAS implementations, some
of which require specific GPUs and some of which have restrictive license. And some
which are optimized for dense or sparse matrix's.

### Configuration

The Configuration section defines the different configuration options available
for the users to chose. These must also match the `provides_map` and the options
used in the contexts.

``` yaml
configuration:
  bsp:
      default: pi
      options: [ name: pi, name: x86, name: aarch64 ]
```

### Context

The contexts allow the elements to load in groups of preset plugin and environment
variables. The value of these variables can also be a function of configuration
options.

An example of multiple contexts

``` yaml
contexts:
- name: build_toolchain
  plugin_variables:
    plugin_var1: tomarto
  environment_variables:
    PATH: /usr/bin
- name: build_target
  plugin_variables:
    install-root: beans
  environment_variables:
    PATH: /bin/
```

These can then be used in a element

A element can set the build sandbox's PATH to `/usr/bin` by using the
`build_toolchain` context with `context: build_toolchain`.

A element can set the build sandbox's PATH to `/bin` by using the
`build_target` context with `context: build_target`.
