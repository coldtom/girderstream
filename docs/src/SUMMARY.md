# Summary

# What is Girderstream

- [About Girderstream](./about.md)
- [Principles of Girderstream](./principles.md)

# Using Girderstream

- [Intro](./using_intro.md)
- [Install](./install.md)
- [The project](./project.md)
- [Elements](./elements.md)
- [Plugins](./plugins.md)
- [Running](./running.md)
- [Bootstrapping](./bootstrap.md)
- [Requires Provides patten](./provides_requires.md)
- [Junctions](./junctions.md)

# Why Girderstream

- [Why girderstream Short](./why_girderstream.md)
- [Why girderstream Long - Structure](./project_structure_details.md)
