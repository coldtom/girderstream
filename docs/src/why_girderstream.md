# Why Girderstream

GirderStream is currently in the design/early implementation phase.  But can
be used to build fairly complex projects.

### Who is this document for?

Anyone interested in why I think we need yet another integration tool.

Who is it not for, people what want to understand the deep technical details.
Or who wish to pick holes in my some what simplified arguments. Or anyone
interested in a deep dive of the details, the details are however provided
in the next chapter.

## What is Girderstream for?

Girderstream is a integration tool. This means that it takes a number
of different software projects and combines them in a sensible manner
into a coherent hole. For instance a full disk image that can be flashed
on to a embedded computer. Or a desktop environment where a compositor,
file explorer, etc must all be built to compatible versions.

## What is the current state of the art?

There are many tools that can do some of this but the 3 tools that do this
well and can be used for a range of projects are buildroot, yocto and buildstream.

## Why are these not good enough?

### What we need from these tools?

These are BIG projects so we need to be able to share what we create
between runs and within teams. Yocto and buildroot struggle with this.

These are complex projects we wish to share the parts of the project aimed
at at target platform between different projects that all have the same target
but maybe have different apps. Or we may wish to share the same integration
of libs and apps over different targets. Buildstream struggles with this.

We may wish to build the project many years later all the tools struggle with this.

### How big a deal are these defects you wish to address?

Yocto has the ablity to combine many different apps and target requirements with
bsps in a way that is very useful but it does not track its inputs or isolate the
build from the host which means that what it builds can not be shared.

Buildstream is very careful to track most of its inputs to ensure that if something
changes it is reflected in the builds key. And it isolates most of its build from
the host. These steps mean it has confidence in its cache keys. This allows artifacts
to be shared with confidence. For projects with limited targets this works
very well, eg. Flatpak and Freedesktop-SDK.

Buildstream has a fixed dependency graph which severely limits they users ability to
reuse integrations. Interface the project maintainers state that they only support
sharing the build outputs not the integration instruction them selves. This makes
it unsuitable for any project where there is a requirement to have multiple similar
elements that in yocto would be a small number of elements that could be combined
at run time, due to the combinatorial explosion that this creates.

## Why is girderstream different?

We have more isolation and more explicit versioning of plugins.

We will be very careful to have cache keys that are a function of ALL of there inputs
like buildstream does but we will add some flexibility in the way you can combine
elements.

Combining elements is tricky, we can see that yocto is very flexible but buildstream
is not flexible enough. We will try to find a middle ground but we may fail.

## Is it worth it? It seems like a lot of effort for marginal gains?

These reasons on first glance may not seem to justify a hole new tool however:

I recently worked on a client yocto project, i regularly had to re build from scratch after
yocto corrupted its own local cache. This meant that tasks that i can do in half a day
in buildstream tuck 2 or more days in yocto. This adds up fast and when you multiply
this over a team it can start to get in to BIG amounts of time wasted.

But buildstream is not suitable due to the comminatory explosion mentioned above, taking
a typical Manufacturer who could have 5 or 6 active products each with 2 or 3 skews of each
could end up managing 10-30 forks of there BaseSDK. And while buildstream would let them
build these very quickly, a huge part of the software life cycle is maintenance so
this clients would end up with a unacceptable maintenance burden.

So yes these are subtle changes from what buildstream does now but they seem worth it to
me.
