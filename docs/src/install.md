# Installing Girderstream

Girderstream is a rust program that uses the REAPI protocol to build and store its projects.

## Running Girderstream

To run the most basic functionality of the Girderstream binary you need only supply the binaries
linked .so files. But to use the full range of functionality you will want to have
a local REAPI provider, eg buildbox.

### Building girderstream from source

Girderstream is fairly easy to build from source for a average linux user who already has rust and
buildbox installed. First get the latest version of the source files.

```
git clone https://gitlab.com/girderstream/girderstream.git
```

You would then use cargo to build it.

```
cargo install --path . --locked
```

However building all of buildbox can be a bit of a faff so getting a prebuilt
girderstream and buildbox is a good idea for most people. Please see the section
[using girderstream and buildbox from docker](#using-pre-build-girderstream-docker-image)
for prebuild binaries or the girderstream and buildbox READMEs for more detailed
building instructions.

### The binary

To run a Girderstream binary, built on a gnu linux system, you need to satisfy its
linked dependencies, on a linux system you would need a libc libgcc libm and the linux kernels
"virtual so", and a linker. All of these are present on a standard gnu linux system, eg debian,
fedora, arch or any other gnu system that can run the rust std lib. So long as these libs are at
least as new as the libc and kernel pressent when rustc built the Girderstream binary will be
portable across systems.

``` bash
$ ldd girderstream
	linux-vdso.so.1 (0x00007ffc8d314000)
	libgcc_s.so.1 => /lib64/libgcc_s.so.1 (0x00007fd29ebdc000)
	libm.so.6 => /lib64/libm.so.6 (0x00007fd29eafc000)
	libc.so.6 => /lib64/libc.so.6 (0x00007fd29dc23000)
	/lib64/ld-linux-x86-64.so.2 (0x00007fd29ec16000)
```

### To build a project

To prepare, build, cache or retrieve from a cache, girderstream requires a REAPI provider that implements
the standard Google REAPI standard AND the extended buildbox PROTOs for REAPI.

To build you will also need your local REAPI to have its build-workers run time requirements met.

Currently the only implementer of the extended buildbox PROTOs for REAPI is buildbox so you will probably
need buildbox installed locally and any of its run time dependencies for building, eg bubblewrap.

For instance when using fedora with your own dynamically compiled buildbox you would need:

``` bash
$ dnf install -y fuse3-devel \
        fuse3-libs glog grpc grpc-cpp bubblewrap
```

If you have a static buildbox you would only need `bubblewrap`

### To push or pull to a remote cache

To push or pull to a remote cache, the remote cache needs to implement the standard REAPI PROTOs and does
not need the extended buildbox PROTOs but does need the artifact PROTOs from the latest versions of the
google REAPI PROTOs.

### Remote execution

Girderstream does use the REAPI to talk to buildbox for local builds but does not fully support remote
execution to build on a buildbarn etc cluster but patches to allow this would be simple and welcome.

## Using pre build Girderstream docker image

You can fairly easily build Girderstream and buildbox from source, the respective projects READMEs have ample
documentation for this.

Helpfully the girderstream project builds and publishes docker images containing all the run time deps for
pulling, building and pushing a girderstream project:

You can pull the docker images:

``` bash
docker pull registry.gitlab.com/girderstream/docker-images/girderstream-ci:latest
```

And then use it as follows:

```bash
$ cd /your/girderstream_project/src/
$ docker run -it -v `pwd`:/project:z -v ~/.cache/girderstream/docker:/tmp/cas:z --privileged --workdir /project/ registry.gitlab.com/girderstream/docker-images/girderstream-ci:latest
# buildbox-casd --bind localhost:50040 /tmp/cas &
# girderstream --help
# girderstream -v show --name demo_app
```

Please see the [running girderstream page](running.md) for easy and more complex examples.  
