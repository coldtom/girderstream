// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
use std::{collections::HashMap, fs::File, io::Write, path::PathBuf, str::from_utf8, sync::Arc};

use girderstream::{misc::copy_dir_all, project, show, Crapshoot};
use reapi_tools::{test_infra::CasdWrapper, FullConfig, LocalConfig};
use tempfile::tempdir;
use tokio::process::Command;

#[tokio::test]
async fn test_load_project_config1() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();

    let original_project_root = PathBuf::from("tests/load_tests");

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_junction = tmp_dir_test_path.join("junc");

    copy_dir_all(&original_project_root, &tmp_dir_project)?;
    copy_dir_all(
        original_project_root.join("files/junction1"),
        &tmp_dir_junction,
    )?;
    let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_junction.display()))
        .output()
        .await
        .unwrap();

    println!("upload: {:?}", upload);

    let full_config = FullConfig::new(config, vec![]);
    let mut project =
        project::project::Project::new_from_dir(&full_config, tmp_dir_project, None, true, true)
            .await?;

    let result1 = project.get_element(&Arc::from("app")).await?;
    let result2 = project.get_element(&Arc::from("app.grd")).await?;
    assert!(result1.eq_element(&result2));

    let libb_result = project.get_element(&Arc::from("libb")).await?;
    assert!(libb_result.get_sources()[0]
        .get("digest")
        .unwrap()
        .starts_with("libb1"));

    casd_wrapper.close_with_future().await?;
    Ok(())
}

#[tokio::test]
async fn test_show_project_config1() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();

    let original_project_root = PathBuf::from("tests/load_tests");

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_junction = tmp_dir_test_path.join("junc");

    copy_dir_all(&original_project_root, &tmp_dir_project)?;
    copy_dir_all(
        original_project_root.join("files/junction1"),
        &tmp_dir_junction,
    )?;
    let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_junction.display()))
        .output()
        .await
        .unwrap();

    println!("upload: {:?}", upload);

    let full_config = FullConfig::new(config, vec![]);
    let mut project =
        project::project::Project::new_from_dir(&full_config, tmp_dir_project, None, true, true)
            .await?;

    let result1 = project.get_element(&Arc::from("app")).await?;
    let result2 = project.get_element(&Arc::from("app.grd")).await?;
    assert!(result1.eq_element(&result2));

    let app_result = project.get_element(&Arc::from("app")).await?;

    let mut buffer = Vec::new();
    show::<project::element::ElementDisplayLong>(
        &mut buffer,
        &mut full_config.get_local().to_owned().into(),
        &project,
        &app_result,
    )
    .await?;
    let output_text = from_utf8(&buffer).unwrap();
    assert!(output_text.contains("app"));
    assert!(output_text.contains("Key s"));

    let mut buffer = Vec::new();
    show::<project::element::ElementDisplayStd>(
        &mut buffer,
        &mut full_config.get_local().to_owned().into(),
        &project,
        &app_result,
    )
    .await?;
    let output_text = from_utf8(&buffer).unwrap();
    assert!(output_text.contains("app"));
    assert!(output_text.contains("Key"));

    casd_wrapper.close_with_future().await?;
    Ok(())
}

#[tokio::test]
async fn test_load_project_junctioned_element() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();

    let original_project_root = PathBuf::from("tests/load_tests");

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_junction = tmp_dir_test_path.join("junc");

    copy_dir_all(&original_project_root, &tmp_dir_project)?;
    copy_dir_all(
        original_project_root.join("files/junction1"),
        &tmp_dir_junction,
    )?;
    let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_junction.display()))
        .output()
        .await
        .unwrap();

    println!("upload: {:?}", upload);

    let full_config = FullConfig::new(config, vec![]);
    let mut project =
        project::project::Project::new_from_dir(&full_config, tmp_dir_project, None, true, true)
            .await?;

    let _result1 = project.get_element(&Arc::from("junc")).await?;
    casd_wrapper.close_with_future().await?;
    Ok(())
}

#[tokio::test]
async fn test_load_project_config2_param() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();

    let original_project_root = PathBuf::from("tests/load_tests");

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_junction = tmp_dir_test_path.join("junc");

    copy_dir_all(&original_project_root, &tmp_dir_project)?;
    copy_dir_all(
        original_project_root.join("files/junction1"),
        &tmp_dir_junction,
    )?;
    let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_junction.display()))
        .output()
        .await
        .unwrap();

    println!("upload: {:?}", upload);

    let full_config = FullConfig::new(config, vec![]);

    let mut config = HashMap::new();
    config.insert("set".to_string(), "set2".to_string());
    let mut project = project::project::Project::new_from_dir(
        &full_config,
        tmp_dir_project,
        Some(config),
        true,
        true,
    )
    .await?;

    let result1 = project.get_element(&Arc::from("app")).await?;
    let result2 = project.get_element(&Arc::from("app.grd")).await?;
    assert!(result1.eq_element(&result2));

    let libb_result = project.get_element(&Arc::from("libb")).await?;
    assert!(libb_result.get_sources()[0]
        .get("digest")
        .unwrap()
        .starts_with("libb2"));

    casd_wrapper.close_with_future().await?;
    Ok(())
}

#[tokio::test]
async fn test_load_project_config2_project() -> Crapshoot {
    let original_project_root = PathBuf::from("tests/load_tests");

    let temp_dir = tempdir()?;

    copy_dir_all(original_project_root, &temp_dir)?;
    let temp_dir_path = temp_dir.into_path();

    let mut file = File::create(temp_dir_path.join("project.conf"))?;
    file.write_all(
        b"project_name: Basic Example

source_plugins:
- kind: test_source
  depends:
  - stage0
  - test_source

build_plugins:
- kind: test_build
  depends:
  - stage0


provides_map:
  libA1: liba

configuration:
  set:
      default: set2
      options: [ name: set1, name: set2 ]

provides_rules:
- provision: libb
  provider: libB1.grd
  when:
     set: set1
- provision: libb
  provider: libB2.grd
  when:
     set: set2
",
    )?;
    file.flush().unwrap();
    let config = LocalConfig::new("http://localhost:50055".to_string(), "".to_string());
    let full_config = FullConfig::new(config, vec![]);
    let mut project =
        project::project::Project::new_from_dir(&full_config, temp_dir_path, None, false, false)
            .await?;

    let libb_result = project.get_element(&Arc::from("libb")).await?;
    assert!(libb_result.get_sources()[0]
        .get("digest")
        .unwrap()
        .starts_with("libb2"));

    Ok(())
}
