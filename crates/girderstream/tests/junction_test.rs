// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
use std::{fs::create_dir_all, path::PathBuf, sync::Arc};

use girderstream::{
    misc::copy_dir_all,
    project::{self},
    scheduler, Crapshoot,
};
use reapi_tools::{test_infra::CasdWrapper, FullConfig, LocalConfig};
use tokio::process::Command;

// test that we can load a element over a junction
#[tokio::test]
async fn test_load_by_source() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_extract = tmp_dir_test_path.join("extract");
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let original_project_root = PathBuf::from("tests/junction_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    create_dir_all(&tmp_dir_extract).expect("Could not make dir for testing");
    let extract = Command::new("tar")
        .arg("-xf")
        .arg("tests/build_tests/files/busybox.tar.xz")
        .arg("-C")
        .arg(format!("{}", tmp_dir_extract.display()))
        .output()
        .await
        .unwrap();

    println!("Extract: {:?}", extract);

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_extract.join("busybox2").display()))
        .output()
        .await
        .unwrap();

    println!("Upload: {:?}", upload);

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!(
            "{}",
            tmp_dir_project.join("files").join("junction1").display()
        ))
        .output()
        .await
        .unwrap();

    println!("Upload junc1: {:?}", upload);

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!(
            "{}",
            tmp_dir_project.join("files").join("junction2").display()
        ))
        .output()
        .await
        .unwrap();

    println!("Upload junc2: {:?}", upload);

    println!("About to setup project");

    let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
    let full_config = FullConfig::new(config, vec![]);

    let mut project = project::project::Project::new_from_dir(
        &full_config,
        tmp_dir_project.to_path_buf(),
        None,
        true,
        true,
    )
    .await?;

    println!("Fully loaded project");
    let build_element = project.get_element(&Arc::from("cross_junc")).await?;
    {
        let locked_project = Arc::new(project);
        scheduler::build(&full_config, &build_element, locked_project.clone(), true).await?;
        println!("Built build");

        //target_element.checkout(&mut full_config.get_local().to_owned().into(), &locked_project, tmp_dir_str ).await?;
        build_element
            .checkout(
                &mut full_config.get_local().to_owned().into(),
                &locked_project,
                &format!("{}", tmp_dir_checkout.display()),
                false,
                false,
            )
            .await?;
    }

    casd_wrapper.close_with_future().await?;
    Ok(())
}

// Todo check that a souce plugin can be loaded across junction

// Todo check that a build plugin can be loaded across junction

// Check that two jucntions can be loaded and there element dep on each other
#[tokio::test]
async fn test_load_over_two_juncs() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_extract = tmp_dir_test_path.join("extract");
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let original_project_root = PathBuf::from("tests/junction_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    create_dir_all(&tmp_dir_extract).expect("Could not make dir for testing");
    let extract = Command::new("tar")
        .arg("-xf")
        .arg("tests/build_tests/files/busybox.tar.xz")
        .arg("-C")
        .arg(format!("{}", tmp_dir_extract.display()))
        .output()
        .await
        .unwrap();

    println!("Extract: {:?}", extract);

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_extract.join("busybox2").display()))
        .output()
        .await
        .unwrap();

    println!("Upload: {:?}", upload);

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!(
            "{}",
            tmp_dir_project.join("files").join("junction1").display()
        ))
        .output()
        .await
        .unwrap();

    println!("Upload junc1: {:?}", upload);

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!(
            "{}",
            tmp_dir_project.join("files").join("junction2").display()
        ))
        .output()
        .await
        .unwrap();

    println!("Upload junc2: {:?}", upload);

    println!("About to setup project");

    let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
    let full_config = FullConfig::new(config, vec![]);

    let mut project = project::project::Project::new_from_dir(
        &full_config,
        tmp_dir_project.to_path_buf(),
        None,
        true,
        true,
    )
    .await?;

    println!("Loaded project");

    let build_element = project.get_element(&Arc::from("built_with_junc")).await?;
    dbg!(&project);
    {
        let locked_project = Arc::new(project);
        scheduler::build(&full_config, &build_element, locked_project.clone(), true).await?;
        println!("Built build");

        build_element
            .checkout(
                &mut full_config.get_local().to_owned().into(),
                &locked_project,
                &format!("{}", tmp_dir_checkout.display()),
                false,
                false,
            )
            .await?;
    }

    casd_wrapper.close_with_future().await?;
    Ok(())
}
