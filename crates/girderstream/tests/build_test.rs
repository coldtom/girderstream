// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
use std::{collections::HashMap, fs::create_dir_all, path::PathBuf, sync::Arc};

use girderstream::{misc::copy_dir_all, project, scheduler, Crapshoot};
use reapi_tools::{test_infra::CasdWrapper, FullConfig, LocalConfig};
use tokio::{
    fs::File,
    io::{AsyncReadExt, AsyncWriteExt},
    process::Command,
};

fn get_test_bin_dir() -> std::path::PathBuf {
    // Cargo puts the integration test binary in target/debug/deps
    let current_exe =
        std::env::current_exe().expect("Failed to get the path of the integration test binary");
    let current_dir = current_exe
        .parent()
        .expect("Failed to get the directory of the integration test binary");

    let test_bin_dir = current_dir
        .parent()
        .expect("Failed to get the binary folder");
    test_bin_dir.to_owned()
}

/// This uses the public api of the girderstream lib to build a project
///
/// This is less complete of a end to end test as the similar one that
/// uses girderstream its self. But it lest us make a number of assertions
/// as we build the project which will give us a lot more options to catch
/// issues and hopefully give more helpful diagnostics  if a change brakes
/// something.
#[tokio::test]
async fn test_build_by_parts() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_extract = tmp_dir_test_path.join("extract");
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    create_dir_all(&tmp_dir_extract).expect("Could not make dir for testing");
    let extract = Command::new("tar")
        .arg("-xf")
        .arg("tests/build_tests/files/busybox.tar.xz")
        .arg("-C")
        .arg(format!("{}", tmp_dir_extract.display()))
        .output()
        .await
        .unwrap();

    println!("extract: {:?}", extract);

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_extract.join("busybox2").display()))
        .output()
        .await
        .unwrap();

    println!("upload: {:?}", upload);

    println!("about to setup project");

    let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
    let full_config = FullConfig::new(config, vec![]);

    let mut project = project::project::Project::new_from_dir(
        &full_config,
        tmp_dir_project.to_path_buf(),
        None,
        false,
        false,
    )
    .await?;
    let stage0_element = project.get_element(&Arc::from("stage0")).await?;
    let build_element = project.get_element(&Arc::from("basic_build")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(&full_config, &stage0_element, locked_project.clone(), false).await?;
    println!("built stage0");

    scheduler::build(&full_config, &build_element, locked_project.clone(), false).await?;
    println!("built build");

    build_element
        .checkout(
            &mut full_config.get_local().to_owned().into(),
            &locked_project,
            &format!("{}", tmp_dir_checkout.display()),
            false,
            false,
        )
        .await?;
    println!("Checked out");

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("test_source_second.output"))
        .await
        .expect("Could not open file 'test_source_second.output'")
        .read_to_end(&mut buff)
        .await
        .expect("Could not read from file 'test_source_second.output'");
    let content = String::from_utf8_lossy(&buff);
    assert!(content.contains("non empty file"));

    let build_result = build_element
        .get_build_artifact(
            &mut full_config.get_local().to_owned().into(),
            &locked_project,
        )
        .await?;
    let stdout = build_result
        .stdout(&mut full_config.get_local().to_owned().into())
        .await?;
    assert!(stdout
        .expect("Build did not have a stdout")
        .contains("message to stdout"));

    let stderr = build_result
        .stderr(&mut full_config.get_local().to_owned().into())
        .await?;
    assert!(stderr
        .expect("Build did not have a stderr")
        .contains("message to stderr"));

    println!("finished just drop casd");
    casd_wrapper.close_with_future().await?;
    drop(casd_wrapper);

    Ok(())
}

/// This test checks that Girderstream its self can build a project
///
/// This is a more complete integration test as it uses Girderstream
/// as it is but it does not give as much information about why things
/// might have gone wrong and does not let use make assertions about
/// internal state at any points throughout the build.
#[tokio::test]
async fn test_build_by_tool() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();

    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");
    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_extract = tmp_dir_test_path.join("extract");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    create_dir_all(&tmp_dir_extract).expect("Could not make dir for testing");
    let extract = Command::new("tar")
        .arg("-xf")
        .arg("tests/build_tests/files/busybox.tar.xz")
        .arg("-C")
        .arg(format!("{}", tmp_dir_extract.display()))
        .output()
        .await
        .unwrap();

    println!("extract: {:?}", extract);

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_extract.join("busybox2").display()))
        .output()
        .await
        .unwrap();
    if !upload.status.success() {
        panic!("Could not upload busybox to cas");
    }
    println!("upload: {:?}", upload);

    println!("about to build project");

    let girderstream_binary = get_test_bin_dir().join("girderstream");
    let girderstream_stage0 = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(casd_wrapper.get_address())
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("build")
        .arg("--name")
        .arg("stage0")
        .output()
        .await
        .unwrap();
    assert!(girderstream_stage0.status.success());

    println!("Girderstream stage0: {:?}", girderstream_stage0);
    println!("{}", String::from_utf8_lossy(&girderstream_stage0.stdout));
    println!("{}", String::from_utf8_lossy(&girderstream_stage0.stderr));

    let girderstream_build = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(casd_wrapper.get_address())
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("build")
        .arg("--name")
        .arg("basic_build")
        .output()
        .await
        .unwrap();
    assert!(girderstream_build.status.success());

    println!("Girderstream basic_build: {:?}", girderstream_build);
    let girderstream_checkout = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(casd_wrapper.get_address())
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("checkout")
        .arg("--name")
        .arg("basic_build")
        .arg("--location")
        .arg(format!("{}", tmp_dir_checkout.display()))
        .output()
        .await
        .unwrap();

    println!("Girderstream checkout: {:?}", girderstream_checkout);
    assert!(girderstream_checkout.status.success());
    // why do we need this? grd's checkout should wait till its finished.

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("test_source_second.output"))
        .await
        .unwrap_or_else(|_| panic!("Could not open"))
        .read_to_end(&mut buff)
        .await?;
    let content = String::from_utf8_lossy(&buff);
    assert!(content.contains("non empty file"));

    casd_wrapper.close_with_future().await?;
    Ok(())
}

/// This test checks that Girderstream can pull a cas import source
#[tokio::test]
async fn test_build_with_pull_cas() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let mut casd_remote_wrapper = CasdWrapper::start_casd()?;

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_extract = tmp_dir_test_path.join("extract");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    File::create(tmp_dir_project.join(".remotes"))
        .await
        .unwrap()
        .write_all(
            format!(
                "remotes:
    - url: \"http://localhost:{}\"
      instance: \"\"
      push: false
    ",
                casd_remote_wrapper.get_port()
            )
            .as_bytes(),
        )
        .await?;

    create_dir_all(&tmp_dir_extract).expect("Could not make dir for testing");
    let extract = Command::new("tar")
        .arg("-xf")
        .arg("tests/build_tests/files/busybox.tar.xz")
        .arg("-C")
        .arg(format!("{}", tmp_dir_extract.display()))
        .output()
        .await
        .unwrap();

    println!("extract: {:?}", extract);

    let upload = Command::new("casupload")
        .arg(casd_remote_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_extract.join("busybox2").display()))
        .output()
        .await
        .unwrap();
    if !upload.status.success() {
        panic!("Could not upload busybox to cas");
    }
    println!("upload: {:?}", upload);

    println!("about to build project");

    let girderstream_binary = get_test_bin_dir().join("girderstream");
    let girderstream_stage0 = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(casd_wrapper.get_address())
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("build")
        .arg("--name")
        .arg("stage0")
        .output()
        .await
        .unwrap();
    assert!(girderstream_stage0.status.success());

    println!("Girderstream stage0: {:?}", girderstream_stage0);
    println!("{}", String::from_utf8_lossy(&girderstream_stage0.stdout));
    println!("{}", String::from_utf8_lossy(&girderstream_stage0.stderr));

    let girderstream_build = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(casd_wrapper.get_address())
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("build")
        .arg("--name")
        .arg("basic_build")
        .output()
        .await
        .unwrap();
    assert!(girderstream_build.status.success());

    println!("Girderstream basic_build: {:?}", girderstream_build);
    let girderstream_checkout = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(casd_wrapper.get_address())
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("checkout")
        .arg("--name")
        .arg("basic_build")
        .arg("--location")
        .arg(format!("{}", tmp_dir_checkout.display()))
        .output()
        .await
        .unwrap();

    println!("Girderstream checkout: {:?}", girderstream_checkout);
    assert!(girderstream_checkout.status.success());
    // why do we need this? grd's checkout should wait till its finished.

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("test_source_second.output"))
        .await
        .unwrap_or_else(|_| panic!("Could not open"))
        .read_to_end(&mut buff)
        .await?;
    let content = String::from_utf8_lossy(&buff);
    assert!(content.contains("non empty file"));

    casd_wrapper.close_with_future().await?;
    casd_remote_wrapper.close_with_future().await?;
    Ok(())
}

/// This test checks that Girderstream can push and then pull down a cached
/// successful build.
#[tokio::test]
async fn test_build_with_push_pull() -> Crapshoot {
    let mut casd_first_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_first_wrapper.get_temp_root();
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let mut casd_remote_wrapper = CasdWrapper::start_casd()?;
    let mut casd_second_wrapper = CasdWrapper::start_casd()?;

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_extract = tmp_dir_test_path.join("extract");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    File::create(tmp_dir_project.join(".remotes"))
        .await
        .unwrap()
        .write_all(
            format!(
                "remotes:
    - url: \"{}\"
      instance: \"\"
      push: true
    ",
                casd_remote_wrapper.get_address()
            )
            .as_bytes(),
        )
        .await?;

    create_dir_all(&tmp_dir_extract).expect("Could not make dir for testing");
    let extract = Command::new("tar")
        .arg("-xf")
        .arg("tests/build_tests/files/busybox.tar.xz")
        .arg("-C")
        .arg(format!("{}", tmp_dir_extract.display()))
        .output()
        .await
        .unwrap();

    println!("extract: {:?}", extract);

    let upload = Command::new("casupload")
        .arg(casd_first_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_extract.join("busybox2").display()))
        .output()
        .await
        .unwrap();
    if !upload.status.success() {
        panic!("Could not upload busybox to cas");
    }
    println!("upload: {:?}", upload);

    println!("about to build project");

    let girderstream_binary = get_test_bin_dir().join("girderstream");
    let girderstream_stage0 = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(casd_first_wrapper.get_address())
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("build")
        .arg("--name")
        .arg("stage0")
        .output()
        .await
        .unwrap();

    println!("Girderstream stage0: {:?}", girderstream_stage0);
    println!("{}", String::from_utf8_lossy(&girderstream_stage0.stdout));
    println!("{}", String::from_utf8_lossy(&girderstream_stage0.stderr));
    assert!(girderstream_stage0.status.success());

    let girderstream_build = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(casd_first_wrapper.get_address())
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("build")
        .arg("--name")
        .arg("basic_build")
        .output()
        .await
        .unwrap();
    println!("Girderstream basic_build: {:?}", girderstream_build);
    assert!(girderstream_build.status.success());

    let girderstream_build = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(casd_second_wrapper.get_address())
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("build")
        .arg("--name")
        .arg("basic_build")
        .output()
        .await
        .unwrap();
    println!("Girderstream basic_build: {:?}", girderstream_build);
    assert!(girderstream_build.status.success());

    let girderstream_checkout = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(casd_second_wrapper.get_address())
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("checkout")
        .arg("--name")
        .arg("basic_build")
        .arg("--location")
        .arg(format!("{}", tmp_dir_checkout.display()))
        .output()
        .await
        .unwrap();
    println!("Girderstream checkout: {:?}", girderstream_checkout);
    assert!(girderstream_checkout.status.success());
    // why do we need this? grd's checkout should wait till its finished.

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("test_source_second.output"))
        .await
        .unwrap_or_else(|_| panic!("Could not open"))
        .read_to_end(&mut buff)
        .await?;
    let content = String::from_utf8_lossy(&buff);
    assert!(content.contains("non empty file"));

    casd_first_wrapper.close_with_future().await?;
    casd_remote_wrapper.close_with_future().await?;
    casd_second_wrapper.close_with_future().await?;
    Ok(())
}

/// This test checks that environment variables are passed on
///
/// This has some variables going in and others excluded by the configuration
#[tokio::test]
async fn test_build_environment_vars() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_extract = tmp_dir_test_path.join("extract");
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    create_dir_all(&tmp_dir_extract).expect("Could not make dir for testing");
    let extract = Command::new("tar")
        .arg("-xf")
        .arg("tests/build_tests/files/busybox.tar.xz")
        .arg("-C")
        .arg(format!("{}", tmp_dir_extract.display()))
        .output()
        .await
        .unwrap();

    println!("extract: {:?}", extract);

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_extract.join("busybox2").display()))
        .output()
        .await
        .unwrap();

    println!("upload: {:?}", upload);

    println!("about to setup project");

    let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
    let full_config = FullConfig::new(config, vec![]);

    let mut project_config = HashMap::new();
    project_config.insert("set".to_string(), "set2".to_string());

    let mut project = project::project::Project::new_from_dir(
        &full_config,
        tmp_dir_project.to_path_buf(),
        Some(project_config),
        false,
        false,
    )
    .await?;
    let stage0_element = project.get_element(&Arc::from("stage0")).await?;
    let build_element = project.get_element(&Arc::from("env_test")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(&full_config, &stage0_element, locked_project.clone(), false).await?;
    println!("built stage0");

    scheduler::build(&full_config, &build_element, locked_project.clone(), false).await?;
    println!("built build");

    build_element
        .checkout(
            &mut full_config.get_local().to_owned().into(),
            &locked_project,
            &format!("{}", tmp_dir_checkout.display()),
            false,
            false,
        )
        .await?;

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("var_files"))
        .await
        .unwrap_or_else(|_| panic!("Could not open"))
        .read_to_end(&mut buff)
        .await?;
    let content = String::from_utf8_lossy(&buff);
    println!("content {}", content);
    assert!(content.contains("first env var value1"));
    assert!(content.contains("second env var value2"));

    casd_wrapper.close_with_future().await?;
    Ok(())
}

/// This test checks that failing source dont success fully build
///
/// This should fail in a way that helps let the user know why
#[tokio::test]
async fn test_build_failing_source() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_extract = tmp_dir_test_path.join("extract");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    create_dir_all(&tmp_dir_extract).expect("Could not make dir for testing");
    let extract = Command::new("tar")
        .arg("-xf")
        .arg("tests/build_tests/files/busybox.tar.xz")
        .arg("-C")
        .arg(format!("{}", tmp_dir_extract.display()))
        .output()
        .await
        .unwrap();

    println!("extract: {:?}", extract);

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_extract.join("busybox2").display()))
        .output()
        .await
        .unwrap();

    println!("upload: {:?}", upload);

    println!("about to setup project");

    let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
    let full_config = FullConfig::new(config, vec![]);

    let mut project_config = HashMap::new();
    project_config.insert("set".to_string(), "set2".to_string());

    let mut project = project::project::Project::new_from_dir(
        &full_config,
        tmp_dir_project.to_path_buf(),
        Some(project_config),
        false,
        false,
    )
    .await?;
    let stage0_element = project.get_element(&Arc::from("stage0")).await?;
    let bad_source_element = project.get_element(&Arc::from("bad_source")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(&full_config, &stage0_element, locked_project.clone(), false).await?;
    println!("built stage0");

    assert!(scheduler::build(
        &full_config,
        &bad_source_element,
        locked_project.clone(),
        false
    )
    .await
    .is_err());
    println!("built build");

    let build_result = bad_source_element
        .get_build_success(
            &mut full_config.get_local().to_owned().into(),
            &locked_project,
        )
        .await;
    println!("result: {build_result:?}");
    assert!(!build_result.unwrap());

    casd_wrapper.close_with_future().await?;
    Ok(())
}

/// This test checks that empty sources dont success fully build
///
/// This should fail in a way that helps let the user know why
#[tokio::test]
async fn test_build_empty_source() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_extract = tmp_dir_test_path.join("extract");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    create_dir_all(&tmp_dir_extract).expect("Could not make dir for testing");
    let extract = Command::new("tar")
        .arg("-xf")
        .arg("tests/build_tests/files/busybox.tar.xz")
        .arg("-C")
        .arg(format!("{}", tmp_dir_extract.display()))
        .output()
        .await
        .unwrap();

    println!("extract: {:?}", extract);

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_extract.join("busybox2").display()))
        .output()
        .await
        .unwrap();

    println!("upload: {:?}", upload);

    println!("about to setup project");

    let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
    let full_config = FullConfig::new(config, vec![]);

    let mut project_config = HashMap::new();
    project_config.insert("set".to_string(), "set2".to_string());

    let mut project = project::project::Project::new_from_dir(
        &full_config,
        tmp_dir_project.to_path_buf(),
        Some(project_config),
        false,
        false,
    )
    .await?;
    let stage0_element = project.get_element(&Arc::from("stage0")).await?;
    let bad_source_element = project.get_element(&Arc::from("bad_source")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(&full_config, &stage0_element, locked_project.clone(), false).await?;
    println!("built stage0");

    assert!(scheduler::build(
        &full_config,
        &bad_source_element,
        locked_project.clone(),
        false
    )
    .await
    .is_err());
    println!("built build");

    let build_result = bad_source_element
        .get_build_success(
            &mut full_config.get_local().to_owned().into(),
            &locked_project,
        )
        .await;
    println!("result: {build_result:?}");
    assert!(!build_result.unwrap());

    casd_wrapper.close_with_future().await?;
    Ok(())
}

/// This test demonstrates variables in a context passing into a element
///
/// This test element moves the vars into the artifact and then checks
/// that the correct variables are present. It also makes sure the the
/// when works in the context.
#[tokio::test]
async fn test_build_variables() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_extract = tmp_dir_test_path.join("extract");
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    create_dir_all(&tmp_dir_extract).expect("Could not make dir for testing");
    let extract = Command::new("tar")
        .arg("-xf")
        .arg("tests/build_tests/files/busybox.tar.xz")
        .arg("-C")
        .arg(format!("{}", tmp_dir_extract.display()))
        .output()
        .await
        .unwrap();

    println!("extract: {:?}", extract);

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_extract.join("busybox2").display()))
        .output()
        .await
        .unwrap();

    println!("upload: {:?}", upload);

    println!("about to setup project");

    let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
    let full_config = FullConfig::new(config, vec![]);

    let mut project = project::project::Project::new_from_dir(
        &full_config,
        tmp_dir_project.to_path_buf(),
        None,
        false,
        false,
    )
    .await?;
    let stage0_element = project.get_element(&Arc::from("stage0")).await?;
    let build_element = project.get_element(&Arc::from("var_test")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(&full_config, &stage0_element, locked_project.clone(), false).await?;
    println!("built stage0");

    scheduler::build(&full_config, &build_element, locked_project.clone(), false).await?;
    println!("built build");

    //target_element.checkout(&mut full_config.get_local().to_owned().into(), &locked_project, tmp_dir_str ).await?;
    build_element
        .checkout(
            &mut full_config.get_local().to_owned().into(),
            &locked_project,
            &format!("{}", tmp_dir_checkout.display()),
            false,
            false,
        )
        .await?;

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("plugin_vars.yaml"))
        .await
        .unwrap_or_else(|_| panic!("Could not open"))
        .read_to_end(&mut buff)
        .await?;
    let content = String::from_utf8_lossy(&buff);
    println!("content {}", content);
    assert!(content.contains("shell: /bin/sh"));
    assert!(content.contains("bob: bob_value"));
    assert!(content.contains("sam: tom"));

    casd_wrapper.close_with_future().await?;
    Ok(())
}

/// This test demonstrates variables in a context passing into a element
///
/// This has some variables going in and others excluded by the configuration
#[tokio::test]
async fn test_build_variables_config_second() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_extract = tmp_dir_test_path.join("extract");
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    create_dir_all(&tmp_dir_extract).expect("Could not make dir for testing");
    let extract = Command::new("tar")
        .arg("-xf")
        .arg("tests/build_tests/files/busybox.tar.xz")
        .arg("-C")
        .arg(format!("{}", tmp_dir_extract.display()))
        .output()
        .await
        .unwrap();

    println!("extract: {:?}", extract);

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_extract.join("busybox2").display()))
        .output()
        .await
        .unwrap();

    println!("upload: {:?}", upload);

    println!("about to setup project");

    let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
    let full_config = FullConfig::new(config, vec![]);

    let mut project_config = HashMap::new();
    project_config.insert("set".to_string(), "set2".to_string());

    let mut project = project::project::Project::new_from_dir(
        &full_config,
        tmp_dir_project.to_path_buf(),
        Some(project_config),
        false,
        false,
    )
    .await?;
    let stage0_element = project.get_element(&Arc::from("stage0")).await?;
    let build_element = project.get_element(&Arc::from("var_test")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(&full_config, &stage0_element, locked_project.clone(), false).await?;
    println!("built stage0");

    scheduler::build(&full_config, &build_element, locked_project.clone(), false).await?;
    println!("built build");

    //target_element.checkout(&mut full_config.get_local().to_owned().into(), &locked_project, tmp_dir_str ).await?;
    build_element
        .checkout(
            &mut full_config.get_local().to_owned().into(),
            &locked_project,
            &format!("{}", tmp_dir_checkout.display()),
            false,
            false,
        )
        .await?;

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("plugin_vars.yaml"))
        .await
        .unwrap_or_else(|_| panic!("Could not open"))
        .read_to_end(&mut buff)
        .await?;
    let content = String::from_utf8_lossy(&buff);
    println!("content {}", content);
    assert!(content.contains("shell: /bin/sh"));
    assert!(content.contains("bob: bob_value"));
    assert!(!content.contains("sam: tom"));

    casd_wrapper.close_with_future().await?;
    Ok(())
}

/// Check that the build artifact of a failed build creates logs
///
/// In order to debug failed builds we need to be able to export the
/// stdout and stderr of the build and a prerequisite is that after a failed
/// build the stdout and stderr is correctly saved in the failed build artifact.
#[tokio::test]
async fn test_build_failed_logs() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_extract = tmp_dir_test_path.join("extract");
    let tmp_dir_checkout = tmp_dir_test_path.join("checkout");
    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    create_dir_all(&tmp_dir_extract).expect("Could not make dir for testing");
    let extract = Command::new("tar")
        .arg("-xf")
        .arg("tests/build_tests/files/busybox.tar.xz")
        .arg("-C")
        .arg(format!("{}", tmp_dir_extract.display()))
        .output()
        .await
        .unwrap();

    println!("extract: {:?}", extract);

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_extract.join("busybox2").display()))
        .output()
        .await
        .unwrap();
    if !upload.status.success() {
        panic!("Could not upload busybox to cas");
    }
    println!("upload: {:?}", upload);

    println!("about to build project");
    let girderstream_binary = get_test_bin_dir().join("girderstream");
    let girderstream_build = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(casd_wrapper.get_address())
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("build")
        .arg("--name")
        .arg("failed_build")
        .output()
        .await
        .unwrap();

    println!("Girderstream failed_build: {:?}", girderstream_build);
    println!("{}", String::from_utf8_lossy(&girderstream_build.stdout));
    println!("{}", String::from_utf8_lossy(&girderstream_build.stderr));
    assert!(!girderstream_build.status.success());

    let girderstream_checkout = Command::new(&girderstream_binary)
        .arg("--endpoint")
        .arg(casd_wrapper.get_address())
        .arg("--project-root")
        .arg(&tmp_dir_project)
        .arg("--verbose")
        .arg("export-logs")
        .arg("--name")
        .arg("failed_build")
        .arg("--location")
        .arg(format!("{}/logs", tmp_dir_checkout.display()))
        .output()
        .await
        .unwrap();
    println!("Girderstream checkout: {:?}", girderstream_checkout);
    assert!(girderstream_checkout.status.success());

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("logs").join("failed_build.stdout"))
        .await
        .unwrap_or_else(|_| panic!("Could not open"))
        .read_to_end(&mut buff)
        .await?;
    let content = String::from_utf8_lossy(&buff);
    assert!(content.contains("Message to stdout"));

    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_checkout.join("logs").join("failed_build.stderr"))
        .await
        .unwrap_or_else(|_| panic!("Could not open"))
        .read_to_end(&mut buff)
        .await?;
    let content = String::from_utf8_lossy(&buff);
    assert!(content.contains("Message to stderr"));

    casd_wrapper.close_with_future().await?;
    Ok(())
}

/// Check that cached failed builds dont trigger a build
#[tokio::test]
async fn test_build_after_failure() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_extract = tmp_dir_test_path.join("extract");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    create_dir_all(&tmp_dir_extract).expect("Could not make dir for testing");
    let extract = Command::new("tar")
        .arg("-xf")
        .arg("tests/build_tests/files/busybox.tar.xz")
        .arg("-C")
        .arg(format!("{}", tmp_dir_extract.display()))
        .output()
        .await
        .unwrap();

    println!("extract: {:?}", extract);

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_extract.join("busybox2").display()))
        .output()
        .await
        .unwrap();

    println!("upload: {:?}", upload);

    println!("about to setup project");

    let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
    let full_config = FullConfig::new(config, vec![]);

    let mut project = project::project::Project::new_from_dir(
        &full_config,
        tmp_dir_project.to_path_buf(),
        None,
        false,
        false,
    )
    .await?;
    let stage0_element = project.get_element(&Arc::from("stage0")).await?;
    let failed_element = project.get_element(&Arc::from("failed_build")).await?;
    let dep_failed_element = project.get_element(&Arc::from("dep_failure")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(&full_config, &stage0_element, locked_project.clone(), false).await?;
    println!("built stage0");

    // Once we do the work of having proper errors we can check for failed build
    assert!(
        scheduler::build(&full_config, &failed_element, locked_project.clone(), false)
            .await
            .is_err()
    );
    let mut connection = full_config.get_local().to_owned().into();
    assert!(
        !failed_element
            .get_build_success(&mut connection, &locked_project)
            .await?
    );

    // Once we do the work of having proper errors we can check for did not start build due to failed dep
    assert!(scheduler::build(
        &full_config,
        &dep_failed_element,
        locked_project.clone(),
        false
    )
    .await
    .is_err());

    casd_wrapper.close_with_future().await?;
    Ok(())
}
