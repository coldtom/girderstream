// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
use std::{fs::create_dir_all, path::PathBuf, sync::Arc};

use tokio::{fs::File, io::AsyncReadExt, process::Command};

use girderstream::{misc::copy_dir_all, project, scheduler, Crapshoot};
use reapi_tools::{test_infra::CasdWrapper, FullConfig, LocalConfig};

/// This uses the public api of the girderstream lib to build a project
///
/// This is less complete of a end to end test as the similar one that
/// uses girderstream its self. But it lest us make a number of assertions
/// as we build the project which will give us a lot more options to catch
/// issues and hopefully give more helpful diagnostics  if a change brakes
/// something.
#[tokio::test]
async fn test_tack() -> Crapshoot {
    let mut casd_wrapper = CasdWrapper::start_casd()?;
    let tmp_dir_test_path = casd_wrapper.get_temp_root();

    let tmp_dir_project = tmp_dir_test_path.join("project");
    let tmp_dir_extract = tmp_dir_test_path.join("extract");
    let _tmp_dir_checkout = tmp_dir_test_path.join("checkout");

    let original_project_root = PathBuf::from("tests/build_tests");
    copy_dir_all(original_project_root, &tmp_dir_project)?;

    create_dir_all(&tmp_dir_extract).expect("Could not make dir for testing");
    let extract = Command::new("tar")
        .arg("-xf")
        .arg("tests/build_tests/files/busybox.tar.xz")
        .arg("-C")
        .arg(format!("{}", tmp_dir_extract.display()))
        .output()
        .await
        .unwrap();

    println!("extract: {:?}", extract);

    let upload = Command::new("casupload")
        .arg(casd_wrapper.get_cas_upload())
        .arg(format!("{}", tmp_dir_extract.join("busybox2").display()))
        .output()
        .await
        .unwrap();

    println!("upload: {:?}", upload);

    println!("about to setup project");

    let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
    let full_config = FullConfig::new(config, vec![]);

    let mut project = project::project::Project::new_from_dir(
        &full_config,
        tmp_dir_project.to_path_buf(),
        None,
        false,
        false,
    )
    .await?;
    let stage0_element = project.get_element(&Arc::from("stage0")).await?;
    let track_element = project.get_element(&Arc::from("track_element")).await?;
    let locked_project = Arc::new(project);

    scheduler::build(&full_config, &stage0_element, locked_project.clone(), false).await?;
    println!("built stage0");

    track_element.track(&locked_project, &full_config).await?;

    println!("tracked");
    let mut buff: Vec<u8> = vec![];
    File::open(tmp_dir_project.join("elements").join("track_element.grd"))
        .await
        .expect("Could not open file 'test_source_second.output'")
        .read_to_end(&mut buff)
        .await
        .expect("Could not read from file 'test_source_second.output'");
    let content = String::from_utf8_lossy(&buff);
    assert!(content.contains("sha: \"new_value\""));

    println!("finished just drop casd");
    casd_wrapper.close_with_future().await?;
    drop(casd_wrapper);

    Ok(())
}
