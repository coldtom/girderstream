// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! Mange the order and execution of long running tasks
//!
//! Currently we can build in a sensible order with the only
//! "unit" of work being the "build" which currently includes
//! fetching and caching to the local cas.
//!
//! The scheduler is invisaged to share its ordering code so
//! show can use the same functions to workout what to show
//! and in what order. And when building to also have other
//! units of work that can be resond about and happen seperatly.
//!
//! Other units of work to add:
//! - fetch sources
//! - pull artifact
//! - push artifact
//!
//! The current code has been shown to work for 5 or 6 element
//! projects but it lacks verification on larger projects and
//! needs unit and integration tests. Any work on expanding
//! functionality or making it easer to test should be sympathetic
//! to both goals.
use async_channel::{unbounded, Receiver, RecvError, Sender};
use async_recursion::async_recursion;
use itertools::Itertools;
use std::{
    collections::{HashMap, HashSet},
    ops::Deref,
    result::Result::Ok,
    sync::Arc,
};
use tokio::{select, task::JoinSet};

use crate::{
    cache::{cache_build, get_cached_build},
    project::{element::ElementRef, project::Project, BuildResult},
    reapi::pull_cached_build,
    Crapshoot,
};
use reapi_tools::{FullConfig, ReapiConnection};

/// Calculate all of the runtime dependencies of a given element
///
/// This goes through all of the run time dependencies of a element and then
/// finds all of there run time dependencies and continues until all of the
/// direct and indirect run time dependencies have been found.
#[async_recursion]
pub async fn get_runtime_deps(
    element: &ElementRef,
    project: &Project,
) -> anyhow::Result<Vec<ElementRef>> {
    let mut run_time_deps: Vec<ElementRef> = vec![element.clone()];

    for new_dep_name in element.get_run_dependencies() {
        let new_dep_element = project
            .get_existing_element(new_dep_name.get_name())
            .unwrap();
        for sub_dep in get_runtime_deps(&new_dep_element, project).await.unwrap() {
            if !run_time_deps.contains(&sub_dep) {
                run_time_deps.push(sub_dep);
            }
        }
    }

    Ok(run_time_deps)
}

/// Calculate all of the dependencies of a given element
///
/// This goes through all of the dependencies of a element and then finds all of
/// there dependencies and continues until all of the direct and indirect
/// dependencies have been found.
///
/// This function also records how these dependencies relate to each other. The
/// enables allows the scheduler to know which elements it needs to check if it
/// can build following the build of a element. And the requires allows the
/// scheduler to know if all of the elements dependencies have now been for
/// filled.
#[allow(clippy::type_complexity)]
pub async fn get_all_deps(
    connection: &mut ReapiConnection,
    project: &Project,
    element: &ElementRef,
) -> anyhow::Result<(
    Vec<ElementRef>,
    HashMap<ElementRef, Vec<ElementRef>>,
    HashMap<ElementRef, Vec<ElementRef>>,
)> {
    let mut all_deps: Vec<ElementRef> = vec![];
    let mut enables = HashMap::new();
    let mut requires = HashMap::new();

    add_dep_to_lists(
        connection,
        project,
        element,
        &mut all_deps,
        &mut vec![],
        &mut HashMap::new(),
        &mut enables,
        &mut requires,
        None,
        false,
    )
    .await?;
    all_deps.reverse();

    Ok((all_deps, enables, requires))
}

/// Add a element to a build
///
/// This will populate the enables and requires hashmaps needed for this build
/// and will then add the elements that are able to start to the build queues.
#[allow(clippy::too_many_arguments, clippy::map_entry)]
async fn add_element_to_build(
    config: &FullConfig,
    project: &Project,
    element: &ElementRef,
    building: &mut Vec<ElementRef>,
    enables: &mut HashMap<ElementRef, Vec<ElementRef>>,
    requires: &mut HashMap<ElementRef, Vec<ElementRef>>,
    built: &mut HashMap<ElementRef, BuildResult>,
    build_generator: &Sender<Option<ElementRef>>,
    force: bool,
) -> Crapshoot {
    let mut all_deps: Vec<ElementRef> = building.clone();
    all_deps.extend(built.keys().cloned());
    let mut connection = config.get_local().to_owned().into();
    if let Ok(res) = element.get_build_artifact(&mut connection, project).await {
        if res.successfully_build() {
            return Ok(());
        }
    }

    add_dep_to_lists(
        &mut connection,
        project,
        element,
        &mut all_deps,
        building,
        built,
        enables,
        requires,
        Some(build_generator),
        force,
    )
    .await
}

#[allow(clippy::too_many_arguments, clippy::map_entry)]
#[async_recursion]
async fn add_dep_to_lists(
    connection: &mut ReapiConnection,
    project: &Project,
    element: &ElementRef,
    all_deps: &mut Vec<ElementRef>,
    building: &mut Vec<ElementRef>,
    built: &mut HashMap<ElementRef, BuildResult>,
    enables: &mut HashMap<ElementRef, Vec<ElementRef>>,
    requires: &mut HashMap<ElementRef, Vec<ElementRef>>,
    build_generator: Option<&'async_recursion Sender<Option<ElementRef>>>,
    _force: bool,
) -> Crapshoot {
    let mut this_elements_reqs = Vec::with_capacity(element.dep_count());
    let this_element_built = element.get_build_success(connection, project).await?;

    // Go through all the deps to find  all the requirements
    for dep in element.get_recursive_build_elements() {
        // "enables" and "requires" are needed for unbuild elements
        // skip adding this element to them if its built.
        if built.keys().contains(&dep) {
        } else {
            let res = dep.get_build_success(connection, project).await?;
            if res {
                let res = dep.get_build_artifact(connection, project).await?;
                built.insert(dep.to_owned(), res);
            } else if !this_elements_reqs.contains(&dep) {
                this_elements_reqs.push(dep.to_owned());
                if let Some(enabler) = enables.get_mut(&dep) {
                    if !enabler.contains(element) {
                        enabler.push(element.clone());
                    }
                } else {
                    enables.insert(dep.to_owned(), vec![element.to_owned()]);
                };
            }
        }
        // Do not recurse into our selves if we have already processed this element
        if !all_deps.contains(&dep) {
            add_dep_to_lists(
                connection,
                project,
                &dep,
                all_deps,
                building,
                built,
                enables,
                requires,
                build_generator,
                false,
            )
            .await?;
        }
    }

    // By adding our selves after adding our deps we end up with the bottom most
    // elements first, so the bottome elements end up at the start of the show.
    if !all_deps.contains(element) {
        all_deps.push(element.clone())
    }
    // If this element is not build and we could be built then add it to the build
    // queue
    if !this_element_built && this_elements_reqs.is_empty() {
        if let Some(sender) = build_generator {
            if !building.contains(element) {
                sender.send(Some(element.clone())).await?;
                building.push(element.clone())
            }
        }
    };
    // If this element has un built required elements then add this elements reqs to the
    // requirements map.
    if !this_elements_reqs.is_empty() {
        requires.insert(element.to_owned(), this_elements_reqs);
    }
    Ok(())
}

/// Public entry point that works out what needs to be built and builds
///
/// This function works out what needs to be built for a given element to
/// work, ie its self and any run time deps. And then builds them all.
///
/// This function tries to use existing cached builds were possible and only
/// builds what it needs or what it needs to build what it needs.
pub async fn build(
    config: &FullConfig,
    element: &ElementRef,
    project: Arc<Project>,
    force: bool,
) -> Crapshoot {
    let must_be_present = get_runtime_deps(element, &project).await?;

    schedular(config, must_be_present, project, force).await
}

/// Create a set of tasks to perform the build
///
/// The tasks listen for new jobs on the receivers channel
/// and report the work on the senders channel
///
/// The function returns a set of handles to the build tasks
/// that can be joined to or killed.
///
/// The tasks will wait on there channel and process the tasks
/// until they receive a None in the channel at which point the
/// task will stop. To stop all the tasks send N * None's to
/// the receivers channel.
async fn builders(
    builders: i32,
    config: &FullConfig,
    project: Arc<Project>,
    receivers: Receiver<Option<ElementRef>>,
    senders: Sender<Result<(ElementRef, BuildResult), String>>,
) -> JoinSet<()> {
    let mut joins = JoinSet::new();
    for _iii in 0..builders {
        let this_builder_receiver = receivers.clone();
        let this_sender = senders.clone();
        let full_config = config.clone();
        let this_project = project.clone(); // we should take a &project create a Arc<Projec> on this line
        joins.spawn(async move {
            let mut connection = full_config.get_local().to_owned().into();
            println!("Waiting for task");
            loop {
                // Todo this wait should be a select with this queue and a cancellation system.
                match this_builder_receiver.recv().await {
                    Err(e) => {
                        match e {
                            RecvError => {
                                println!("Receiver Error");
                                return;
                            }
                        };
                    }
                    Ok(received) => match received {
                        Some(element) => {
                            println!("Got build: {} {}", element.get_name(), element.get_kind());

                            let mut pulled_result: Option<BuildResult> = None;
                            for remote in full_config.get_remote() {
                                let mut connection_remote = remote.to_owned().into();
                                if pull_cached_build(
                                    &mut connection,
                                    &mut connection_remote,
                                    &this_project,
                                    &element,
                                )
                                .await
                                .is_ok()
                                {
                                    pulled_result = Some(
                                        get_cached_build(&mut connection, &this_project, &element)
                                            .await
                                            .unwrap(),
                                    );
                                    println!("Got cached element");
                                    break;
                                };
                            }
                            let build_result = if let Some(build_result) = pulled_result {
                                if build_result.successfully_build() {
                                    build_result
                                } else {
                                    println!("Rebuilding {:?}", element.get_name());
                                    let build_result =
                                        this_project.build_element(&full_config, &element).await;
                                    cache_build(
                                        &mut connection,
                                        &this_project,
                                        &element,
                                        &build_result,
                                    )
                                    .await
                                    .expect("Could not cache element locally");
                                    build_result
                                }
                            } else {
                                println!("Building {:?}", element.get_name());
                                let build_result =
                                    this_project.build_element(&full_config, &element).await;

                                cache_build(
                                    &mut connection,
                                    &this_project,
                                    &element,
                                    &build_result,
                                )
                                .await
                                .expect("Could not cache element locally");
                                build_result
                            };

                            // This should be after the build step but need a re jig of the schedular to have the pull,
                            // fetch, build and push be separate queues.
                            if build_result.successfully_build() {
                                for remote in full_config.get_remote() {
                                    if remote.can_push() {
                                        let mut connection_remote = remote.to_owned().into();
                                        if element
                                            .push(
                                                &mut connection,
                                                &mut connection_remote,
                                                &this_project,
                                            )
                                            // It would be better not to await here but have a second loop to wait for
                                            // the futures.
                                            .await
                                            .is_err()
                                        {
                                            println!("Could not push to remote");
                                            if remote.stop_on_failed_push() {
                                                this_sender
                                                    .send(Err(
                                                        "Could not push to remote".to_string()
                                                    ))
                                                    .await
                                                    .expect("Could not send finished building");
                                                return;
                                            }
                                        } else {
                                            println!(
                                                "Pushed '{}' to remote: '{}'",
                                                element.get_name(),
                                                remote.get_endpoint()
                                            );
                                        };
                                    }
                                }
                            }

                            // If we have a cache build then we always send the result to the schedulure as the result
                            // contains the success information.
                            let build_success = build_result.successfully_build();
                            this_sender
                                .send(Ok((element.clone(), build_result)))
                                .await
                                .expect("Could not send build result");
                            // But if it was a unsuccessful build then we stop the builder.
                            if build_success {
                                println!("Successfully completed build: {:?}", element.get_name());
                            } else {
                                println!(
                                    "Un successfully complted build: {:?}",
                                    element.get_name()
                                );
                                return;
                            }
                        }
                        None => return,
                    },
                }
            }
        });
    }
    joins
}

/// Schedule the building of the elements
///
/// This function creates the the machinery of the build process and
/// manges the updates as elements are built and other elements
/// builds are then queued.
async fn schedular(
    config: &FullConfig,
    elements: Vec<ElementRef>,
    project: Arc<Project>,
    force: bool,
) -> Crapshoot {
    let (build_requester, builder_receiver) = unbounded();
    let (build_finisher, finished_builder_receiver) = unbounded();

    let mut building: Vec<ElementRef> = Vec::new();
    let mut enables: HashMap<ElementRef, Vec<ElementRef>> = HashMap::new();
    let mut requires: HashMap<ElementRef, Vec<ElementRef>> = HashMap::new();
    let mut built: HashMap<ElementRef, BuildResult> = HashMap::new();

    let mut builder_futures =
        builders(5, config, project.clone(), builder_receiver, build_finisher).await;

    // This builds the build graph. Currently it only builds the things that need to be built
    // but this logic could be abstracted as show should have some very similar logic to stop
    // it repeating elements etc.
    for element in elements {
        add_element_to_build(
            config,
            &project,
            &element,
            &mut building,
            &mut enables,
            &mut requires,
            &mut built,
            &build_requester,
            force,
        )
        .await?
    }

    let mut building: HashSet<ElementRef> = HashSet::from_iter(building.into_iter());

    let mut final_result = Ok(());

    while !building.is_empty() {
        println!(
            "enables {:#?}",
            enables
                .iter()
                .map(|(el, els)| {
                    format!(
                        "{}: {:?}",
                        el.get_name().deref(),
                        els.iter()
                            .map(|child| child.get_name().deref())
                            .collect::<Vec<&str>>()
                    )
                })
                .collect::<Vec<String>>()
        );
        println!(
            "requires {:#?}",
            requires
                .iter()
                .map(|(el, els)| {
                    format!(
                        "{}: {:?}",
                        el.get_name().deref(),
                        els.iter()
                            .map(|child| child.get_name().deref())
                            .collect::<Vec<&str>>()
                    )
                })
                .collect::<Vec<String>>()
        );
        println!(
            "building {:#?}",
            building
                .iter()
                .map(|el| el.get_name().deref())
                .collect::<Vec<&str>>()
        );

        let received = select! {
            result = finished_builder_receiver.recv() => result,
            Some(result) = builder_futures.join_next() => return Err(anyhow::anyhow!("failed {:?}", result)),
        };

        let build_result_result: Result<(ElementRef, BuildResult), String> =
            if let Ok(result) = received {
                result
            } else {
                println!("Issue Receiving from Unbound");
                final_result = Err(anyhow::format_err!("Did not receive from Unbound"));
                break;
            };
        let (finished_build_name, finished_build_result) = match build_result_result {
            Ok((finished_build_name, finished_build_result)) => {
                (finished_build_name, finished_build_result)
            }
            Err(error) => {
                println!("Builder failed: {error}");
                final_result = Err(anyhow::format_err!("Did not build: {error}"));
                break;
            }
        };
        building.remove(&finished_build_name);
        println!("'{finished_build_name}' has finished {finished_build_result:?}");
        if !finished_build_result.successfully_build() {
            println!("Could not build '{finished_build_name}' exiting build queue");
            if let Some(err) = finished_build_result.get_failure_info() {
                println!("    Failed build due to: {err}");
            }
            final_result = Err(anyhow::format_err!("Did not build"));
            break;
        }
        // top level finisheds dont have enables
        if let Some(newly_enabled) = enables.remove(&finished_build_name) {
            for enables in newly_enabled {
                let enabled_build_req = requires.get_mut(&enables).unwrap();
                enabled_build_req.retain(|req| req.ne(&finished_build_name));
                if enabled_build_req.is_empty() {
                    // println!("can now build {enables}");
                    assert!(!building.contains(&enables));
                    //let enabled_element = project.get_existing_element(&enables)?;
                    requires.remove(&enables);
                    building.insert(enables.clone());
                    build_requester.send(Some(enables)).await.unwrap();
                } else {
                    // println! {"still cant build {enables}: {enabled_build_req:?}"};
                }
            }
        }
    }

    for _join in 0..builder_futures.len() {
        build_requester
            .send(None)
            .await
            .expect("Could not send stop to children");
    }

    while (builder_futures.join_next().await).is_some() {}

    //println!("build_requester {:?}", build_requester);
    println!("final_result: {final_result:?}");

    final_result
}

#[cfg(test)]
mod tests {
    use std::{collections::HashMap, sync::Arc, vec};

    use async_channel::unbounded;
    use serde_either::StringOrStruct;

    use crate::{
        project::{
            element::{BuildType, Element, ElementRef, InputDependency, InputElement, StageType},
            plugin::BuildPlugin,
            project::Project,
        },
        scheduler::{add_element_to_build, get_all_deps, schedular},
        Crapshoot,
    };
    use reapi_tools::{test_infra::CasdWrapper, FullConfig, LocalConfig};

    // This test checks the most basic functioning of a add_element_to_build
    #[tokio::test]
    async fn test_add_element_to_build() -> Crapshoot {
        let mut casd_wrapper = CasdWrapper::start_casd()?;

        let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
        let full_config = FullConfig::new(config, vec![]);

        let (build_requester, builder_receiver) = unbounded();

        let build_plugin = BuildPlugin::for_test();
        let mut project = Project::for_test();
        project.add_test_build_plugin("bob".to_owned(), build_plugin);
        let project = project;

        let element = Element::for_test("bob".to_string(), Arc::from("bob"));
        let element_ref = ElementRef::new(element);

        let mut building = Vec::new();
        let mut enables = HashMap::new();
        let mut requires = HashMap::new();
        let mut built = HashMap::new();

        add_element_to_build(
            &full_config,
            &project,
            &element_ref,
            &mut building,
            &mut enables,
            &mut requires,
            &mut built,
            &build_requester,
            false,
        )
        .await
        .unwrap();

        {
            assert_eq!(building.len(), 1);
            assert_eq!(built.len(), 0);
            assert_eq!(enables.len(), 0);
            assert_eq!(requires.len(), 0);
        }
        {
            let added = builder_receiver
                .recv()
                .await
                .unwrap()
                .expect("Should have a element added");
            assert_eq!(added.get_name(), &Arc::from("bob"));
        }

        // Now we try to add the same thing again, this time it should
        // have no effect as the element is already in the building hashmap
        add_element_to_build(
            &full_config,
            &project,
            &element_ref,
            &mut building,
            &mut enables,
            &mut requires,
            &mut built,
            &build_requester,
            false,
        )
        .await
        .unwrap();

        {
            assert_eq!(building.len(), 1);
            assert_eq!(built.len(), 0);
            assert_eq!(enables.len(), 0);
            assert_eq!(requires.len(), 0);
        }
        assert!(builder_receiver.is_empty());

        casd_wrapper.close_with_future().await?;
        Ok(())
    }

    // This test checks if multiple elements are added one at a time
    #[tokio::test]
    async fn test_add_element_to_build_multi_step() -> Crapshoot {
        let mut casd_wrapper = CasdWrapper::start_casd()?;

        let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
        let full_config = FullConfig::new(config, vec![]);

        let (build_requester, builder_receiver) = unbounded();

        let build_plugin = BuildPlugin::for_test();
        let mut project = Project::for_test();
        project.add_test_build_plugin("bob".to_owned(), build_plugin.clone());
        project.add_test_build_plugin("second".to_owned(), build_plugin);

        let element_name = "bob".to_string();
        let input_element =
            InputElement::new_for_test("bob".to_string(), Some("bob".to_string()), None);
        project.add_serde_test_element(element_name.clone(), input_element);
        let element_ref = project
            .get_element(&Arc::from(element_name.as_str()))
            .await
            .unwrap();

        let mut building = Vec::new();
        let mut enables = HashMap::new();
        let mut requires = HashMap::new();
        let mut built = HashMap::new();

        add_element_to_build(
            &full_config,
            &project,
            &element_ref,
            &mut building,
            &mut enables,
            &mut requires,
            &mut built,
            &build_requester,
            false,
        )
        .await
        .unwrap();

        {
            assert_eq!(building.len(), 1);
            assert_eq!(built.len(), 0);
            assert_eq!(enables.len(), 0);
            assert_eq!(requires.len(), 0);
        }
        {
            let added = builder_receiver
                .recv()
                .await
                .unwrap()
                .expect("Should have a element added");
            assert_eq!(added.get_name(), &Arc::from("bob"));
        }

        let second_element_name = "second".to_string();
        let second_serde_element = InputElement::new_for_test(
            "second".to_string(),
            Some("second".to_string()),
            Some(vec![StringOrStruct::String("bob".to_string())]),
        );
        project.add_serde_test_element(second_element_name.clone(), second_serde_element);
        let second_element_ref = project
            .get_element(&Arc::from(second_element_name.as_str()))
            .await
            .unwrap();

        add_element_to_build(
            &full_config,
            &project,
            &second_element_ref,
            &mut building,
            &mut enables,
            &mut requires,
            &mut built,
            &build_requester,
            false,
        )
        .await
        .unwrap();

        {
            assert_eq!(building.len(), 1);
            assert_eq!(built.len(), 0);
            assert_eq!(enables.len(), 1);
            assert_eq!(requires.len(), 1);
        }
        assert!(builder_receiver.is_empty());

        // Now we try to add the same thing again, this time it should
        // have no effect as the element is already in the building hashmap
        add_element_to_build(
            &full_config,
            &project,
            &second_element_ref,
            &mut building,
            &mut enables,
            &mut requires,
            &mut built,
            &build_requester,
            false,
        )
        .await
        .unwrap();

        {
            assert_eq!(building.len(), 1);
            assert_eq!(built.len(), 0);
            assert_eq!(enables.len(), 1);
            assert_eq!(requires.len(), 1);
        }
        assert!(builder_receiver.is_empty());

        casd_wrapper.close_with_future().await?;
        Ok(())
    }

    // This test checks that multiple elements are added correctly
    #[tokio::test]
    async fn test_add_element_to_build_multi_same() -> Crapshoot {
        let mut casd_wrapper = CasdWrapper::start_casd()?;

        let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
        let full_config = FullConfig::new(config, vec![]);

        let (build_requester, builder_receiver) = unbounded();

        let build_plugin = BuildPlugin::for_test();
        let mut project = Project::for_test();
        project.add_test_build_plugin("alice".to_owned(), build_plugin.clone());
        project.add_test_build_plugin("forth".to_owned(), build_plugin.clone());
        project.add_test_build_plugin("fifth".to_owned(), build_plugin);

        let mut building = Vec::new();
        let mut enables = HashMap::new();
        let mut requires = HashMap::new();
        let mut built = HashMap::new();

        let element_name_alice = "alice".to_string();
        let input_element_alice =
            InputElement::new_for_test("alice".to_string(), Some(element_name_alice.clone()), None);
        project.add_serde_test_element(element_name_alice.clone(), input_element_alice);
        let _element_ref_alice = project
            .get_element(&Arc::from(element_name_alice.as_str()))
            .await
            .unwrap();

        let element_name_forth = "forth".to_string();
        let input_element_forth = InputElement::new_for_test(
            "forth".to_string(),
            Some(element_name_forth.clone()),
            Some(vec![StringOrStruct::String("alice".to_string())]),
        );
        project.add_serde_test_element(element_name_forth.clone(), input_element_forth);
        let _element_ref_forth = project
            .get_element(&Arc::from(element_name_forth.as_str()))
            .await
            .unwrap();

        let element_name_fifth = "fifth".to_string();
        let input_element_fifth = InputElement::new_for_test(
            "fifth".to_string(),
            Some(element_name_fifth.clone()),
            Some(vec![StringOrStruct::String("forth".to_string())]),
        );
        project.add_serde_test_element(element_name_fifth.clone(), input_element_fifth);
        let element_fifth_ref = project
            .get_element(&Arc::from(element_name_fifth.as_str()))
            .await
            .unwrap();

        add_element_to_build(
            &full_config,
            &project,
            &element_fifth_ref,
            &mut building,
            &mut enables,
            &mut requires,
            &mut built,
            &build_requester,
            false,
        )
        .await
        .unwrap();

        {
            assert_eq!(building.len(), 1);
            assert_eq!(built.len(), 0);
            assert_eq!(enables.len(), 2);
            assert_eq!(requires.len(), 2);
        }
        {
            let added = builder_receiver
                .recv()
                .await
                .unwrap()
                .expect("Should have a element added");
            assert_eq!(added.get_name(), &Arc::from("alice"));
        }

        casd_wrapper.close_with_future().await?;
        Ok(())
    }

    // Basic run of the scheduler
    #[tokio::test]
    async fn test_full_schedule() -> Crapshoot {
        let mut casd_wrapper = CasdWrapper::start_casd()?;

        let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
        let full_config = FullConfig::new(config, vec![]);

        let mut project = Project::for_test();

        let element_name_alice = "alice".to_string();
        let input_element_alice = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some(element_name_alice.clone()),
            None,
        );
        project.add_serde_test_element(element_name_alice.clone(), input_element_alice);
        let element_ref = project
            .get_element(&Arc::from(element_name_alice.as_str()))
            .await
            .unwrap();

        casd_wrapper.wait_for_casd().await?;

        schedular(&full_config, vec![element_ref], Arc::new(project), false)
            .await
            .unwrap();

        casd_wrapper.close_with_future().await?;
        Ok(())
    }

    // Run the scheduler where one element enables many elements
    #[tokio::test]
    async fn test_single_element_enable_multi() -> Crapshoot {
        let mut casd_wrapper = CasdWrapper::start_casd()?;

        let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
        let full_config = FullConfig::new(config, vec![]);

        let (build_requester, _builder_receiver) = unbounded();

        let mut project = Project::for_test();

        let mut building = Vec::new();
        let mut enables = HashMap::new();
        let mut requires = HashMap::new();
        let mut built = HashMap::new();

        let element_name_bob = "bob".to_string();
        let input_element_bob = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some(element_name_bob.clone()),
            None,
        );
        project.add_serde_test_element(element_name_bob.clone(), input_element_bob);
        let element_bob_ref = project
            .get_element(&Arc::from(element_name_bob.as_str()))
            .await
            .unwrap();

        let element_name_fifth = "fifth".to_string();
        let input_element_fifth = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some(element_name_fifth.clone()),
            Some(vec![StringOrStruct::String("bob".to_string())]),
        );
        project.add_serde_test_element(element_name_fifth.clone(), input_element_fifth);
        let element_fifth_ref = project
            .get_element(&Arc::from(element_name_fifth.as_str()))
            .await
            .unwrap();

        let element_name_six = "six".to_string();
        let input_element_six = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some(element_name_six.clone()),
            Some(vec![StringOrStruct::String("bob".to_string())]),
        );
        project.add_serde_test_element(element_name_six.clone(), input_element_six);
        let element_six_ref = project
            .get_element(&Arc::from(element_name_six.as_str()))
            .await
            .unwrap();

        let element_name_thr = "thr".to_string();
        let input_element_thr = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some(element_name_thr.clone()),
            Some(vec![
                StringOrStruct::String("fifth".to_string()),
                StringOrStruct::String("six".to_string()),
            ]),
        );
        project.add_serde_test_element(element_name_thr.clone(), input_element_thr);
        let element_thr_ref = project
            .get_element(&Arc::from(element_name_thr.as_str()))
            .await
            .unwrap();

        let element_name_fou = "fou".to_string();
        let input_element_fou = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some(element_name_fou.clone()),
            Some(vec![StringOrStruct::String("six".to_string())]),
        );
        project.add_serde_test_element(element_name_fou.clone(), input_element_fou);
        let element_fou_ref = project
            .get_element(&Arc::from(element_name_fou.as_str()))
            .await
            .unwrap();

        let element_name_top = "top".to_string();
        let input_element_top = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some(element_name_top.clone()),
            Some(vec![
                StringOrStruct::String("fifth".to_string()),
                StringOrStruct::String("six".to_string()),
                StringOrStruct::String("thr".to_string()),
                StringOrStruct::String("fou".to_string()),
            ]),
        );
        project.add_serde_test_element(element_name_top.clone(), input_element_top);
        let element_top_ref = project
            .get_element(&Arc::from(element_name_top.as_str()))
            .await
            .unwrap();

        let mut connection = full_config.get_local().to_owned().into();
        let (_build_deps, en, rq) =
            get_all_deps(&mut connection, &project, &element_top_ref).await?;

        en.get(&element_bob_ref).unwrap();
        en.get(&element_fifth_ref).unwrap();
        en.get(&element_six_ref).unwrap();
        en.get(&element_thr_ref).unwrap();
        en.get(&element_fou_ref).unwrap();

        rq.get(&element_fifth_ref).unwrap();
        rq.get(&element_six_ref).unwrap();
        rq.get(&element_thr_ref).unwrap();
        rq.get(&element_fou_ref).unwrap();
        rq.get(&element_top_ref).unwrap();

        casd_wrapper.wait_for_casd().await?;

        add_element_to_build(
            &full_config,
            &project,
            &element_top_ref,
            &mut building,
            &mut enables,
            &mut requires,
            &mut built,
            &build_requester,
            false,
        )
        .await
        .unwrap();

        {
            let bob_enables = enables.get(&element_bob_ref).unwrap();
            assert!(bob_enables.contains(&element_fifth_ref));
            assert!(bob_enables.contains(&element_six_ref));
        }

        schedular(
            &full_config,
            vec![element_top_ref],
            Arc::new(project),
            false,
        )
        .await
        .unwrap();

        casd_wrapper.close_with_future().await?;
        Ok(())
    }

    // This tests the get_deps function
    #[tokio::test]
    async fn test_get_deps() -> Crapshoot {
        let mut casd_wrapper = CasdWrapper::start_casd()?;

        let mut project = Project::for_test();

        let element_name_alice = "alice".to_string();
        let input_element_alice = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some(element_name_alice.clone()),
            None,
        );
        project.add_serde_test_element(element_name_alice.clone(), input_element_alice);
        let element_alice_ref = project
            .get_element(&Arc::from(element_name_alice.as_str()))
            .await
            .unwrap();

        let element_name_forth = "forth".to_string();
        let input_element_forth = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some(element_name_forth.clone()),
            Some(vec![StringOrStruct::String("alice".to_string())]),
        );
        project.add_serde_test_element(element_name_forth.clone(), input_element_forth);
        let element_forth_ref = project
            .get_element(&Arc::from(element_name_forth.as_str()))
            .await
            .unwrap();

        let element_name_fifth = "fifth".to_string();
        let input_element_fifth = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some(element_name_fifth.clone()),
            Some(vec![StringOrStruct::String("forth".to_string())]),
        );
        project.add_serde_test_element(element_name_fifth.clone(), input_element_fifth);
        let element_fifth_ref = project
            .get_element(&Arc::from(element_name_fifth.as_str()))
            .await
            .unwrap();

        let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
        let full_config = FullConfig::new(config, vec![]);
        let mut connection = full_config.get_local().to_owned().into();
        let (build_deps, en, rq) =
            get_all_deps(&mut connection, &project, &element_fifth_ref).await?;

        dbg!(&build_deps);
        dbg!(&en);
        dbg!(&rq);

        assert_eq!(build_deps.len(), 3);
        assert_eq!(en.len(), 2);
        assert_eq!(rq.len(), 2);

        assert!(build_deps.contains(&element_alice_ref));
        assert!(build_deps.contains(&element_forth_ref));
        assert!(build_deps.contains(&element_fifth_ref));

        casd_wrapper.close_with_future().await?;
        Ok(())
    }

    // This tests the get_deps function
    #[tokio::test]
    async fn test_get_deps_run_build() -> Crapshoot {
        let mut casd_wrapper = CasdWrapper::start_casd()?;

        let mut project = Project::for_test();

        let element_name_alice = "alice".to_string();
        let input_element_alice = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some(element_name_alice.clone()),
            None,
        );
        project.add_serde_test_element(element_name_alice.clone(), input_element_alice);
        let element_alice_ref = project
            .get_element(&Arc::from(element_name_alice.as_str()))
            .await
            .unwrap();

        let element_name_forth = "forth".to_string();
        let input_element_forth = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some(element_name_forth.clone()),
            Some(vec![StringOrStruct::Struct(InputDependency::new(
                element_name_alice.clone(),
                Some(BuildType::Run),
                None,
                None,
                None,
            ))]),
        );
        project.add_serde_test_element(element_name_forth.clone(), input_element_forth);
        let element_forth_ref = project
            .get_element(&Arc::from(element_name_forth.as_str()))
            .await
            .unwrap();

        let element_name_fifth = "fifth".to_string();
        let input_element_fifth = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some(element_name_fifth.clone()),
            Some(vec![StringOrStruct::Struct(InputDependency::new(
                element_name_forth.clone(),
                Some(BuildType::Build),
                None,
                None,
                None,
            ))]),
        );
        project.add_serde_test_element(element_name_fifth.clone(), input_element_fifth);
        let element_fifth_ref = project
            .get_element(&Arc::from(element_name_fifth.as_str()))
            .await
            .unwrap();

        let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
        let full_config = FullConfig::new(config, vec![]);
        let mut connection = full_config.get_local().to_owned().into();
        let (build_deps, en, rq) =
            get_all_deps(&mut connection, &project, &element_fifth_ref).await?;

        dbg!(&build_deps);
        dbg!(&en);
        dbg!(&rq);

        assert_eq!(build_deps.len(), 3);
        assert_eq!(en.len(), 2);

        // both alice and forth can build immediately because they have a run not build dep
        // so nether have requirements, just fifth
        assert_eq!(rq.len(), 1);
        // but that one element depends on both alice and forth, because fifth build deps on forth which run time dep on alice
        // so it needs both to build
        assert_eq!(rq.get(&element_fifth_ref).unwrap().len(), 2);

        assert!(build_deps.contains(&element_alice_ref));
        assert!(build_deps.contains(&element_forth_ref));
        assert!(build_deps.contains(&element_fifth_ref));

        casd_wrapper.close_with_future().await?;
        Ok(())
    }

    // This tests the get_deps function
    #[tokio::test]
    async fn test_get_deps_run_build_stage() -> Crapshoot {
        let mut casd_wrapper = CasdWrapper::start_casd()?;

        let mut project = Project::for_test();

        let element_name_alice = "alice".to_string();
        let input_element_alice = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some(element_name_alice.clone()),
            None,
        );
        project.add_serde_test_element(element_name_alice.clone(), input_element_alice);
        let _element_alice_ref = project
            .get_element(&Arc::from(element_name_alice.as_str()))
            .await
            .unwrap();

        let element_name_forth = "forth".to_string();
        let input_element_forth = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some(element_name_forth.clone()),
            Some(vec![StringOrStruct::Struct(InputDependency::new(
                element_name_alice.clone(),
                Some(BuildType::Run),
                None,
                None,
                None,
            ))]),
        );
        project.add_serde_test_element(element_name_forth.clone(), input_element_forth);
        let element_forth_ref = project
            .get_element(&Arc::from(element_name_forth.as_str()))
            .await
            .unwrap();

        let element_name_fifth = "fifth".to_string();
        let input_element_fifth = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some(element_name_fifth.clone()),
            Some(vec![StringOrStruct::Struct(InputDependency::new(
                element_name_forth.clone(),
                Some(BuildType::Build),
                None,
                Some(StageType::None),
                None,
            ))]),
        );
        project.add_serde_test_element(element_name_fifth.clone(), input_element_fifth);
        let element_fifth_ref = project
            .get_element(&Arc::from(element_name_fifth.as_str()))
            .await
            .unwrap();

        let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
        let full_config = FullConfig::new(config, vec![]);
        let mut connection = full_config.get_local().to_owned().into();
        let (build_deps, en, rq) =
            get_all_deps(&mut connection, &project, &element_fifth_ref).await?;

        dbg!(&build_deps);
        dbg!(&en);
        dbg!(&rq);

        assert_eq!(build_deps.len(), 2);
        assert_eq!(en.len(), 1);

        // both alice and forth can build immediately because they have a run not build dep
        // so nether have requirements, just fifth
        assert_eq!(rq.len(), 1);
        // but that one element depends on both alice and forth, because fifth build deps on forth which run time dep on alice
        // so it needs both to build. Except that we set the staged deps to None, so it does not need alice.
        assert_eq!(rq.get(&element_fifth_ref).unwrap().len(), 1);

        assert!(build_deps.contains(&element_forth_ref));
        assert!(build_deps.contains(&element_fifth_ref));

        casd_wrapper.close_with_future().await?;
        Ok(())
    }
}
