// Copyright 2024 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! This is the main girderstream cli entry point
//!
//! This is used to run girderstream project commands.
//! A project command is one that operates on the project
//! ie build something in the project. show the cache state
//! of a project or element.
//!
//! Other entry points may contain utility functions
//! ie. add a directory or tar to the cas that a project
//! may then consume as a source.

use std::{
    env,
    path::{Path, PathBuf},
    process::exit,
};

use anyhow::bail;
use clap::Parser;
use reapi::{
    self,
    build::bazel::remote::{
        asset::v1::FetchBlobRequest,
        execution::v2::{Digest, FindMissingBlobsRequest, GetCapabilitiesRequest},
    },
};
use serde_json::json;
use tokio::join;

use girderstream::{configure::UserConfig, Crapshoot};
use reapi_tools::{
    grpc_io::{fetch_blob, upload_blob},
    ReapiConnection, RemoteConfig, RemotesConfig,
};
use tonic::Code;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about=None)]
pub struct CheckCli {
    /// Specify the project root to load from, defaults to pwd if non given
    #[clap(long)]
    project_root: Option<String>,
}

impl CheckCli {
    /// Get the directory that contains the Girderstream project
    pub fn get_root(&self) -> anyhow::Result<PathBuf> {
        Ok(if let Some(root) = &self.project_root {
            PathBuf::from(root.clone())
        } else {
            env::current_dir()?
        })
    }
}
async fn test_caps(remote_connection: &mut ReapiConnection, endpoint: &str) -> Crapshoot {
    let caps_client = match remote_connection.get_capabilities_client().await {
        Ok(caps_client) => {
            let caps_status = json!({
                "Status": "Start",
                "Test": "Get Capabilities",
                "Endpoint": endpoint,
                "Debug": format!("{:?}", caps_client),
            });
            println!("{}", caps_status);
            caps_client
        }
        Err(err) => {
            let caps_status = json!({
                "Status": "Error",
                "Test": "Get Capabilities Client",
                "Endpoint": endpoint,
                "Error": format!("{:?}", err) ,
            });
            println!("{}", caps_status);
            anyhow::bail!("Could not get Capabilities Client")
        }
    };

    let request = GetCapabilitiesRequest {
        instance_name: "".to_string(),
    };

    // Check the capabilities service
    match caps_client.get_capabilities(request).await {
        Ok(caps_reply) => {
            let caps_status = json!({
                "Status": "OK",
                "Test": "Get Capabilities",
                "Endpoint": endpoint,
                "Debug": format!("{:?}", caps_reply),
            });
            println!("{}", caps_status);
        }
        Err(err) => {
            let caps_status = json!({
                "Status": "Error",
                "Test": "Get Capabilities",
                "Endpoint": endpoint,
                "Error": format!("{:?}", err) ,
            });
            println!("{}", caps_status);
            anyhow::bail!("Could not get Capabilities result")
        }
    };
    Ok(())
}

async fn test_missing_blobs(remote_connection: &mut ReapiConnection, endpoint: &str) -> Crapshoot {
    let cas_client = match remote_connection.get_cas_client().await {
        Ok(caps_client) => {
            let caps_status = json!({
                "Status": "Start",
                "Test": "Get Cas Client",
                "Endpoint": endpoint,
                "Debug": format!("{:?}", caps_client),
            });
            println!("{}", caps_status);
            caps_client
        }
        Err(err) => {
            let caps_status = json!({
                "Status": "Error",
                "Test": "Get Cas Client",
                "Endpoint": endpoint,
                "Error": format!("{:?}", err) ,
            });
            println!("{}", caps_status);
            anyhow::bail!("Could not get Capabilities Client")
        }
    };
    let request = FindMissingBlobsRequest {
        instance_name: "".to_string(),
        blob_digests: vec![
            // the empty blob is always pressent
            Digest {
                hash: "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
                    .to_string(),
                size_bytes: 0,
            },
            // only the empty blob can have size_bytes = 0 so this blob will never exist
            Digest {
                hash: "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b856"
                    .to_string(),
                size_bytes: 1,
            },
        ],
    };

    match cas_client.find_missing_blobs(request).await {
        Ok(reply) => {
            let responce = reply.into_inner();
            if responce.missing_blob_digests.len() == 1
                && responce.missing_blob_digests.first().unwrap().hash
                    == "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b856"
            {
                let caps_status = json!({
                    "Status": "Ok",
                    "Test": "Missing blob result",
                    "Endpoint": endpoint,
                    "Debug": format!("responce: {:?}", responce)
                });
                println!("{}", caps_status);
            } else {
                let caps_status = json!({
                    "Status": "Error",
                    "Test": "Missing blob result",
                    "Endpoint": endpoint,
                    "Debug": format!("responce: {:?}", responce)
                });
                println!("{}", caps_status);
                anyhow::bail!("Could not get Capabilities Client")
            };
        }
        Err(err) => {
            let caps_status = json!({
                "Status": "Error",
                "Test": "Missing blob result",
                "Endpoint": endpoint,
                "Error": format!("{:?}", err) ,
            });
            println!("{}", caps_status);
            anyhow::bail!("Could not get Capabilities Client")
        }
    };

    Ok(())
}

async fn test_asset(
    remote_connection: &mut ReapiConnection,
    endpoint: &str,
    _push: bool,
) -> Crapshoot {
    let caps_fetch_client = match remote_connection.get_asset_fetch_client().await {
        Ok(caps_client) => {
            let caps_status = json!({
                "Status": "Start",
                "Test": "Get Asset fetch",
                "Endpoint": endpoint,
                "Debug": format!("{:?}", caps_client),
            });
            println!("{}", caps_status);
            caps_client
        }
        Err(err) => {
            let caps_status = json!({
                "Status": "Error",
                "Test": "Get Asset Client",
                "Endpoint": endpoint,
                "Error": format!("{:?}", err) ,
            });
            println!("{}", caps_status);
            anyhow::bail!("Could not get Capabilities Client")
        }
    };
    let request = FetchBlobRequest {
        instance_name: "".to_string(),
        qualifiers: vec![],
        uris: vec!["urn:fdc:dummy:artifact:should:not:exist".to_string()],
        oldest_content_accepted: None,
        timeout: None,
    };

    match caps_fetch_client.fetch_blob(request).await {
        Ok(_) => {
            let caps_status = json!({
                "Status": "Error",
                "Test": "Server should not have dummy element",
                "Endpoint": endpoint,
                "Error": "Bad reply" ,
            });
            println!("{}", caps_status);
            anyhow::bail!("Could not get Capabilities Client");
        }
        Err(err) => match err.code() {
            Code::NotFound => {
                let fetch_status = json!({
                    "Status": "Ok",
                    "Test": "Fetch Asset",
                    "Endpoint": endpoint,
                    "Debug": "Dummy element returns not found"
                });
                println!("{}", fetch_status);
            }
            _ => {
                let fetch_status = json!({
                    "Status": "Error",
                    "Test": "Fetch Asset",
                    "Endpoint": endpoint,
                    "Error": format!("{:?}", err) ,
                });
                println!("{}", fetch_status);
                anyhow::bail!("Returned a non Not found error")
            }
        },
    };

    let _caps_push_client = match remote_connection.get_asset_push_client().await {
        Ok(caps_client) => {
            let caps_status = json!({
                "Status": "Start",
                "Test": "Get Asset push",
                "Endpoint": endpoint,
                "Debug": format!("{:?}", caps_client),
            });
            println!("{}", caps_status);
            caps_client
        }
        Err(err) => {
            let caps_status = json!({
                "Status": "Error",
                "Test": "Get Asset Client",
                "Endpoint": endpoint,
                "Error": format!("{:?}", err) ,
            });
            println!("{}", caps_status);
            anyhow::bail!("Could not get Capabilities Client")
        }
    };

    Ok(())
}

async fn test_fetch(
    remote_connection: &mut ReapiConnection,
    endpoint: &str,
    push: bool,
) -> Crapshoot {
    // Check the cas service
    // The REAPI standard says that all caches should contain the
    match fetch_blob(
        remote_connection,
        &Digest {
            hash: "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855".to_string(),
            size_bytes: 0,
        },
    )
    .await
    {
        Ok(blob) => {
            if blob.is_empty() {
                let fetch_status = json!({
                    "Status": "Start",
                    "Test": "Fetch blob",
                    "Endpoint": endpoint,
                });
                println!("{}", fetch_status);
            } else {
                let fetch_status = json!({
                    "Status": "Error",
                    "Test": "Fetch blob",
                    "Endpoint": endpoint,
                    "Error": "Fetched blob was not correct" ,
                });
                println!("{}", fetch_status);
                anyhow::bail!("Fetched blob was not correct")
            }
        }
        Err(err) => {
            let fetch_status = json!({
                "Status": "Error",
                "Test": "Fetch blob",
                "Endpoint": endpoint,
                "Error": format!("{:?}", err) ,
            });
            println!("{}", fetch_status);
            anyhow::bail!("Could not get fetch Client")
        }
    };

    if push {
        let new_blob = vec![0, 1, 2, 3, 4];

        let blob_digest = match upload_blob(remote_connection, new_blob.clone()).await {
            Ok(blob) => {
                let push_status = json!({
                    "Status": "OK",
                    "Test": "Fetch blob",
                    "Endpoint": endpoint,
                });
                println!("{}", push_status);
                blob
            }
            Err(err) => {
                let push_status = json!({
                    "Status": "Error",
                    "Test": "Push blob",
                    "Endpoint": endpoint,
                    "Error": format!("{:?}", err) ,
                });
                println!("{}", push_status);
                anyhow::bail!("Could not get Push Client")
            }
        };
        if blob_digest.size_bytes == 5 {
            let push_status = json!({
                "Status": "OK",
                "Test": "Push blob Matches",
                "Endpoint": endpoint,
            });
            println!("{}", push_status);
        } else {
            let push_status = json!({
                "Status": "Error",
                "Test": "Push blob",
                "Endpoint": endpoint,
            });
            println!("{}", push_status);
            anyhow::bail!("Error uploading")
        };

        let returned_blob = match fetch_blob(remote_connection, &blob_digest).await {
            Ok(blob) => {
                let push_status = json!({
                    "Status": "OK",
                    "Test": "Re Fetch blob",
                    "Endpoint": endpoint,
                });
                println!("{}", push_status);
                blob
            }
            Err(err) => {
                let push_status = json!({
                    "Status": "Error",
                    "Test": "Re Fetch blob",
                    "Endpoint": endpoint,
                    "Error": format!("{:?}", err) ,
                });
                println!("{}", push_status);
                anyhow::bail!("Error fetching");
            }
        };
        if new_blob == returned_blob {
            let push_status = json!({
                "Status": "OK",
                "Test": "Fetched blob matches",
                "Endpoint": endpoint,
            });
            println!("{}", push_status);
        } else {
            let push_status = json!({
                "Status": "Error",
                "Test": "Fetch blob does not match",
                "Endpoint": endpoint,
            });
            println!("{}", push_status);
            anyhow::bail!("Pulled blob did not match");
        };
    };
    Ok(())
}

async fn test_remote(remote: RemoteConfig) -> Crapshoot {
    let endpoint = remote.get_endpoint();
    let push = remote.can_push();
    let status = json!({
        "Status": "Begin",
        "Endpoint": endpoint,
        "Test Push": push,
    });
    println!("{}", status);
    let mut remote_connection = ReapiConnection::from(remote);

    let mut remote_connection_cap = remote_connection.clone();
    let mut remote_connection_asset = remote_connection.clone();
    let mut remote_connection_blobs = remote_connection.clone();
    let cap_test = test_caps(&mut remote_connection_cap, &endpoint);
    let fetch_test = test_fetch(&mut remote_connection, &endpoint, push);
    let asset_test = test_asset(&mut remote_connection_asset, &endpoint, push);
    let missing_test = test_missing_blobs(&mut remote_connection_blobs, &endpoint);

    // Wait for both to finish before we evaluate if ether failed
    let (cap_result, fetch_result, asset_result, missing_result) =
        join!(cap_test, fetch_test, asset_test, missing_test);

    let mut failed = false;
    if cap_result.is_err() {
        failed = true
    };
    if fetch_result.is_err() {
        failed = true
    };
    if asset_result.is_err() {
        failed = true
    };
    if missing_result.is_err() {
        failed = true
    };

    let final_status = json!({
        "Status": "Finished",
        "Endpoint": endpoint,
        "Result": if !failed {"Success"} else {"Failure"}
    });
    println!("{}", final_status);
    if failed {
        bail!("{endpoint} failed")
    } else {
        Ok(())
    }
}

#[tokio::main]
async fn main() -> Crapshoot {
    // Use the grd logic to load the remotes, this allows us to check a grd projects config
    let cli = CheckCli::parse();
    let project_root = cli.get_root()?;
    let mut remote_configs: Vec<RemoteConfig> = vec![];
    let remotes = RemotesConfig::open_from_file(&project_root.join(".remotes")).await;
    if let Ok(remotes) = remotes {
        for remote in remotes.get_remotes() {
            remote_configs.push(remote.clone().try_into()?);
        }
    } else {
        let error = json!({
            "Status": "Error",
            "Test": "Test Configuration",
            "Details": "Could not real remotes file",
        });
        println!("{}", error);
    }
    let config_path = match env::var("XDG_CONFIG_HOME") {
        Ok(path) => Path::new(&path).to_owned(),
        Err(_) => Path::new(&env::var("HOME").unwrap()).join(".config"),
    };
    let config_path = config_path.join("girderstream");
    let user_config = UserConfig::open_from_file(&config_path).await;
    remote_configs.extend(
        user_config
            .get_remotes()
            .map(|remote| remote.clone().try_into().unwrap()),
    );

    // Start testing all the remotes at the same time
    let mut futures = vec![];
    for remote in remote_configs {
        futures.push(tokio::spawn(test_remote(remote)));
    }
    // Wait for all the futures return
    let mut results = vec![];
    for future in futures {
        results.push(future.await);
    }
    let mut pass = true;
    // After all tests have finished then exit with failure if any failed.
    for result in results {
        if let Ok(inner) = result {
            if inner.is_err() {
                pass = false
            }
        } else {
            pass = false
        };
    }
    let final_status = json!({
        "Status": "Finished",
        "Result": if pass {"Success"} else {"Failure"}
    });
    println!("{}", final_status);
    if !pass {
        exit(1)
    }
    // If they all passed return success.
    Ok(())
}
