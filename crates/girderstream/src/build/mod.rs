// Copyright 2023 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// Perform a build locally
//! Translation layer between the Girderstream specific build functions and REAPI
//!
//! Currently this wraps buildbox-run but could be expended with minimal reworking
//! to also support remote execution over REAPI.

use std::collections::HashMap;

use anyhow::Ok;
use prost::Message;
use reapi::build::bazel::remote::execution::v2::{
    command::EnvironmentVariable, platform::Property, Action, ActionResult, Command as BzCommand,
    Digest, Platform,
};
use serde::{Deserialize, Serialize};

use reapi_tools::{
    cas_fs::{digest_from_directory, tree_from_digest},
    grpc_io::{fetch_blob, upload_blob},
    ReapiConnection, RemoteConfig,
};

mod localbuild;
mod remotebuild;

#[derive(Serialize, Deserialize, Eq, PartialEq, Debug)]
pub(crate) struct SubprocessResult {
    /// The exit status of the process running the build, eg buildbox-run
    pub(crate) exit_status: i32,
    /// The digest containing the stdout of the process running the build
    pub(crate) stdout: Digest,
    /// The digest containing the stderr of the process running the build
    pub(crate) stderr: Digest,
}

impl SubprocessResult {
    /// Get the stdout and stderr of the subprocess running the build
    pub(crate) async fn get_std_out_err(
        &self,
        connection: &mut ReapiConnection,
    ) -> anyhow::Result<(Option<String>, Option<String>)> {
        Ok((
            Some(string_from_digest(connection, &self.stdout).await?),
            Some(string_from_digest(connection, &self.stderr).await?),
        ))
    }
}

#[derive(Serialize, Deserialize, Eq, PartialEq, Debug)]
pub(crate) struct ReapiResult {
    pub(crate) code: i32,
    meta_data: String,
}

#[derive(Serialize, Deserialize, Eq, PartialEq, Debug)]
pub(crate) enum RunnerResults {
    BuildboxRun(SubprocessResult),
    Reapi(ReapiResult),
}
impl RunnerResults {
    pub fn success(&self) -> bool {
        match &self {
            RunnerResults::BuildboxRun(subprocess) => subprocess.exit_status == 0,
            RunnerResults::Reapi(reapi_result) => reapi_result.code == 200,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub(crate) struct GrdActionResult {
    /// The exit code of the action
    exit_code: i32,
    /// A digest containing the stdout out of action, or None.
    pub(crate) stdout_digest: Option<Digest>,
    /// A digest containing the stderr out of action, or None.
    pub(crate) stderr_digest: Option<Digest>,
    /// Any directories captured by the action, as a hashmap containing the dir name and the Digest of its [`v2::Directory`]
    pub(crate) output_directories: HashMap<String, Digest>,
}

pub(crate) async fn string_from_digest(
    connection: &mut ReapiConnection,
    digest: &Digest,
) -> anyhow::Result<String> {
    Ok(String::from_utf8_lossy(&fetch_blob(connection, digest).await?).to_string())
}

impl GrdActionResult {
    async fn from_action_results(
        connection: &mut ReapiConnection,
        build_type: ActionResult,
    ) -> anyhow::Result<Self> {
        let mut bob = HashMap::new();
        for out_dir in build_type.output_directories.iter() {
            let tree_digest = out_dir
                .tree_digest
                .as_ref()
                .ok_or(anyhow::anyhow!("missing directory blob"))?;

            let tree = tree_from_digest(connection, tree_digest).await?;
            let root = tree.root.unwrap();
            let root_digest = digest_from_directory(connection, root).await?;
            bob.insert(out_dir.path.clone(), root_digest);
        }

        Ok(Self {
            exit_code: build_type.exit_code,
            stdout_digest: build_type.stdout_digest,
            stderr_digest: build_type.stderr_digest,
            output_directories: bob,
        })
    }

    pub(crate) async fn get_std_out_err(
        &self,
        connection: &mut ReapiConnection,
    ) -> anyhow::Result<(Option<String>, Option<String>)> {
        let stdout = if let Some(di) = self.stdout_digest.as_ref() {
            Some(string_from_digest(connection, di).await?)
        } else {
            None
        };
        let stderr = if let Some(di) = self.stderr_digest.as_ref() {
            Some(string_from_digest(connection, di).await?)
        } else {
            None
        };

        Ok((stdout, stderr))
    }
}

/// Represents the result of a REAPI build or fetch action
///
/// This always has data about the action execution and if the executor returned
/// a action result then a [`GrdActionResult`] will also be in [`CommandResult::action`]
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub(crate) struct CommandResult {
    pub(crate) run_result: RunnerResults,
    pub(crate) action: Option<GrdActionResult>,
}

impl CommandResult {
    pub fn success(&self) -> bool {
        if !self.run_result.success() {
            return false;
        };
        if let Some(action) = &self.action {
            action.exit_code == 0
        } else {
            false
        }
    }

    pub(crate) fn get_result_digest(&self) -> Option<&Digest> {
        if let Some(action) = &self.action {
            action.output_directories.get("output_dir")
        } else {
            None
        }
    }
}

/// General entry point for running a build
///
/// This will take the first [`RemoteConfig`] from `remote_executers` and will
/// use it to run the command remotely, if `remote_executers` is empty then
/// a local executor will be used instead.
pub(crate) async fn run_build_plugin(
    connection: &mut ReapiConnection,
    arguments: Vec<String>,
    root_digest: Digest,
    with_network: bool,
    environment_variables: Vec<EnvironmentVariable>,
    remote_executers: impl Iterator<Item = &RemoteConfig> + '_,
    interactive: bool,
) -> anyhow::Result<CommandResult> {
    let result = run_command(
        connection,
        arguments,
        root_digest,
        None,
        with_network,
        environment_variables,
        remote_executers,
        interactive,
    )
    .await?;

    Ok(result)
}

/// This creates the REAPI action and runs it
///
/// This will take the first [`RemoteConfig`] from `remote_executers` and will
/// use it to run the command remotely, if `remote_executers` is empty then
/// a local executor will be used instead.
#[allow(clippy::too_many_arguments)]
pub(crate) async fn run_command(
    connection: &mut ReapiConnection,
    arguments: Vec<String>,
    root_digest: Digest,
    cwd: Option<String>,
    with_network: bool,
    environment_variables: Vec<EnvironmentVariable>,
    mut remote_executers: impl Iterator<Item = &RemoteConfig> + '_,
    interactive: bool,
) -> anyhow::Result<CommandResult> {
    let mut plt = Platform { properties: vec![] };
    // These values need to come from the project/element and be part of the cache key too

    if with_network {
        plt.properties.push(Property {
            name: "network".to_string(),
            value: "on".to_string(),
        });
    }

    let remote = remote_executers.next();
    if let Some(remote) = remote {
        let executer = remote.get_execution().unwrap();
        for (key, value) in executer.get_platform() {
            plt.properties.push(Property {
                name: key.to_owned(),
                value: value.to_owned(),
            });
        }
    } else {
        plt.properties.push(Property {
            name: "OSfamily".to_string(),
            value: "linux".to_string(),
        });
        plt.properties.push(Property {
            name: "ISA".to_string(),
            value: "x86-64".to_string(),
        });
    };
    let output_paths = if interactive {
        vec![]
    } else {
        vec![
            "output_dir".to_string(),
            "sources".to_string(),
            ".".to_string(),
        ]
    };
    let cmd = BzCommand {
        arguments,
        environment_variables,
        // be careful because these are relative to the cwd set in the 'working_directory' and must not be absolute.
        output_files: vec!["output.txt".to_string()],
        output_directories: vec![],
        output_paths,
        platform: Some(plt.clone()),
        working_directory: cwd.unwrap_or_default(),
        output_node_properties: vec![],
    };

    let mut buf: Vec<u8> = vec![];
    cmd.encode(&mut buf).unwrap();
    let command_digest = upload_blob(connection, buf).await?;

    run_input(connection, command_digest, root_digest, remote, interactive).await
}

/// This takes a REAPI command digest and triggers a executer
///
/// This function will invoke a remote build if `remote_executer` is Some()
/// or a local executor if it is `None`.
pub(crate) async fn run_input(
    connection: &mut ReapiConnection,
    command: Digest,
    input: Digest,
    remote_executer: Option<&RemoteConfig>,
    interactive: bool,
) -> anyhow::Result<CommandResult> {
    let mut plt = Platform { properties: vec![] };
    // These values need to come from the project/element and be part of the cache key too

    let mut use_remote = false;
    if let Some(remote) = remote_executer {
        let executer = remote.get_execution().unwrap();
        for (key, value) in executer.get_platform() {
            plt.properties.push(Property {
                name: key.to_owned(),
                value: value.to_owned(),
            });
        }
        use_remote = true;
    } else {
        plt.properties.push(Property {
            name: "network".to_string(),
            value: "on".to_string(),
        });
        plt.properties.push(Property {
            name: "OSfamily".to_string(),
            value: "linux".to_string(),
        });
        plt.properties.push(Property {
            name: "ISA".to_string(),
            value: "x86-64".to_string(),
        });
    }

    let action = Action {
        command_digest: Some(command),
        input_root_digest: Some(input),
        timeout: None,
        do_not_cache: true,
        salt: vec![],
        platform: Some(plt),
    };

    if use_remote {
        remotebuild::run_action(connection, remote_executer.unwrap(), action).await
    } else {
        localbuild::run_action(connection, action, interactive).await
    }
}
