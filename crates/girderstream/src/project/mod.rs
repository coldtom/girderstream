// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
/// This module holds the Project and Element information
///
/// This module has the functions and struts to load and then
/// store the hole project and its elements and then provide
/// the project specific functions to allow the build to
/// complete.
use std::collections::HashMap;

use anyhow::Ok;
use reapi::build::bazel::remote::execution::v2::Digest;
use serde::{Deserialize, Serialize};

use crate::build::CommandResult;
use reapi_tools::ReapiConnection;
pub mod element;
pub mod plugin;
// Todo: come up with some better names!
#[allow(clippy::module_inception)]
pub mod project;

#[derive(Serialize, Deserialize, Eq, PartialEq, Debug, Clone)]
pub struct ConfigWhen {
    #[serde(flatten)]
    inner: HashMap<String, String>,
}

impl ConfigWhen {
    pub fn met(&self, configuration: &HashMap<String, String>) -> anyhow::Result<bool> {
        let mut met = true;
        for (rule_condition, rule_value) in &self.inner {
            if let Some(project_value) = configuration.get(rule_condition) {
                if project_value != rule_value {
                    met = false;
                }
            } else {
                return Err(anyhow::Error::msg(
                    "Can not supply configuration option if the option's value is not defined in project.conf",
                ));
            }
        }
        Ok(met)
    }
}

#[derive(Serialize, Deserialize, Eq, PartialEq, Debug)]
enum RunState {
    Success,
    FailureInternal(String),
    Failure,
}

impl From<bool> for RunState {
    fn from(success: bool) -> Self {
        if success {
            RunState::Success
        } else {
            RunState::Failure
        }
    }
}

/// This struct represents a completed build and is used to cache a build
///
/// This struct holds all the information from a build that is then passed
/// to the elements that depend on it and is serialized to form the cached
/// element.
#[derive(Serialize, Deserialize, Eq, PartialEq, Debug)]
pub struct BuildResult {
    success: RunState,
    build_output: Option<Digest>,
    command_result: Option<CommandResult>,
    source_keys: Vec<String>,
}

impl BuildResult {
    /// Get the digest of the root of the generated files of this elements build
    pub(crate) fn get_result_digest(&self) -> Option<&Digest> {
        self.build_output.as_ref()
    }

    /// Get the digest of the root of the generated files of this elements build
    pub(crate) fn get_command_result(&self) -> Option<&CommandResult> {
        self.command_result.as_ref()
    }

    /// Does this cached result represent a successful fetch
    pub fn successfully_build(&self) -> bool {
        self.success == RunState::Success
    }

    /// Information about why the build did not build
    ///
    /// If the build failed due to a girderstream issue then failure
    /// information will be in the option. If the build succeeded or
    /// failed in due to non zero exit of the sandbox then this will
    /// be None.
    pub(crate) fn get_failure_info(&self) -> Option<String> {
        match &self.success {
            RunState::FailureInternal(err) => Some(err.into()),
            RunState::Failure | RunState::Success => None,
        }
    }

    /// The standard out of the local executor that performed the build
    ///
    /// The result returning [`Err`] means that something went unexpectedly wrong, eg network issue with reapi
    /// If the function return [`Ok(None)`] then the [`BuildResult`] never had a stdout in its
    /// [`BuildResult::command_result`] but this could be because [`CommandResult::run_result`] was
    /// [`crate::build::RunnerResults::Reapi`] or because it was a [`crate::build::RunnerResults::BuildboxRun`]
    /// but that that had its [`crate::build::SubprocessResult::stdout`] was [`None`]
    // It would be better to be a Enum to indicate why the stdout was not there.
    pub async fn stdout_runner(
        &self,
        connection: &mut ReapiConnection,
    ) -> anyhow::Result<Option<String>> {
        if let Some(command) = self.command_result.as_ref() {
            match &command.run_result {
                crate::build::RunnerResults::BuildboxRun(sub_process) => {
                    Ok(sub_process.get_std_out_err(connection).await?.0)
                }
                crate::build::RunnerResults::Reapi(_) => Ok(None),
            }
        } else {
            Ok(None)
        }
    }

    /// The standard out of the sandbox that performed the build
    ///
    /// The result returning [`Err`] means that something went unexpectedly wrong, eg network issue with reapi
    /// If the function return [`Ok(None)`] then the [`BuildResult`] never had a stdout in its
    /// [`BuildResult::command_result`] but this could be because [`BuildResult::command_result`] was [`None`]
    /// or because it was a [`crate::build::GrdActionResult`] but that that had its [`crate::build::GrdActionResult::stdout_digest`] was [`None`]
    // It would be better to be a Enum to indicate why the stdout was not there.
    pub async fn stdout(&self, connection: &mut ReapiConnection) -> anyhow::Result<Option<String>> {
        if let Some(command) = self.command_result.as_ref() {
            if let Some(grd_action_result) = command.action.as_ref() {
                Ok(grd_action_result.get_std_out_err(connection).await?.0)
            } else {
                Ok(None)
            }
        } else {
            Ok(None)
        }
    }

    /// The standard err of the local executor that performed the build
    ///
    /// The result returning [`Err`] means that something went unexpectedly wrong, eg network issue with reapi
    /// If the function return [`Ok(None)`] then the [`BuildResult`] never had a stderr in its
    /// [`BuildResult::command_result`] but this could be because [`CommandResult::run_result`] was
    /// [`crate::build::RunnerResults::Reapi`] or because it was a [`crate::build::RunnerResults::BuildboxRun`]
    /// but that that had its [`crate::build::SubprocessResult::stderr`] was [`None`]
    // It would be better to be a Enum to indicate why the stdout was not there.
    pub async fn stderr_runner(
        &self,
        connection: &mut ReapiConnection,
    ) -> anyhow::Result<Option<String>> {
        if let Some(command) = self.command_result.as_ref() {
            match &command.run_result {
                crate::build::RunnerResults::BuildboxRun(sub_process) => {
                    Ok(sub_process.get_std_out_err(connection).await?.1)
                }
                crate::build::RunnerResults::Reapi(_) => Ok(None),
            }
        } else {
            Ok(None)
        }
    }

    /// The standard error of the sandbox that performed the build
    ///
    /// The result returning [`Err`] means that something went unexpectedly wrong, eg network issue with reapi
    /// If the function return [`Ok(None)`] then the [`BuildResult`] never had a stderr in its
    /// [`BuildResult::command_result`] but this could be because [`BuildResult::command_result`] was [`None`]
    /// or because it was a [`crate::build::GrdActionResult`] but that that had its [`crate::build::GrdActionResult::stderr_digest`] was [`None`]
    // It would be better to be a Enum to indicate why the stdout was not there.
    pub async fn stderr(&self, connection: &mut ReapiConnection) -> anyhow::Result<Option<String>> {
        if let Some(command) = self.command_result.as_ref() {
            if let Some(grd_action_result) = command.action.as_ref() {
                Ok(grd_action_result.get_std_out_err(connection).await?.1)
            } else {
                Ok(None)
            }
        } else {
            Ok(None)
        }
    }
}

/// This struct represents a completed source fetch and is used to cache a source.
#[derive(Debug, Deserialize, Serialize)]
pub struct FetchResult {
    success: RunState,
    fetch_output: Option<Digest>,
    command_result: Option<CommandResult>,
}

impl FetchResult {
    /// Get the digest of the root of the fetched input files for this source
    pub(crate) fn get_result_digest(&self) -> Option<&Digest> {
        self.fetch_output.as_ref()
    }

    /// Does this cached result represent a successful fetch
    pub(crate) fn successfully_fetch(&self) -> bool {
        match self.success {
            RunState::Failure | RunState::FailureInternal(_) => false,
            RunState::Success => true,
        }
    }

    /// Information about why the source could not be fetched
    ///
    /// If the build failed due to a girderstream issue then failure
    /// information will be in the option. If the build succeeded or
    /// failed in due to non zero exit of the sandbox then this will
    /// be None.
    pub(crate) fn get_failure_info(&self) -> Option<String> {
        match &self.success {
            RunState::FailureInternal(err) => Some(err.into()),
            RunState::Failure | RunState::Success => None,
        }
    }

    /// The standard out of the sandbox that performed the fetch
    ///
    /// The result returning [`Err`] means that something went unexpectedly wrong, eg network issue with reapi
    /// If the function return [`Ok(None)`] then the [`BuildResult`] never had a std err in its
    /// [`BuildResult::command_result`] but this could be because [`BuildResult::command_result`] was [`None`]
    /// or because it was a [`crate::build::GrdActionResult`] but that that had its [`crate::build::GrdActionResult::stdout_digest`] was [`None`]
    // It would be better to be a Enum to indicate why the stdout was not there.
    pub async fn stdout(&self, connection: &mut ReapiConnection) -> anyhow::Result<Option<String>> {
        if let Some(command) = self.command_result.as_ref() {
            if let Some(grd_action_result) = command.action.as_ref() {
                Ok(grd_action_result.get_std_out_err(connection).await?.0)
            } else {
                Ok(None)
            }
        } else {
            Ok(None)
        }
    }

    /// The standard error of the sandbox that performed the fetch
    ///
    /// The result returning [`Err`] means that something went unexpectedly wrong, eg network issue with reapi
    /// If the function return [`Ok(None)`] then the [`BuildResult`] never had a std err in its
    /// [`BuildResult::command_result`] but this could be because [`BuildResult::command_result`] was [`None`]
    /// or because it was a [`crate::build::GrdActionResult`] but that that had its [`crate::build::GrdActionResult::stderr_digest`] was [`None`]
    // It would be better to be a Enum to indicate why the stdout was not there.
    pub async fn stderr(&self, connection: &mut ReapiConnection) -> anyhow::Result<Option<String>> {
        if let Some(command) = self.command_result.as_ref() {
            if let Some(grd_action_result) = command.action.as_ref() {
                Ok(grd_action_result.get_std_out_err(connection).await?.1)
            } else {
                Ok(None)
            }
        } else {
            Ok(None)
        }
    }
}
