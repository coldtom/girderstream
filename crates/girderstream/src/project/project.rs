// Copyright 2022-2024 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! The project holds all of the non elements specific configuration
//!
//! The project is the key structure that creates most other structures
//! and points to the other important structures.
//!
//! This module contains the project and its supporting structures.
use std::{collections::HashMap, ops::Deref, path::PathBuf, sync::Arc};

use anyhow::bail;
use async_recursion::async_recursion;
use itertools::Itertools;
use reapi::build::bazel::remote::execution::v2::Digest;
use serde::{Deserialize, Serialize};
use serde_either::StringOrStruct;

use super::{
    element::{
        DependenceProto, Element, ElementProto, ElementRef, InputElement, PluginVariables,
        SourceDetails,
    },
    plugin::{BuildPlugin, Compound, InputBuildPlugin, InputSourcePlugin, SourcePlugin},
    BuildResult, ConfigWhen, FetchResult, RunState,
};
use crate::{cache::get_cached_build, scheduler, Crapshoot};
use reapi_tools::{
    virtual_fs::{VirtualDir, VirtualNode},
    FullConfig, ReapiConnection,
};

/// Define a configuration object
///
/// This simple struct forces the yaml files to specify the `name` key word as
/// well as the value of the configuration. This makes it clear and allows
/// adding more values in the future. We could remove this in the future
/// if it is shown to be superfluous.
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub(crate) struct InputConfigurationOption {
    name: String,
}

/// A configuration within a Girderstream project
///
/// Every configuration must have a list of possible options and a default
/// the default will always be used unless the user explicitly changes it
/// and it can only be one fo the options.
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub(crate) struct InputConfiguration {
    default: String,
    options: Vec<InputConfigurationOption>,
}

impl InputConfiguration {
    /// Get the default value of the configuration
    pub fn get_default(&self) -> String {
        self.default.clone()
    }

    /// Get the other posable options of the configuration
    ///
    /// We must never set the configuration to be anything other than one of
    /// these values. Or everything will end up getting to complex and out of
    /// hand.
    pub fn get_options(&self) -> Vec<String> {
        self.options
            .iter()
            .map(|serde_option| serde_option.name.clone())
            .collect()
    }
}

/// A helper function for getting all the configs and there defaults
///
/// This is useful for the creation of the project from the serde project.
/// It would be nicer to provide meaningful type wrappers around Hashmap as done
/// else were in the project to help make things clearer and allow rustc to help
/// spot logic mistakes.
pub(crate) fn get_defaults_from_configs(
    config: &HashMap<String, InputConfiguration>,
) -> (HashMap<String, String>, HashMap<String, Vec<String>>) {
    let mut all_defaults = HashMap::new();
    let mut all_options = HashMap::new();
    for (config_name, config_struct) in config {
        all_defaults.insert(config_name.clone(), config_struct.get_default());
        all_options.insert(config_name.clone(), config_struct.get_options());
    }
    (all_defaults, all_options)
}

/// Provisioning options allow use Configuration options to control providers
///
/// The provisioning option uses the Configuration options to select which of
/// the possible providers to use, when a element requires somethings that
/// multiple elements provide.
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub(crate) struct ProvisioningOption {
    provision: String,
    provider: String,
    when: Option<ConfigWhen>,
}

// Instead of these 3 getters we could have a single into()
impl ProvisioningOption {
    /// Get the conditions for when this options should be used
    pub fn get_when(&self) -> Option<ConfigWhen> {
        self.when.clone()
    }

    /// Get what requirement that this options controls the provision of
    pub fn get_provision(&self) -> String {
        self.provision.clone()
    }

    /// Get what this options provides
    pub fn get_provider(&self) -> String {
        self.provider.clone()
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub(crate) struct InputContextDetail {
    value: String,
    when: String,
}

/// Resolve context variables for a given configuration
///
/// The contexts can set when their variables are set or what they are set to.
/// This function takes the serde context variables and resolves them to what
/// they should be set to for the projects configuration options.
///
/// If the variable is given but non of the when branches are true then the
/// variable will no be in the final HashMap.
fn filter_variables_for_when(
    variables: &HashMap<String, StringOrStruct<Vec<InputContextDetail>>>,
    configuration: &HashMap<String, String>,
) -> HashMap<String, String> {
    variables
        .iter()
        .filter_map(|(k, v)| match v {
            StringOrStruct::String(value) => Some((k.to_string(), value.to_string())),
            StringOrStruct::Struct(values) => {
                for val in values {
                    if let Some((option_name, option_value)) = val.when.split('=').collect_tuple() {
                        if let Some(config_value) = configuration.get(&option_name.to_string()) {
                            if config_value == &option_value.to_string() {
                                return Some((k.to_string(), val.value.to_string()));
                            }
                        }
                    } else {
                        panic!("Could not parse configuration options in context {:?}", val)
                    };
                }
                None
            }
        })
        .collect()
}

/// Project contexts provide a set of plugin and environment vars
///
/// This lets a element select a context and then get the relevant plugin and
/// environment vars it needs without having to have includes like bst does.
///
/// The context values are all found in the same place in girderstram projects
/// and the program should in the future also print them out if needed.
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub(crate) struct InputProjectContext {
    name: String,
    plugin_variables: HashMap<String, StringOrStruct<Vec<InputContextDetail>>>,
    environment_variables: HashMap<String, StringOrStruct<Vec<InputContextDetail>>>,
}

impl InputProjectContext {
    /// Create a ProjectContext from a SerdeProjectContext
    ///
    /// This function takes the SerdeProjectContext and resolves the whens from
    /// the given configuration and creates a ProjectContext with all of the
    /// `when`'s resolved.
    pub(crate) fn for_configuration(
        self,
        configuration: &HashMap<String, String>,
    ) -> ProjectContext {
        ProjectContext::new(
            self.name,
            filter_variables_for_when(&self.plugin_variables, configuration),
            filter_variables_for_when(&self.environment_variables, configuration),
        )
    }
}

/// SerdeProject defines the format of project.conf that girderstream
/// can read
///
/// `provides_map` lets you change what a element provides, this is handy
/// if your using a junction and the junction has different conventions.
/// That said we have not implemented junctions yet.
///
///
/// `provides_rules` is there for if you have a collision between providers
/// you can only have one element provide. if your project has more elements
/// trying to provide the same thing then you can use a provides_rule to spesify
/// which element to use. It may seem silly to have multiple providers of the same
/// thing but in the future this will let you:
/// - conditionally build your project, ie command line options
/// - if you junction two projects both providing the same thing you will
/// need to specify which one should be used.
/// - allow others junctioning you project to provide the most appropriate
/// provider for there context.
#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub(crate) struct InputProject {
    project_name: String,
    #[serde(rename = "min-version")]
    min_version: Option<String>,
    #[serde(rename = "Description")]
    description: Option<String>,
    #[serde(rename = "element-path")]
    elements_path: Option<String>,
    source_plugins: Option<Vec<InputSourcePlugin>>,
    build_plugins: Option<Vec<InputBuildPlugin>>,
    provides_map: Option<HashMap<String, String>>,
    configuration: Option<HashMap<String, InputConfiguration>>,
    provides_rules: Option<Vec<ProvisioningOption>>,
    contexts: Option<Vec<InputProjectContext>>,
}

impl InputProject {
    /// Create a `SerdeProject` from the file location of its file
    ///
    /// Note that we should allow this location to be a real or meta file in the
    /// future as junctioned projects will exist in the cas rather then on the
    /// file system.
    pub async fn new(
        connection: &mut ReapiConnection,
        root: &mut VirtualDir,
    ) -> anyhow::Result<Self> {
        let s = root.read_file(connection, "project.conf").await?;
        let serde_project: InputProject = serde_yaml::from_str(&s).expect("Could parse yaml");
        Ok(serde_project)
    }
}

/// Contexts hold groups of plugin vars and environments
///
/// The Context holds a set of default plugin and environment
/// variables that plugins can use.
///
/// Elements can set the context they fit in to. Then they
/// pull in there default plugin and environment vars from
/// that context instead of all files having the same project
/// defaults.
#[derive(Debug, Clone)]
pub(crate) struct ProjectContext {
    name: String,
    variables: HashMap<String, String>,
    environment: HashMap<String, String>,
}

impl ProjectContext {
    pub fn new(
        name: String,
        variables: HashMap<String, String>,
        environment: HashMap<String, String>,
    ) -> Self {
        ProjectContext {
            name,
            variables,
            environment,
        }
    }

    // This is needed once elements can set the context they use.
    pub fn get_name(&self) -> String {
        self.name.clone()
    }

    pub fn get_variables(&self) -> &HashMap<String, String> {
        &self.variables
    }
    pub fn get_environment(&self) -> &HashMap<String, String> {
        &self.environment
    }
}

/// A struct containing all the non-element specific information for a build
///
/// When preparing to do a build the project will need to be mutable
/// so that it can create all the elements needed for a build. But once
/// this state is created then the project can be borrowed, behind a
/// Arc to allow for cross thread operation in a non-mutable manor.
///
/// When building the elements are created by the project and the
/// elementRefs are returned on the public API of the project. But
/// then the elementRefs need the project to be passed to them so
/// that they can access the plugins etc.
///
/// Note: When sharing elements via a junction the junction will
/// load in a hole project as a sub-project. There for we will
/// need  to load projects from cas so we will need to change the
/// way projects are loaded. We need to create a meta-filesystem
/// which can be backed by a normal filesystem or the cas.
#[derive(Debug)]
pub struct Project {
    // This will be a meta_directory so it can point to a fs or a cas dir
    root: VirtualDir,
    // This will be a meta_directory so it can point to a fs or a cas dir
    elements_path: VirtualDir,
    elements: HashMap<Arc<str>, ElementRef>,
    provides: HashMap<String, InputElement>,
    source_plugins: HashMap<String, SourcePlugin>,
    build_plugins: HashMap<String, BuildPlugin>,
    provides_map: HashMap<String, String>,
    configuration: HashMap<String, String>,
    // This is only really needed for user info eg things like grd show etc
    configuration_options: HashMap<String, Vec<String>>,
    provide_rules: HashMap<String, String>,
    contexts: HashMap<String, ProjectContext>,
}

#[derive(PartialEq)]
enum JoinStrategy {
    SkipAll,
    SkipUnlessOverride,
    Keep,
}

impl JoinStrategy {
    fn from_string(join_strategy_str: &str) -> anyhow::Result<Self> {
        match join_strategy_str {
            "skip-all" => Ok(JoinStrategy::SkipAll),
            "skip-unless-override" => Ok(JoinStrategy::SkipUnlessOverride),
            "keep" => Ok(JoinStrategy::Keep),
            _ => {
                anyhow::bail!(
                    "Could not determine join strategy for {}",
                    join_strategy_str
                )
            }
        }
    }
}

fn join_state<T>(
    hash_map_like: &mut HashMap<String, T>,
    join_map_like: HashMap<String, T>,
    junction_vars: &PluginVariables,
    type_name: String,
    default_join: JoinStrategy,
) -> Crapshoot {
    let join_strategy_name = format!("join-{}-strategy", type_name);
    let join_strategy: JoinStrategy =
        if let Some(join_strategy_string) = junction_vars.get(&join_strategy_name) {
            let join_strategy = JoinStrategy::from_string(join_strategy_string)?;
            if join_strategy == JoinStrategy::SkipAll {
                return Ok(());
            }
            join_strategy
        } else {
            default_join
        };

    let over_rides = {
        let mut over_rides = HashMap::new();
        for (var, val) in junction_vars.iter() {
            let over = format!("over-ride-{}-", type_name);
            if var.starts_with(&over) {
                let name = var[over.len()..].to_string();
                over_rides.insert(name.to_owned(), val.clone());
            }
        }
        over_rides
    };

    let skip_name = format!("skip-{}-prefix", type_name);
    let skip_context_prefix = junction_vars.get(&skip_name);
    for (name, variables) in join_map_like {
        if let Some(over_ride) = over_rides.get(&name) {
            if over_ride == "skip" {
                continue;
            }
            hash_map_like.insert(over_ride.to_owned(), variables);
            continue;
        };
        if join_strategy == JoinStrategy::SkipUnlessOverride {
            continue;
        }
        if let Some(full_skip_prefix) = skip_context_prefix {
            if name.starts_with(full_skip_prefix) {
                continue;
            }
        };
        match hash_map_like.entry(name) {
            std::collections::hash_map::Entry::Vacant(e) => {
                e.insert(variables);
            }
            std::collections::hash_map::Entry::Occupied(e) => {
                let raw_name = e.key();
                anyhow::bail!("Cant have two {type_name} with {raw_name} even through a junction");
            }
        }
    }
    Ok(())
}

impl Project {
    pub(crate) fn get_root(&self) -> &VirtualDir {
        &self.root
    }

    pub async fn get_un_built_junctions(&mut self) -> anyhow::Result<Vec<ElementRef>> {
        let mut names_to_build = vec![];
        for (name, serde_element) in &self.provides {
            if serde_element.kind() == "junction" {
                names_to_build.push(Arc::from(name.as_str()));
                //self.get_element(&name).await?)
            }
        }
        let mut built_refs = vec![];
        for name_to_build in names_to_build {
            built_refs.push(self.get_element(&name_to_build).await?)
        }

        Ok(built_refs)
    }

    pub fn join_junction(
        &mut self,
        plugin_variables: &PluginVariables,
        junctioned_project: Project,
    ) -> Crapshoot {
        let mut sources = junctioned_project.source_plugins;
        sources.remove("cas").unwrap();
        join_state(
            &mut self.source_plugins,
            sources,
            plugin_variables,
            "source".to_string(),
            JoinStrategy::SkipUnlessOverride,
        )?;

        let mut builders = junctioned_project.build_plugins;
        builders.remove("bootstrap_import").unwrap();
        builders.remove("junction").unwrap();
        join_state(
            &mut self.build_plugins,
            builders,
            plugin_variables,
            "build".to_string(),
            JoinStrategy::SkipUnlessOverride,
        )?;

        join_state(
            &mut self.contexts,
            junctioned_project.contexts,
            plugin_variables,
            "contexts".to_string(),
            JoinStrategy::SkipUnlessOverride,
        )?;

        join_state(
            &mut self.provides,
            junctioned_project.provides,
            plugin_variables,
            "elements".to_string(),
            JoinStrategy::Keep,
        )?;

        Ok(())
    }

    #[async_recursion]
    pub async fn load_junctions(
        full_config: &FullConfig,
        mut this_project: Project,
        force: bool,
    ) -> anyhow::Result<Project> {
        // TODO how the heck to make this into a member function..?
        let junction_elements = this_project.get_un_built_junctions().await?;
        println!("unbuilt junctions: {junction_elements:?}");
        let locked_project = Arc::new(this_project);
        {
            for junc in &junction_elements {
                scheduler::build(full_config, junc, locked_project.clone(), force).await?;
            }
            println!("built junctions");
        }

        let mut this_project = Arc::<Project>::try_unwrap(locked_project).unwrap();
        for junc in junction_elements {
            let load_sub =
                if let Some(config) = junc.get_plugin_variables().get("load-sub-junctions") {
                    config.trim().parse::<bool>().unwrap()
                } else {
                    false
                };

            let mut join_options = HashMap::new();
            if let Some(raw_option_names) = junc.get_plugin_variables().get("pass-though-options") {
                #[allow(clippy::needless_collect)]
                // The collect lets use do the `contains()` inside the for loop
                let option_names: Vec<&str> = raw_option_names.split(',').collect();
                for (config_name, config_option) in &this_project.configuration {
                    if option_names.contains(&config_name.as_str()) {
                        join_options.insert(config_name.to_owned(), config_option.to_owned());
                    }
                }
            }
            let mut connection = full_config.get_local().to_owned().into();
            let elem_result = get_cached_build(&mut connection, &this_project, &junc)
                .await
                .unwrap();
            let junc_project = Project::new_from_cas(
                full_config,
                elem_result.get_result_digest().unwrap().clone(),
                Some(join_options),
                load_sub,
                force,
            )
            .await
            .unwrap();
            this_project.join_junction(&junc.get_plugin_variables(), junc_project)?;

            // TODO load in the sources and maybe some configuration etc
        }
        Ok(this_project)
    }

    #[async_recursion]
    async fn walk_elements(&mut self, connection: &mut ReapiConnection) -> Crapshoot {
        // Walk the element directory and find all the elements
        // We need this so that we can find all the provides and work out
        // the build graph
        // This could be expensive for LARGE projects, so A) dont make your projects
        // too big for the sake of it, B) lets just load enough to work out build
        // graphs. Both in terms of cpu and ram.
        // The second thing is to calculate cache keys so if we can get that info on this
        // pass then thats nice but not key.
        for entry in self.elements_path.walk(connection).await {
            match entry {
                VirtualNode::VirtualFile(mut file) => {
                    if file
                        .get_extension()
                        .unwrap_or_else(|| panic!("no ex, {file:?}"))
                        == "grd"
                    {
                        let element = InputElement::new(connection, &mut file).await?;

                        let provide = self.map_required_element(
                            &element
                                .provides()
                                .unwrap_or_else(|| file.file_stem().unwrap()),
                        );

                        // Todo/Fix: This doesn't work if your rule is "folder/element.grd"
                        if let Some(provider) = self.provide_rules.get(&provide) {
                            if file.file_name().unwrap() != provider.as_str() {
                                continue;
                            }
                        }
                        if self.provides.contains_key(&provide) {
                            return Err(anyhow::Error::msg(
                                format!("Overlapping providers: for \"{}\" you must configure a non over-lapping tree.", provide),
                            ));
                        }
                        self.provides.insert(provide, element);
                        //}
                    }
                }
                VirtualNode::VirtualDir(_) => {}
            }
        }
        Ok(())
    }

    pub async fn new_from_dir(
        full_config: &FullConfig,
        root: PathBuf,
        configuration_option: Option<HashMap<String, String>>,
        load_junctions: bool,
        force: bool,
    ) -> anyhow::Result<Self> {
        Self::new(
            full_config,
            VirtualDir::Local(root),
            configuration_option,
            load_junctions,
            force,
        )
        .await
    }

    pub async fn new_from_cas(
        full_config: &FullConfig,
        root: Digest,
        configuration_option: Option<HashMap<String, String>>,
        load_junctions: bool,
        force: bool,
    ) -> anyhow::Result<Self> {
        Self::new(
            full_config,
            VirtualDir::CasBacked((root, "".to_string())),
            configuration_option,
            load_junctions,
            force,
        )
        .await
    }

    /// Create a new project from a given folder and configuration options
    ///
    /// Projects should be loaded by the smallest possible amount of information
    /// So it can be very clear what went into a project and to make it as easy
    /// as possible for others to replicate what was built and as easy as posable
    /// for users to be certain that they are building the same thing as they did
    /// or will do.
    ///
    /// Note: Currently the root is given as a path on a normal filesystem but
    /// we will go towards a meta-filesystem that can be backed by a normal or cas
    /// file system.
    ///
    /// Note: Projects can be the project being built or a sub project being
    /// junctioned into another build for its recipes/elements to be shared.
    async fn new(
        full_config: &FullConfig,
        mut root: VirtualDir,
        configuration_option: Option<HashMap<String, String>>,
        load_junctions: bool,
        force: bool,
    ) -> anyhow::Result<Self> {
        // This function seems to be way too long. It is already in blocks
        // that could each be a manageable sized function
        let mut connection = full_config.get_local().to_owned().into();
        let raw_project = InputProject::new(&mut connection, &mut root).await?;
        let elements_path = root
            .get_dir_name(
                &mut connection,
                &PathBuf::from(&raw_project.elements_path.unwrap_or("elements".to_string()))
                    .as_os_str()
                    .to_string_lossy(),
            )
            .await;

        let (configuration, configuration_options) = if let Some(project_default_config) =
            raw_project.configuration
        {
            let (mut default_config_options, configuration_options) =
                get_defaults_from_configs(&project_default_config);
            if let Some(provided_config) = configuration_option {
                for (provided_key, provided_value) in provided_config {
                    if let Some(options) = project_default_config.get(&provided_key) {
                        let options_strings = options.get_options();
                        if !options_strings.contains(&provided_value) {
                            return Err(anyhow::Error::msg(
                                "Can not supply configuration option value not in the configuration options defined in project.conf",
                            ));
                        }
                    } else {
                        return Err(anyhow::Error::msg(
                            "Can not supply configuration option not in the configuration options defined in project.conf",
                        ));
                    };
                    default_config_options.insert(provided_key, provided_value);
                }
                (default_config_options, configuration_options)
            } else {
                (default_config_options, configuration_options)
            }
        } else {
            if let Some(_config) = configuration_option {
                return Err(anyhow::Error::msg(
                    "Can not supply configuration option if configuration is not defined in project.conf",
                ));
            };
            (HashMap::new(), HashMap::new())
        };

        let mut provide_rules = HashMap::new();
        for rule in raw_project.provides_rules.unwrap_or_default() {
            if let Some(conditions) = rule.get_when() {
                if conditions.met(&configuration)? {
                    // todo: fail if already exists
                    if let Some(_existing) =
                        provide_rules.insert(rule.get_provision(), rule.get_provider())
                    {
                        bail!(
                            "Could not add duplicate provider for {}",
                            rule.get_provision()
                        )
                    };
                }
            } else if let Some(_existing) =
                provide_rules.insert(rule.get_provision(), rule.get_provider())
            {
                bail!(
                    "Could not add duplicate provider for {}",
                    rule.get_provision()
                )
            };
        }

        let mut source_plugins: HashMap<String, SourcePlugin> = HashMap::new();
        source_plugins.insert("cas".to_string(), SourcePlugin::new_cas());
        for raw_source_plugin in raw_project.source_plugins.unwrap_or_default() {
            let source_plugin = SourcePlugin::from(raw_source_plugin);
            source_plugins.insert(source_plugin.get_kind().clone(), source_plugin);
        }

        let mut build_plugins: HashMap<String, BuildPlugin> = HashMap::new();
        let bootstrap = BuildPlugin::new_bootstrap_import();
        build_plugins.insert(bootstrap.get_kind().clone(), bootstrap);
        build_plugins.insert("junction".to_string(), BuildPlugin::new_junction());
        for raw_build_plugin in raw_project.build_plugins.unwrap_or_default() {
            let build_plugin = BuildPlugin::from(raw_build_plugin);
            build_plugins.insert(build_plugin.get_kind().clone(), build_plugin);
        }

        let ordered_contexts: Vec<ProjectContext> = raw_project
            .contexts
            .unwrap_or_default()
            .into_iter()
            .map(|serde_context| serde_context.for_configuration(&configuration))
            .collect();
        let contexts: HashMap<String, ProjectContext> = ordered_contexts
            .into_iter()
            .map(|context: ProjectContext| (context.get_name(), context))
            .collect();

        let mut project = Project {
            root,
            elements_path,
            elements: HashMap::new(),
            provides: HashMap::new(),
            source_plugins,
            build_plugins,
            provides_map: raw_project.provides_map.unwrap_or_default(),
            configuration,
            configuration_options,
            provide_rules,
            contexts,
        };

        project.walk_elements(&mut connection).await?;

        if load_junctions {
            loop {
                project = Project::load_junctions(full_config, project, force).await?;
                if project
                    .provides
                    .iter()
                    .filter(|(_, input_element)| input_element.kind() == "junction")
                    .count()
                    == 0
                {
                    break;
                }
            }
        };

        Ok(project)
    }

    /// Get the bytes to uniquely identify a source
    ///
    /// This must be combined with the bytes of the dependencies to get a
    /// completely unique identifiers.
    pub(crate) fn get_source_bytes(&self, kind: String, source_details: &SourceDetails) -> Vec<u8> {
        self.source_plugins
            .get(&kind)
            .unwrap()
            .get_unique_bytes(source_details)
            .into()
    }

    /// Get the dependencies needed to use a source plugin
    ///
    /// typically the plugin its self and a runtime for it to run in.
    pub(crate) fn get_source_plugin_dependencies(
        &self,
        plugin: &str,
    ) -> impl Iterator<Item = DependenceProto> + '_ {
        // do not unwrap here!
        self.source_plugins
            .get(plugin)
            .unwrap_or_else(|| panic!("No source dependency provides {}", plugin))
            .get_dependencies()
            .iter()
            .map(|dep| DependenceProto::from_name_plugin_source(dep.to_owned()))
    }

    /// Get the dependencies needed to use a build plugin
    ///
    /// typically the plugin its self and a runtime for it to run in.
    pub(crate) fn get_build_plugin_dependencies(
        &self,
        plugin: &str,
    ) -> impl Iterator<Item = DependenceProto> + '_ {
        // do not unwrap here!
        self.build_plugins
            .get(plugin)
            .unwrap_or_else(|| panic!("No build dependency provides {}", plugin))
            .get_dependencies()
            .iter()
            .map(|dep| DependenceProto::from_name_plugin_build(Arc::from(dep.as_str())))
    }

    /// Get the name that a element should be referred to as
    ///
    /// In other words what should other elements require and
    /// what does this element provide.
    fn map_required_element(&self, required: &str) -> String {
        if self.provides_map.contains_key(required) {
            self.provides_map.get(required).unwrap().clone()
        } else {
            required.to_string()
        }
    }

    fn _map_required_elements(&self, required: Vec<String>) -> Vec<String> {
        required
            .iter()
            .map(|mapping| self.map_required_element(mapping))
            .collect()
    }

    /// Retrieve build plugins by there name
    ///
    /// If a build plugin exists with the given name then returns with a borrow
    /// of the plugin otherwise returns None.
    pub(crate) fn get_build_plugin(&self, plugin_name: &String) -> Option<&BuildPlugin> {
        self.build_plugins.get(plugin_name)
    }

    /// Get a existing ElementRef
    ///
    /// This function takes a immutable borrow of the project so can
    /// be used during the build once the project is inside a Arc for
    /// elements or anything else to get refs to other elements. But as
    /// the element must already exist you must ensure that the
    /// requested element was already created.
    pub fn get_existing_element(&self, root_element: &Arc<str>) -> anyhow::Result<ElementRef> {
        if let Some(existing_element) = self.elements.get(root_element) {
            return Ok(existing_element.clone());
        };
        Err(anyhow::Error::msg(format!(
            "Could not find element {root_element}"
        )))
    }

    /// Creates or returns a created element as a ElementRef
    ///
    /// Because this will create the element in needed this function
    /// needs the project to be mutable so it must be called before
    /// a build is started.
    ///
    /// This function will only return the elementRef of the root element
    /// but it will ensure that all of that elements direct and indirect
    /// dependencies are also created so they can be retreaved with
    /// `get_existing_element` later.
    #[async_recursion]
    pub async fn get_element(&mut self, root_element: &Arc<str>) -> anyhow::Result<ElementRef> {
        // Clean up element name, stripping .grd if there
        let name = root_element.deref();
        let root_element_stem: Arc<str> = if name.ends_with(".grd") {
            Arc::from(&name[0..name.len() - 4])
        } else {
            root_element.clone()
        };
        // Check if this element has already been initialized.
        if let Some(existing_element) = self.elements.get(&root_element_stem) {
            return Ok(existing_element.clone());
        }
        // Load this first element from its serde rep to be a real element
        let serde_element = self
            .provides
            .remove(&root_element_stem.deref().to_string())
            .unwrap_or_else(|| panic!("no element provides {}", root_element));
        let element = ElementProto::from_serde(serde_element, self)
            .map_err(|err| anyhow::anyhow!("Ran into {err:?} when processing {root_element}"))?;
        let mut required_elements: Vec<Arc<str>> = element
            .get_all_deps()
            .map(|dep| dep.get_name().clone())
            .collect();
        //required_elements.reverse();

        // Make sure that all of its dependencies and transitive dependencies are also created
        // as real elements
        while let Some(next_element) = required_elements.pop() {
            if self.elements.contains_key(&next_element) {
                continue;
            }
            self.get_element(&next_element).await?;
        }
        // at this point element could be mut, and then you can replace each dep with a dep that has a element ref instead of a name.
        // then other things can point strait to the element ref rather than having todo a big look up

        let element = Element::from_proto(root_element.clone(), element, self)?;
        let element_ref = ElementRef::new(element);
        self.elements.insert(root_element_stem, element_ref.clone());

        Ok(element_ref)
    }

    pub fn get_selected_configuration(&self) -> &HashMap<String, String> {
        &self.configuration
    }

    /// Get the configuration state
    ///
    /// Get the selected configuration along with the options that
    /// the config option was selected from.
    ///
    /// This is useful to be recorded so that others can make sure they are
    /// doing the same if they want to recreate your build.
    pub fn get_configuration(&self) -> HashMap<String, (String, Vec<String>)> {
        let mut result = HashMap::new();
        for (config_name, config_selected) in &self.configuration {
            result.insert(
                config_name.clone(),
                (
                    config_selected.clone(),
                    self.configuration_options.get(config_name).unwrap().clone(),
                ),
            );
        }

        result
    }

    /// Get the contexts of the project
    pub(crate) fn get_context(&self) -> &HashMap<String, ProjectContext> {
        &self.contexts
    }

    /// Retrieve the contents of a source as a digest
    ///
    /// This runs the source plugin within the sandbox and
    /// returns a pointer to that source within the cas.
    pub(crate) async fn get_source_digest(
        &self,
        full_config: &FullConfig,
        source_details: &SourceDetails,
        base: Option<&Digest>,
    ) -> anyhow::Result<(String, FetchResult, bool)> {
        let source_type = source_details.kind();
        let source_plugin = self.source_plugins.get(&source_type).unwrap_or_else(|| {
            panic!("Souece plugin {source_type} must be defined in proejct.conf")
        });

        let (key, fetch_result) = source_plugin
            .get_source_digest(self, full_config, source_details, base)
            .await?;
        Ok((
            key,
            fetch_result,
            source_plugin.get_compound() == &Compound::Replace,
        ))
    }

    pub(crate) fn get_source(&self, kind: &String) -> &SourcePlugin {
        self.source_plugins.get(kind).unwrap()
    }

    /// Build a given element
    ///
    /// Note that this is a async function so it will not block
    /// but it may take a long time before the future returns
    /// complete as some software takes a long time to build.
    pub(crate) async fn build_element(
        &self,
        full_config: &FullConfig,
        element: &ElementRef,
    ) -> BuildResult {
        let build_type = element.get_kind();
        let build_plugin = self.build_plugins.get(&build_type).expect("We should have checked and failed way before now if there is no build plugin of the right kind");
        match build_plugin.build(self, full_config, element).await {
            Ok(build_result) => build_result,
            Err(e) => {
                // This is not giving a lot of info but it does at least follow the patten
                // and any rerun can provide at least a little useful info.
                BuildResult {
                    build_output: None,
                    success: RunState::FailureInternal(format!("Internal build error {e:?}")),
                    source_keys: vec![],
                    command_result: None,
                }
            }
        }
    }

    /// Create a Project for unit testing
    ///
    /// This function is only compiled when unit testing and allow
    /// unit tests to quickly create a Project so that they
    /// can test how other code paths work without having to have
    /// a full integration tests worth of over head.
    #[cfg(test)]
    pub(crate) fn for_test() -> Self {
        let mut build_plugins = HashMap::new();
        build_plugins.insert("bootstrap_import".to_string(), BuildPlugin::for_test());
        let mut source_plugins: HashMap<String, SourcePlugin> = HashMap::new();
        source_plugins.insert("cas".to_string(), SourcePlugin::new_cas());
        Project {
            root: VirtualDir::Local(PathBuf::new()),
            elements_path: VirtualDir::Local(PathBuf::new()),
            elements: HashMap::new(),
            provides: HashMap::new(),
            source_plugins,
            build_plugins,
            provides_map: HashMap::new(),
            configuration: HashMap::new(),
            configuration_options: HashMap::new(),
            provide_rules: HashMap::new(),
            contexts: HashMap::new(),
        }
    }

    #[cfg(test)]
    pub(crate) fn for_test_configuration(configuration: HashMap<String, String>) -> Self {
        let mut build_plugins = HashMap::new();
        build_plugins.insert("bootstrap_import".to_string(), BuildPlugin::for_test());
        let mut source_plugins: HashMap<String, SourcePlugin> = HashMap::new();
        source_plugins.insert("cas".to_string(), SourcePlugin::new_cas());
        Project {
            root: VirtualDir::Local(PathBuf::new()),
            elements_path: VirtualDir::Local(PathBuf::new()),
            elements: HashMap::new(),
            provides: HashMap::new(),
            source_plugins,
            build_plugins,
            provides_map: HashMap::new(),
            configuration,
            configuration_options: HashMap::new(),
            provide_rules: HashMap::new(),
            contexts: HashMap::new(),
        }
    }

    #[cfg(test)]
    pub(crate) fn add_serde_test_element(&mut self, name: String, element: InputElement) {
        self.provides.insert(name, element);
    }

    #[cfg(test)]
    pub(crate) fn add_test_build_plugin(&mut self, name: String, builder: BuildPlugin) {
        self.build_plugins.insert(name, builder);
    }

    #[cfg(test)]
    pub(crate) fn add_test_source_plugin(&mut self, name: String, builder: SourcePlugin) {
        self.source_plugins.insert(name, builder);
    }
}

#[cfg(test)]
mod tests {
    use std::{collections::HashMap, sync::Arc};

    use anyhow::Ok;
    use serde_either::StringOrStruct;

    use crate::{
        project::{
            element::InputElement,
            project::{Project, ProjectContext},
        },
        Crapshoot,
    };

    #[tokio::test]
    async fn test_most_simple_element() -> Crapshoot {
        let mut project = Project::for_test();

        // This section is a very basic check of the source import and preps for the next section of the test
        let element_name = "BaseElement".to_string();
        let serde_element = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some("bob".to_string()),
            None,
        );
        project.add_serde_test_element(element_name.clone(), serde_element);
        let element = project
            .get_element(&Arc::from(element_name.as_str()))
            .await
            .unwrap();

        assert_eq!(&Arc::from(element_name.as_str()), element.get_name());
        Ok(())
    }

    #[tokio::test]
    async fn test_single_dep() -> Crapshoot {
        let mut project = Project::for_test();

        // Create a base input element
        let element_name = "BaseElement".to_string();
        let element_name_arc = Arc::from(element_name.as_str());
        let serde_element = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some("bob".to_string()),
            None,
        );
        project.add_serde_test_element(element_name.clone(), serde_element);

        // Create a imput element that depends on the base element
        let element_second_name = "SecondElement".to_string();
        let serde_second_element = InputElement::new_for_test(
            "bootstrap_import".to_string(),
            Some("bob".to_string()),
            Some(vec![StringOrStruct::String(element_name.clone())]),
        );
        project.add_serde_test_element(element_second_name.clone(), serde_second_element);
        assert_eq!(project.elements.len(), 0);
        assert_eq!(project.provides.len(), 2);

        // Get the top element
        let element_second = project
            .get_element(&Arc::from(element_second_name.as_str()))
            .await
            .unwrap();
        assert_eq!(project.elements.len(), 2);
        assert_eq!(project.provides.len(), 0);

        // Inspect the created element
        assert_eq!(
            &Arc::from(element_second_name.as_str()),
            element_second.get_name()
        );
        let mut deps = element_second.get_all_deps();
        let dep = deps.next().expect("Second element is missing its dep");
        assert_eq!(dep.get_name(), &element_name_arc);
        assert_eq!(deps.next(), None);

        // Create a non-mut version of project
        let project = project;

        // Ensure that the build deps of a created element are also accessible
        let base_element = project
            .get_existing_element(&element_name_arc)
            .expect("Failed to get base element");
        assert_eq!(base_element.get_name(), &element_name_arc);
        assert_eq!(base_element.get_all_deps().next(), None);

        Ok(())
    }

    #[tokio::test]
    async fn test_context() -> Crapshoot {
        let mut project = Project::for_test();
        let mut variables = HashMap::new();
        variables.insert("extra".to_string(), "value".to_string());
        let context = ProjectContext {
            name: "basic context".to_string(),
            variables,
            environment: HashMap::new(),
        };
        project
            .contexts
            .insert("basic context".to_string(), context);

        // This section is a very basic check of the source import and preps for the next section of the test
        let element_name = "BaseElement".to_string();
        let serde_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            Some("bob".to_string()),
            vec![],
            None,
            Some("basic context".to_string()),
            None,
        );
        project.add_serde_test_element(element_name.clone(), serde_element);
        let element = project
            .get_element(&Arc::from(element_name.as_str()))
            .await
            .unwrap();

        assert_eq!(&Arc::from(element_name.as_str()), element.get_name());
        assert_eq!(
            element.get_plugin_variables().get("extra"),
            Some(&"value".to_string())
        );

        // This section is a very basic check of the source import and preps for the next section of the test
        let second_element_name = "SecondBaseElement".to_string();
        let serde_element = InputElement::new_for_test_extra(
            "bootstrap_import".to_string(),
            Some("bob".to_string()),
            vec![],
            None,
            None,
            None,
        );
        project.add_serde_test_element(second_element_name.clone(), serde_element);
        let second_element = project
            .get_element(&Arc::from(second_element_name.as_str()))
            .await
            .unwrap();

        assert_eq!(
            &Arc::from(second_element_name.as_str()),
            second_element.get_name()
        );
        assert_eq!(second_element.get_plugin_variables().len(), 0);

        Ok(())
    }
}
