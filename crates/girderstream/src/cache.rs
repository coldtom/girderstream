// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! Push and fetch artifacts from the cache
// This is currently very simple as we just consider the local cache but we will
// need to expand this once we support multiple cache servers and remote builds.

use reapi::build::bazel::remote::execution::v2::Digest;
use sha2::{Digest as _, Sha256};

use crate::{
    project::{
        element::{ElementRef, SourceDetails},
        plugin::SourcePlugin,
        project::Project,
        BuildResult, FetchResult,
    },
    Crapshoot,
};
use reapi_tools::{
    cas_fs::check_directory,
    grpc_io::{fetch_asset, fetch_blob, push_asset, upload_blob},
    ReapiConnection,
};

const BUILD_ASSET_KEY: &str = "0.0.5";

/// Calculate the assets key used to identify a cached build
///
/// This key is used when adding or retrieving cached builds from the
/// REAPI asset store.
pub(crate) fn build_asset_key(_project: &Project, element_ref: &ElementRef) -> String {
    let cache_key = element_ref.cache_key();
    format!(
        "{}::{}::{}",
        BUILD_ASSET_KEY,
        element_ref.get_kind(),
        cache_key
    )
}

const FETCH_ASSET_KEY: &str = "0.0.4";

/// Calculate the assets key used to identify a cached fetch
///
/// This key is used when adding or retrieving cached sources from the
/// REAPI asset store.
pub(crate) fn fetch_asset_key(
    root_digest: &Digest,
    source_plugin: &SourcePlugin,
    source_map: &SourceDetails,
) -> String {
    let mut dig = Sha256::new();
    dig.update(root_digest.hash.as_bytes());
    dig.update(root_digest.size_bytes.to_ne_bytes());
    // get_unique_bytes includes things like cli and is similar to `element_ref.cache_key`
    dig.update(source_plugin.get_unique_bytes(source_map));
    format!(
        "{}:{}:{}",
        FETCH_ASSET_KEY,
        source_plugin.get_kind(),
        hex::encode(dig.finalize()),
    )
}

/// Add a build result for a element to the cache
pub(crate) async fn cache_build(
    connection: &mut ReapiConnection,
    project: &Project,
    element_ref: &ElementRef,
    build_result: &BuildResult,
) -> Crapshoot {
    let asset_key = build_asset_key(project, element_ref);

    let encoded_result = bincode::serialize::<BuildResult>(build_result).unwrap();
    let encoded_result_digest = upload_blob(connection, encoded_result).await?;
    let reference_directories = if let Some(result) = build_result.get_result_digest() {
        vec![result.to_owned()]
    } else {
        vec![]
    };

    push_asset(
        connection,
        &asset_key,
        encoded_result_digest,
        reference_directories,
    )
    .await?;

    Ok(())
}

/// Add a fetch result for a source to the cache
pub(crate) async fn cache_fetch(
    connection: &mut ReapiConnection,
    asset_key: &String,
    fetch_result: &FetchResult,
) -> Crapshoot {
    let encoded_result = bincode::serialize::<FetchResult>(fetch_result).unwrap();
    let encoded_result_digest = upload_blob(connection, encoded_result).await?;
    let reference_directories = if let Some(result) = fetch_result.get_result_digest() {
        vec![result.to_owned()]
    } else {
        vec![]
    };

    push_asset(
        connection,
        asset_key,
        encoded_result_digest,
        reference_directories,
    )
    .await?;

    Ok(())
}

/// Retrieve a cached builds result from the cache
pub(crate) async fn get_cached_build(
    connection: &mut ReapiConnection,
    project: &Project,
    element_ref: &ElementRef,
) -> anyhow::Result<BuildResult> {
    let asset_key = build_asset_key(project, element_ref);

    let build_asset_digest = fetch_asset(connection, asset_key).await?;
    let build_asset_blob = fetch_blob(connection, &build_asset_digest).await?;

    let encoded_result = bincode::deserialize::<BuildResult>(&build_asset_blob)?;

    if let Some(dir) = encoded_result.get_result_digest() {
        check_directory(connection, dir.to_owned()).await?;
    }

    Ok(encoded_result)
}

/// Retrieve a cached sources result from the cache
pub(crate) async fn get_cached_fetch(
    connection: &mut ReapiConnection,
    asset_key: String,
) -> anyhow::Result<FetchResult> {
    let build_asset_digest = fetch_asset(connection, asset_key).await?;
    let build_asset_blob = fetch_blob(connection, &build_asset_digest).await?;

    let encoded_result = bincode::deserialize::<FetchResult>(&build_asset_blob).unwrap();

    if let Some(dir) = encoded_result.get_result_digest() {
        check_directory(connection, dir.to_owned()).await?;
    }

    Ok(encoded_result)
}
