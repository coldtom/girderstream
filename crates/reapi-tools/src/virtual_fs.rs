// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! Virtual file systems allow a consistent api for cas and locally backed files
//!
//! The point of this module is to allow other functions to operate on files and
//! directories in a way that is agnostic of if the real file or directory is
//! in a local directory or in a cas.
//!
//! This module is used by things like `project` and `loader` that might load
//! from a local project or from a junction-ed project that has been fetch from
//! a remote source into the local cas.
//!
//! So far this the functions in girderstream that need a consistent api are
//! read only but this module should be easy to extent to writing as well if the
//! need arises.
// Lets not optimism this for now
// TODO revisit and see if it make much difference
#![allow(clippy::large_enum_variant)]
use anyhow::bail;
use async_recursion::async_recursion;
use std::path::PathBuf;
use tokio::{fs::File, io::AsyncReadExt};
use walkdir::WalkDir;

use reapi::build::bazel::remote::execution::v2::Digest;

use super::{cas_fs::directory_from_digest, grpc_io::fetch_blob, ReapiConnection};

/// A file system entity that could be a file or directory
///
/// This could be representing something on the local filesystem
/// or it could be something in the cas.
#[derive(Debug)]
pub enum VirtualNode {
    /// A cas backed filesystem entity
    #[allow(dead_code)]
    VirtualDir(VirtualDir),
    /// A local filesystem backed filesystem entity
    VirtualFile(VirtualFile),
}

/// A file like api that could be on a local file system or in cas
///
/// This enum presents a consistent api whether the file is backed by the cas
/// or is on the local file system.
#[derive(Debug)]
pub enum VirtualFile {
    /// A cas backed file
    CasBacked((Digest, String)),
    /// A file on the local file system
    Local(PathBuf),
}

impl VirtualFile {
    /// Get the file extension of the file name.
    ///
    /// This is similar to PathBufs `get_extension` and return None if the file
    /// name does ont include a extension.
    pub fn get_extension(&self) -> Option<&str> {
        match self {
            VirtualFile::Local(path) => path.extension().map(|e| e.to_str().unwrap()),
            VirtualFile::CasBacked((_, name)) => name.split('.').last(),
        }
    }

    /// Get the content of the file.
    ///
    /// This opens the file and returns its contents, whether that is a file
    /// on the local disc or from the cas the function is consistent.
    pub async fn get_content(
        &mut self,
        connection: &mut ReapiConnection,
    ) -> anyhow::Result<String> {
        match self {
            VirtualFile::Local(path) => {
                let mut buff = vec![];

                File::open(&path)
                    .await
                    .unwrap_or_else(|_| panic!("Could not open file at {}", path.display()))
                    .read_to_end(&mut buff)
                    .await?;

                Ok(String::from_utf8_lossy(&buff).to_string())
            }
            VirtualFile::CasBacked((digest, _name)) => {
                let dir_blob = fetch_blob(connection, digest).await?;
                return Ok(String::from_utf8_lossy(&dir_blob).to_string());
            }
        }
    }

    /// Returns the part of the file name before the extension
    ///
    /// This is like `PathBuf`s file_stem, returns None if the file name does not
    /// contain a stem.
    pub fn file_stem(&self) -> Option<String> {
        match self {
            VirtualFile::Local(path) => path
                .file_stem()
                .to_owned()
                .map(|f| f.to_owned().into_string().unwrap()),
            VirtualFile::CasBacked((_, name)) => PathBuf::from(name.to_owned())
                .file_stem()
                .to_owned()
                .map(|f| f.to_owned().into_string().unwrap()),
        }
    }

    /// Returns the name of the file
    ///
    /// This is like `Pathbuf`s file_name function.
    pub fn file_name(&self) -> Option<String> {
        match self {
            VirtualFile::Local(path) => path
                .file_name()
                .to_owned()
                .map(|f| f.to_owned().into_string().unwrap()),
            VirtualFile::CasBacked((_, name)) => PathBuf::from(name.to_owned())
                .file_name()
                .to_owned()
                .map(|f| f.to_owned().into_string().unwrap()),
        }
    }
}

/// A directory like api that could be on a local file system or in cas
///
/// This enum presents a consistent api whether the directory is backed by the cas
/// or is on the local file system.
#[derive(Debug)]
pub enum VirtualDir {
    /// A cas backed directory
    #[allow(clippy::large_enum_variant)]
    CasBacked((Digest, String)),
    /// A directory on the local filesystem
    Local(PathBuf),
}

impl VirtualDir {
    /// Return the content of a file within the directory
    ///
    /// This presents a consistent api for directories on the local file system
    /// and cas backed ones.
    pub async fn read_file(
        &mut self,
        connection: &mut ReapiConnection,
        file_name: &str,
    ) -> anyhow::Result<String> {
        match self {
            VirtualDir::Local(path) => {
                //path.join(file_name)
                let mut buff = vec![];
                let file_path = path.join(file_name);

                File::open(&file_path)
                    .await
                    .unwrap_or_else(|_| {
                        panic!("Could not open project.conf at {}", file_path.display())
                    })
                    .read_to_end(&mut buff)
                    .await?;

                Ok(String::from_utf8_lossy(&buff).to_string())
            }
            VirtualDir::CasBacked((digest, _name)) => {
                let dir = directory_from_digest(connection, digest).await.unwrap();
                for child in &dir.files {
                    if child.name == file_name {
                        let dir_blob =
                            fetch_blob(connection, child.digest.as_ref().unwrap()).await?;
                        return Ok(String::from_utf8_lossy(&dir_blob).to_string());
                    }
                }
                let dirs = dir.directories;
                bail!("Can not join dirs, {file_name:?} {dirs:?}")
            }
        }
    }

    /// Get a sub-directory with a given name
    ///
    /// This will return a `VirtualDir` representing a child directory within the
    /// current directory with the name `dir_name`.
    ///
    /// # Note
    ///
    /// This does not behave consistently for local and cas if the child dir
    /// does not exist. For local dirs we do no validation but for cas ones we
    /// panic if the child does not already exist.
    pub async fn get_dir_name(
        &mut self,
        connection: &mut ReapiConnection,
        dir_name: &str,
    ) -> VirtualDir {
        match self {
            VirtualDir::Local(path) => VirtualDir::Local(path.join(dir_name)),
            VirtualDir::CasBacked((digest, _name)) => {
                let dir = directory_from_digest(connection, digest).await.unwrap();
                for child in dir.directories {
                    if child.name == dir_name {
                        return VirtualDir::CasBacked((child.digest.unwrap(), child.name));
                    }
                }
                panic!("Can not join dirs")
            }
        }
    }

    /// Creates a vector containing all of this directories files and subdirectors
    ///
    /// This function recessively collect all of the file and sub directories of
    /// its self and all subdirectories and files. And then returns them in a
    /// vector.
    ///
    /// # Note
    ///
    /// It would be nice not to need to keep the ful list in memory and instead
    /// return a stream. This function may be replace in the future with a stream
    /// version.
    #[async_recursion]
    pub async fn walk(&mut self, connection: &mut ReapiConnection) -> Vec<VirtualNode> {
        let mut full_list = vec![];
        match self {
            VirtualDir::CasBacked((digest, _name)) => {
                let dir = directory_from_digest(connection, digest).await.unwrap();
                for child_file in dir.files {
                    full_list.push(VirtualNode::VirtualFile(VirtualFile::CasBacked((
                        child_file.digest.unwrap(),
                        child_file.name,
                    ))));
                }
                for child_dir in dir.directories {
                    let mut child =
                        VirtualDir::CasBacked((child_dir.digest.unwrap(), child_dir.name));
                    full_list.extend(child.walk(connection).await);
                }
            }
            VirtualDir::Local(root) => {
                for next in WalkDir::new(root) {
                    let next = match next {
                        Ok(next) => next,
                        _ => break,
                    };
                    let path = next.path();
                    if path.is_file() {
                        full_list.push(VirtualNode::VirtualFile(VirtualFile::Local(
                            next.into_path(),
                        )));
                    } else if path.is_dir() {
                        full_list
                            .push(VirtualNode::VirtualDir(VirtualDir::Local(next.into_path())));
                    }
                }
            }
        };
        full_list
    }
}
