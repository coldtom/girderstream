// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! Push and fetch artifacts from the local cache to the remote cache

use std::vec;

use async_channel::bounded;
use async_recursion::async_recursion;
use prost::Message;
use reapi::build::{
    bazel::remote::execution::v2::{Digest, Directory, FindMissingBlobsRequest, Tree},
    buildgrid::{FetchTreeRequest, GetInstanceNameForRemoteRequest},
};
use tokio::task::{self, JoinHandle};

use crate::{
    cas_fs::directory_from_digest,
    grpc_io::{fetch_blob, move_blob_between_cas, upload_blob},
    Crapshoot, ReapiConnection,
};

/// Gather a list of all the digests that need to be uploaded to fully upload a tree
///
/// It would be nice to be able to start pushing as soon as we have some elements
/// maybe this could be done as a stream.. but if buildbox-casd implemented
/// Uploadtree that would make it even better. As a project we are currently
/// tightly coupled with buildbox so as this project it would be good to contribute
/// to buildbox by implementing UploadTree.
#[async_recursion]
pub async fn from_tree_to_list(
    connection: &mut ReapiConnection,
    directory_digest: &Digest,
) -> Vec<Digest> {
    let directory = directory_from_digest(connection, directory_digest)
        .await
        .unwrap();

    let mut list = vec![directory_digest.clone()];
    for file in directory.files {
        list.push(file.digest.unwrap());
    }
    for dir in directory.directories {
        list.extend(from_tree_to_list(connection, &dir.digest.unwrap()).await);
    }
    list
}

pub async fn get_instance_name(
    connection_local: &mut ReapiConnection,
    connection_remote: &mut ReapiConnection,
) -> anyhow::Result<String> {
    let name_request = GetInstanceNameForRemoteRequest {
        url: connection_remote.get_endpoint().clone(),
        instance_name: connection_remote.get_instance_name().clone(),
        server_cert: vec![],
        client_key: vec![],
        client_cert: vec![],
    };

    let name_responace = connection_local
        .get_local_client()
        .await?
        .ok_or(anyhow::anyhow!("connection_local not a local connection"))?
        .get_instance_name_for_remote(name_request)
        .await?;
    Ok(name_responace.into_inner().instance_name)
}

/// Upload blobs to a remote cas in pure rust
///
/// Chunks the [`FindMissingBlobsRequest`] to avoid
/// creating over sized Requests.
pub async fn push_blobs_manual(
    connection_local: &mut ReapiConnection,
    connection_remote: &mut ReapiConnection,
    blob_digests: Vec<Digest>,
) -> Crapshoot {
    // Set up a set of workers that run `move_blob_between_cas` when there are blobs
    // to move.
    let (s_job, r_job) = bounded(1000);
    let mut joins: Vec<JoinHandle<()>> = vec![];
    for _processor in 0..(100.min(blob_digests.len())) {
        let mut local_connection_local = connection_local.clone();
        let mut local_connection_remote = connection_remote.clone();
        let reciver = r_job.clone();

        let join = task::spawn(async move {
            while let Some(blob) = reciver.recv().await.unwrap() {
                move_blob_between_cas(
                    &mut local_connection_local,
                    &mut local_connection_remote,
                    blob,
                )
                .await
                .unwrap();
            }
        });
        joins.push(join);
    }

    // Setup the client to query which blobs to move.
    let instance_name = connection_remote.get_instance_name().to_string();
    let casc = connection_remote.get_cas_client().await?;

    // Chunk up the blobs so that [`FindMissingBlobsRequest`] requests are not too big
    // each request should be no bigger than the max request size.
    // We should check the max size of requests and then set the chunk size as appropriate.
    for blobs in blob_digests.chunks(10_000) {
        // Check for which blobs need uploading
        let fmbr = FindMissingBlobsRequest {
            instance_name: instance_name.to_string(),
            blob_digests: blobs.to_owned(),
        };
        let fmbres = casc.find_missing_blobs(fmbr).await?;
        let (meta_map, res, _) = fmbres.into_parts();
        assert_eq!(meta_map.into_headers().get("grpc-status").unwrap(), "0");

        // Let the worker tasks know which blobs to move
        for blob in res.missing_blob_digests {
            // The bounded queue means that these will wait once the queue fills up.
            // The queue stops this loop dumping all the blobs into the queue and the queue
            // becoming huge.
            s_job.send(Some(blob)).await?;
        }
    }
    // We have now issued all the requests so now signal to the worker tasks to stop
    for _join in &joins {
        s_job.send(None).await?;
    }
    // Wait for all the works to finish pushing all their blobs
    for join in joins {
        join.await?;
    }

    Ok(())
}

/// Pull a cas structure from a remote to local
///
/// This uses casd's local client to pull down a cas directory from a given
/// remote.
pub async fn pull_cached_cas(
    connection_local: &mut ReapiConnection,
    connection_remote: &mut ReapiConnection,
    tree_digest: &Digest,
) -> Crapshoot {
    // setup remote with casd
    let name_request = GetInstanceNameForRemoteRequest {
        url: connection_remote.get_endpoint().clone(),
        instance_name: connection_remote.get_instance_name().clone(),
        server_cert: vec![],
        client_key: vec![],
        client_cert: vec![],
    };
    let local_client = connection_local
        .get_local_client()
        .await?
        .ok_or(anyhow::anyhow!("connection_local not a local connection"))?;
    let name_responace = local_client
        .get_instance_name_for_remote(name_request)
        .await?;
    let instance_name = name_responace.into_inner().instance_name;

    let request = FetchTreeRequest {
        instance_name,
        root_digest: Some(tree_digest.clone()),
        fetch_file_blobs: true,
    };
    let _result = local_client.fetch_tree(request).await?;
    // It would be good if _result could be checked for a positive status code..

    Ok(())
}

/// Take a [`Directory`], ensure it is in the local cache and return the Digests of its files
///
/// This function is helper for [`pull_cached_cas_manual`]
async fn gather_directories(
    connection_local: &mut ReapiConnection,
    directory: &Directory,
) -> anyhow::Result<Vec<Digest>> {
    let mut root_blob = vec![];
    directory.encode(&mut root_blob)?;
    upload_blob(connection_local, root_blob).await?;
    let mut files = vec![];

    for file in &directory.files {
        files.push(file.digest.clone().unwrap())
    }

    Ok(files)
}

/// Pull a cas structure from a remote to local
///
/// This does not use casd's protos and so can be use with non casd based
/// cas, eg buildbarn
pub async fn pull_cached_cas_manual(
    connection_local: &mut ReapiConnection,
    connection_remote: &mut ReapiConnection,
    tree_digest: &Digest,
) -> Crapshoot {
    let tree_blob = fetch_blob(connection_remote, tree_digest).await?;
    let tree = Tree::decode(tree_blob.as_slice())?;

    let root = tree.root.as_ref().unwrap();
    let mut files = gather_directories(connection_local, root).await?;

    for child_dir in &tree.children {
        files.extend(
            gather_directories(connection_local, child_dir)
                .await?
                .into_iter(),
        );
    }

    push_blobs_manual(connection_remote, connection_local, files).await?;

    Ok(())
}
