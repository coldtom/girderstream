// Copyright 2022 William Salmon
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//! Read and write from the file system to the cas
//!
//! Most of these functions use buildbox-casd to do the actual io.
use std::os::unix::fs::PermissionsExt;
use std::path::Path;

use anyhow::anyhow;
use async_recursion::async_recursion;
use tokio::fs;

use crate::{cas_fs::directory_from_digest, Crapshoot, ReapiConnection};
use reapi::build::bazel::remote::execution::v2::Digest;

use super::grpc_io::write_blob;

/// Checkout the build result of a element to the local file system
///
/// This uses buildbox-casd to do the cas to fs logic but as buildbox-casd
/// only does a fuse mount that will be unmounted when it stops we copy
/// the files from the mount to the final location on the local file system.
///
/// This is not great, we could probably do a better job of writing directly
/// from casd to the fs with our own code. But as we already have the recursive
/// function, then this seems like a good option for now.
pub async fn checkout(
    connection: &mut ReapiConnection,
    target: Digest,
    location: &Path,
    fix_abs_links: bool,
) -> Crapshoot {
    if location.is_dir() || location.is_file() || location.is_symlink() {
        anyhow::bail!("Checkout directory must not exists already")
    };
    let parent = location.parent().expect("Could not determine parent");
    if !(parent.is_dir() || parent.is_symlink()) {
        anyhow::bail!("Checkout directory's parent must exists")
    };
    let fix_abs_links = if fix_abs_links { Some(location) } else { None };
    recursive_checkout(connection, target, location, fix_abs_links).await
}

#[async_recursion]
pub async fn recursive_checkout(
    connection: &mut ReapiConnection,
    target: Digest,
    location: &Path,
    fix_abs_links: Option<&'async_recursion Path>,
) -> Crapshoot {
    tokio::fs::create_dir(location).await?;
    let root = directory_from_digest(connection, &target).await?;

    for dir in root.directories {
        let new_dir = location.join(dir.name);
        recursive_checkout(connection, dir.digest.unwrap(), &new_dir, fix_abs_links).await?
    }

    for file in root.files {
        let new_file = location.join(file.name);
        let mut open_file = fs::File::create(&new_file).await?;
        write_blob(
            connection,
            file.digest.as_ref().ok_or(anyhow!("missing file blob"))?,
            &mut open_file,
        )
        .await?;
        if file.is_executable {
            fs::set_permissions(new_file, std::fs::Permissions::from_mode(0o744)).await?;
        }
    }

    for link in root.symlinks {
        let target = Path::new(&link.target);
        let target = if target.is_relative() {
            target.to_path_buf()
        } else if let Some(root_dir) = fix_abs_links {
            // This means that any absolute links work once checked out
            // but if you checkout to use as a chroot etc then you want them
            // to be broken on the checkout so that they work in the chroot..
            root_dir.join(target)
        } else {
            target.to_path_buf()
        };
        let link_path = location.join(link.name);
        fs::symlink(&target, link_path).await?;
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use tokio::fs;

    use crate::{
        cas_fs::{join_digests, offset_digest, place_file_in_dir},
        grpc_io::upload_blob,
        test_infra::CasdWrapper,
        Crapshoot, FullConfig, LocalConfig,
    };

    use super::checkout;

    #[tokio::test]
    async fn test_checkout_blob() -> Crapshoot {
        let mut casd_wrapper = CasdWrapper::start_casd()?;
        let temp_dir = casd_wrapper.get_temp_root();

        let config = LocalConfig::new(casd_wrapper.get_address(), "".to_string());
        let full_config = FullConfig::new(config, vec![]);
        let mut connection = full_config.get_local().to_owned().into();

        let blob_original = "Hello there how lovely to see you".to_string();

        casd_wrapper.wait_for_casd().await?;

        let digest_blob = upload_blob(&mut connection, blob_original.as_bytes().to_vec())
            .await
            .expect("Could not upload blob");
        let mid_digest_dir =
            place_file_in_dir(&mut connection, "example".to_string(), digest_blob, false)
                .await
                .unwrap();
        let digest_dir_with_sub =
            offset_digest(&mut connection, mid_digest_dir.clone(), "sub_folder".into()).await?;

        let digest_dir =
            join_digests(&mut connection, &digest_dir_with_sub, &mid_digest_dir).await?;

        let temp_dir_out = temp_dir.to_owned().join("buildbox_hates_dots");

        checkout(&mut connection, digest_dir, &temp_dir_out, false)
            .await
            .unwrap();
        let raw = fs::read(temp_dir_out.join("sub_folder").join("example")).await?;
        let result = String::from_utf8_lossy(&raw);
        assert_eq!(result, blob_original);
        let raw = fs::read(temp_dir_out.join("example")).await?;
        let result = String::from_utf8_lossy(&raw);
        assert_eq!(result, blob_original);

        casd_wrapper.close_with_future().await?;

        Ok(())
    }
}
