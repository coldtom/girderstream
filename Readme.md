# Girderstream

Girderstream is a meta-build project that builds and integrates software from a
set of declarative build instructions a bit like buildroot, yocto or buildstream.

A introduction can be found in our docs as [source](./docs) or as [html](https://girderstream.gitlab.io/girderstream/book/main/about.html)

If you use girderstream as a lib or as a dev the doc strings are also rendered as
[html](https://girderstream.gitlab.io/girderstream/doc/girderstream/index.html)

## Users of Girderstream

Some projects providing tool chains for you to use to build your girderstream projects:

* [Girderstream SDK](https://gitlab.com/pointswaves/girderstream-sdk/)
  This imports Freedesktop-sdk into girderstream elements (not maintained)
* [GrdTools](https://gitlab.com/pointswaves/grdtools/)
  This builds up a tool chain including native and cross-compilers and has some
  example target elements.

Some projects that build target systems that you can install your programs into
and provide BSPs so you can build full disk images for varius targets.

* [Girderstream Example Project](https://gitlab.com/pointswaves/girderstream-test-project)
  Provides base x86 and aarch64 userlands including busybox, gnu with optional systemd.
  Provides board support for qemu, pi3 pi4 and pi zero 2.

Example Projects using girderstream as there intergation tool.

* [Big Yellow Camera](https://gitlab.com/pointswaves/pi-camera)
  A open source point and shoot camera project running a girderstream-example-project
  userland and BSP that it adds its camera app into.

If you are using girderstream please create a MR adding your project(s)

## Test

To run the test you should have `buildbox` installed. Both `buildbox-casd` and `buildbox-run` should both
be on your PATH and work correctly.

``` bash
cargo test
```

## Example runs

You can easily run girderstream locally built from source or in a pre built docker
container.

### Run locally

To run girderstream you need casd to be running, eventually girderstream will start and stop it
its self but for now it does not and you need to set it off yourself.

``` bash
buildbox-casd  --bind localhost:50040 /tmp/cas/cas
```

Then you can do things like show a project:

``` bash
cargo run -- --verbose --options set=set1 --project-root tests/load_tests/ show --name app
cargo run -- --verbose --options set=set2 --project-root tests/load_tests/ show --name app
cargo run -- --options set=set1 --project-root tests/load_tests/ show --name app
```

To build we need buildbox-run pre installed an your path.

In one terminal set off buildbox-casd and in another terminal run girderstream

``` bash
cargo run -- --project-root tests/load_tests/ build --name stage0
```

Note that girderstream has been used to build a small number of real, not just import elements but
that these project are in there early stages and are in flux.

### Install locally

You can install girderstream to your local rust folder, `~/.cargo/bin/`, with cargo

```bash
cargo install --path . --locked
```

Then you can run girderstream without cargo, eg, as above but from a installed girderstream:

```bash
girderstream --project-root tests/load_tests/ build --name stage0
```

### Using docker

First pull down the latest build from the git lab registries

``` bash
docker pull registry.gitlab.com/girderstream/docker-images/girderstream-ci-base:latest
```

Currently this docker image is mostly used in ci so is a little ruff round the edges
but should work effectively.

``` bash
$ docker run -it -v `pwd`:/project:Z --privileged --workdir /project/ -v ~/.cache/girderstream/docker:/tmp/cas:Z registry.gitlab.com/girderstream/docker-images/girderstream-ci-base:latest
# buildbox-casd  --bind localhost:50040 /tmp/cas &
# girderstream --help
# girderstream -v show --name busybox
# girderstream build --name busybox
```
